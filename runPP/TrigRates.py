do_ppMinBias =True #For tracks only; muons should always be unused


#------------------------------------------------------------
m_EvtMax    =-1
dataSource  ='data'
GRL=[]
MinBias_triggers=[]

if do_ppMinBias:
  GRL=["AllLowMu13TeVRuns.xml"]
  InputFile   ="/usatlas/u/pyin/usatlasdata/ppData/test_xAOD/data18_13TeV/AOD.25492207._000001.pool.root.1"
  MinBias_triggers        =[
                            "HLT_mb_sptrk"                                    ,
                            "HLT_noalg_mb_L1MBTS_1"                           ,
                            "HLT_noalg_mb_L1MBTS_1_1"                         ,
                            "HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10"         ,
                            "HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5"          ,
                            "HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1"              ,
                            "HLT_mb_sp1000_trk70_hmt_L1TE10"                  ,
                            "HLT_mb_sp1000_trk70_hmt_L1TE5"                   ,
                            "HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10"         ,
                            "HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20"         ,
                            "HLT_mb_sp1100_pusup450_trk70_hmt_L1TE30"         ,
                            "HLT_mb_sp1100_pusup450_trk70_hmt_L1TE5"          ,
                            "HLT_mb_sp1100_trk70_hmt_L1TE10"                  ,
                            "HLT_mb_sp1100_trk70_hmt_L1TE20"                  ,
                            "HLT_mb_sp1100_trk70_hmt_L1TE30"                  ,
                            "HLT_mb_sp1100_trk70_hmt_L1TE5"                   ,
                            "HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5"         ,
                            "HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10"         ,
                            "HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15"         ,
                            "HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20"         ,
                            "HLT_mb_sp1200_pusup500_trk80_hmt_L1TE30"         ,
                            "HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5"          ,
                            "HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5"          ,
                            "HLT_mb_sp1200_trk100_hmt_L1TE5"                  ,
                            "HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1"              ,
                            "HLT_mb_sp1200_trk80_hmt_L1TE10"                  ,
                            "HLT_mb_sp1200_trk80_hmt_L1TE15"                  ,
                            "HLT_mb_sp1200_trk80_hmt_L1TE20"                  ,
                            "HLT_mb_sp1200_trk80_hmt_L1TE30"                  ,
                            "HLT_mb_sp1200_trk80_hmt_L1TE5"                   ,
                            "HLT_mb_sp1200_trk90_hmt_L1TE5"                   ,
                            "HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10"         ,
                            "HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15"         ,
                            "HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20"         ,
                            "HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30"         ,
                            "HLT_mb_sp1400_pusup550_trk90_hmt_L1TE40"         ,
                            "HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5"          ,
                            "HLT_mb_sp1400_trk100_hmt_L1TE20"                 ,
                            "HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1"              ,
                            "HLT_mb_sp1400_trk90_hmt_L1TE10"                  ,
                            "HLT_mb_sp1400_trk90_hmt_L1TE15"                  ,
                            "HLT_mb_sp1400_trk90_hmt_L1TE20"                  ,
                            "HLT_mb_sp1400_trk90_hmt_L1TE30"                  ,
                            "HLT_mb_sp1400_trk90_hmt_L1TE40"                  ,
                            "HLT_mb_sp1400_trk90_hmt_L1TE5"                   ,
                            "HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10"        ,
                            "HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15"        ,
                            "HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20"        ,
                            "HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25"        ,
                            "HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30"        ,
                            "HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40"        ,
                            "HLT_mb_sp1600_pusup600_trk100_hmt_L1TE5"         ,
                            "HLT_mb_sp1600_pusup600_trk100_hmt_L1TE50"        ,
                            "HLT_mb_sp1600_trk100_hmt_L1TE10"                 ,
                            "HLT_mb_sp1600_trk100_hmt_L1TE15"                 ,
                            "HLT_mb_sp1600_trk100_hmt_L1TE20"                 ,
                            "HLT_mb_sp1600_trk100_hmt_L1TE25"                 ,
                            "HLT_mb_sp1600_trk100_hmt_L1TE30"                 ,
                            "HLT_mb_sp1600_trk100_hmt_L1TE40"                 ,
                            "HLT_mb_sp1600_trk100_hmt_L1TE5"                  ,
                            "HLT_mb_sp1600_trk100_hmt_L1TE50"                 ,
                            "HLT_mb_sp1700_pusup650_trk110_hmt_L1TE10"        ,
                            "HLT_mb_sp1700_pusup650_trk110_hmt_L1TE20"        ,
                            "HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30"        ,
                            "HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40"        ,
                            "HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50"        ,
                            "HLT_mb_sp1700_pusup650_trk110_hmt_L1TE60"        ,
                            "HLT_mb_sp1700_trk110_hmt_L1TE10"                 ,
                            "HLT_mb_sp1700_trk110_hmt_L1TE20"                 ,
                            "HLT_mb_sp1700_trk110_hmt_L1TE30"                 ,
                            "HLT_mb_sp1700_trk110_hmt_L1TE40"                 ,
                            "HLT_mb_sp1700_trk110_hmt_L1TE50"                 ,
                            "HLT_mb_sp1700_trk110_hmt_L1TE60"                 ,
                            "HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10"        ,
                            "HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15"        ,
                            "HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20"        ,
                            "HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25"        ,
                            "HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30"        ,
                            "HLT_mb_sp1800_trk110_hmt_L1TE10"                 ,
                            "HLT_mb_sp1800_trk110_hmt_L1TE15"                 ,
                            "HLT_mb_sp1800_trk110_hmt_L1TE20"                 ,
                            "HLT_mb_sp1800_trk110_hmt_L1TE25"                 ,
                            "HLT_mb_sp1800_trk110_hmt_L1TE30"                 ,
                            "HLT_mb_sp1900_pusup700_trk120_hmt_L1TE10"        ,
                            "HLT_mb_sp1900_pusup700_trk120_hmt_L1TE20"        ,
                            "HLT_mb_sp1900_pusup700_trk120_hmt_L1TE30"        ,
                            "HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40"        ,
                            "HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50"        ,
                            "HLT_mb_sp1900_pusup700_trk120_hmt_L1TE60"        ,
                            "HLT_mb_sp1900_pusup700_trk120_hmt_L1TE70"        ,
                            "HLT_mb_sp1900_trk120_hmt_L1TE10"                 ,
                            "HLT_mb_sp1900_trk120_hmt_L1TE20"                 ,
                            "HLT_mb_sp1900_trk120_hmt_L1TE30"                 ,
                            "HLT_mb_sp1900_trk120_hmt_L1TE40"                 ,
                            "HLT_mb_sp1900_trk120_hmt_L1TE50"                 ,
                            "HLT_mb_sp1900_trk120_hmt_L1TE60"                 ,
                            "HLT_mb_sp1900_trk120_hmt_L1TE70"                 ,
                            "HLT_mb_sp2100_pusup750_trk130_hmt_L1TE20"        ,
                            "HLT_mb_sp2100_pusup750_trk130_hmt_L1TE30"        ,
                            "HLT_mb_sp2100_pusup750_trk130_hmt_L1TE40"        ,
                            "HLT_mb_sp2100_pusup750_trk130_hmt_L1TE50"        ,
                            "HLT_mb_sp2100_pusup750_trk130_hmt_L1TE60"        ,
                            "HLT_mb_sp2100_pusup750_trk130_hmt_L1TE70"        ,
                            "HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10"        ,
                            "HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15"        ,
                            "HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20"        ,
                            "HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25"        ,
                            "HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30"        ,
                            "HLT_mb_sp2100_pusup900_trk120_hmt_L1TE5"         ,
                            "HLT_mb_sp2100_trk120_hmt_L1TE10"                 ,
                            "HLT_mb_sp2100_trk120_hmt_L1TE15"                 ,
                            "HLT_mb_sp2100_trk120_hmt_L1TE20"                 ,
                            "HLT_mb_sp2100_trk120_hmt_L1TE25"                 ,
                            "HLT_mb_sp2100_trk120_hmt_L1TE30"                 ,
                            "HLT_mb_sp2100_trk130_hmt_L1TE20"                 ,
                            "HLT_mb_sp2100_trk130_hmt_L1TE30"                 ,
                            "HLT_mb_sp2100_trk130_hmt_L1TE40"                 ,
                            "HLT_mb_sp2100_trk130_hmt_L1TE50"                 ,
                            "HLT_mb_sp2100_trk130_hmt_L1TE60"                 ,
                            "HLT_mb_sp2100_trk130_hmt_L1TE70"                 ,
                            "HLT_mb_sp2200_pusup800_trk140_hmt_L1TE20"        ,
                            "HLT_mb_sp2200_pusup800_trk140_hmt_L1TE30"        ,
                            "HLT_mb_sp2200_pusup800_trk140_hmt_L1TE40"        ,
                            "HLT_mb_sp2200_pusup800_trk140_hmt_L1TE50"        ,
                            "HLT_mb_sp2200_pusup800_trk140_hmt_L1TE60"        ,
                            "HLT_mb_sp2200_pusup800_trk140_hmt_L1TE70"        ,
                            "HLT_mb_sp2200_trk140_hmt_L1TE20"                 ,
                            "HLT_mb_sp2200_trk140_hmt_L1TE30"                 ,
                            "HLT_mb_sp2200_trk140_hmt_L1TE40"                 ,
                            "HLT_mb_sp2200_trk140_hmt_L1TE50"                 ,
                            "HLT_mb_sp2200_trk140_hmt_L1TE60"                 ,
                            "HLT_mb_sp2200_trk140_hmt_L1TE70"                 ,
                            "HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15"       ,
                            "HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20"       ,
                            "HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25"       ,
                            "HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30"       ,
                            "HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40"       ,
                            "HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE5"        ,
                            "HLT_mb_sp2300_trk130_hmt_L1TE15"                 ,
                            "HLT_mb_sp2300_trk130_hmt_L1TE20"                 ,
                            "HLT_mb_sp2300_trk130_hmt_L1TE25"                 ,
                            "HLT_mb_sp2300_trk130_hmt_L1TE30"                 ,
                            "HLT_mb_sp2300_trk130_hmt_L1TE40"                 ,
                            "HLT_mb_sp2400_pusup850_trk150_hmt_L1TE20"        ,
                            "HLT_mb_sp2400_pusup850_trk150_hmt_L1TE30"        ,
                            "HLT_mb_sp2400_pusup850_trk150_hmt_L1TE40"        ,
                            "HLT_mb_sp2400_pusup850_trk150_hmt_L1TE50"        ,
                            "HLT_mb_sp2400_pusup850_trk150_hmt_L1TE60"        ,
                            "HLT_mb_sp2400_pusup850_trk150_hmt_L1TE70"        ,
                            "HLT_mb_sp2400_trk150_hmt_L1TE20"                 ,
                            "HLT_mb_sp2400_trk150_hmt_L1TE30"                 ,
                            "HLT_mb_sp2400_trk150_hmt_L1TE40"                 ,
                            "HLT_mb_sp2400_trk150_hmt_L1TE50"                 ,
                            "HLT_mb_sp2400_trk150_hmt_L1TE60"                 ,
                            "HLT_mb_sp2400_trk150_hmt_L1TE70"                 ,
                            "HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE10"       ,
                            "HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE40"       ,
                            "HLT_mb_sp2500_trk140_hmt_L1TE40"                 ,
                            "HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE40"       ,
                            "HLT_mb_sp2700_trk150_hmt_L1TE40"                 ,
                            "HLT_mb_sp2900_pusup1300_trk160_hmt_L1TE40"       ,
                            "HLT_mb_sp2900_trk160_hmt_L1TE40"                 ,
                            "HLT_mb_sp400_trk40_hmt_L1MBTS_1_1"               ,
                            "HLT_mb_sp600_pusup300_trk40_hmt_L1TE10"          ,
                            "HLT_mb_sp600_pusup300_trk40_hmt_L1TE5"           ,
                            "HLT_mb_sp600_trk40_hmt_L1TE10"                   ,
                            "HLT_mb_sp600_trk40_hmt_L1TE5"                    ,
                            "HLT_mb_sp600_trk45_hmt_L1MBTS_1_1"               ,
                            "HLT_mb_sp700_pusup350_trk50_hmt_L1TE10"          ,
                            "HLT_mb_sp700_pusup350_trk50_hmt_L1TE20"          ,
                            "HLT_mb_sp700_pusup350_trk50_hmt_L1TE5"           ,
                            "HLT_mb_sp700_trk50_hmt_L1MBTS_1_1"               ,
                            "HLT_mb_sp700_trk50_hmt_L1TE10"                   ,
                            "HLT_mb_sp700_trk50_hmt_L1TE20"                   ,
                            "HLT_mb_sp700_trk50_hmt_L1TE5"                    ,
                            "HLT_mb_sp700_trk55_hmt_L1MBTS_1_1"               ,
                            "HLT_mb_sp900_pusup400_trk50_hmt_L1TE5"           ,
                            "HLT_mb_sp900_pusup400_trk60_hmt_L1TE10"          ,
                            "HLT_mb_sp900_pusup400_trk60_hmt_L1TE20"          ,
                            "HLT_mb_sp900_pusup400_trk60_hmt_L1TE5"           ,
                            "HLT_mb_sp900_trk50_hmt_L1TE5"                    ,
                            "HLT_mb_sp900_trk60_hmt_L1MBTS_1_1"               ,
                            "HLT_mb_sp900_trk60_hmt_L1TE10"                   ,
                            "HLT_mb_sp900_trk60_hmt_L1TE20"                   ,
                            "HLT_mb_sp900_trk60_hmt_L1TE5"                    ,
                            "HLT_mb_sp900_trk65_hmt_L1MBTS_1_1"               ,
                            "HLT_noalg_mb_L1MBTS_2"                           ,
                            "HLT_j10_L1MBTS_2"                                ,
                            "HLT_j10_L1RD0_FILLED"                            ,
                            "HLT_j20_L1J12"                                   ,
                            "HLT_j20_L1MBTS_2"                                ,
                            "HLT_j20_L1RD0_FILLED"                            ,
                            "HLT_j45_L1J12"                                   ,
                            "HLT_noalg_mb_L1TE5"                              ,
                            "HLT_noalg_mb_L1TE10"                             ,
                            "HLT_noalg_mb_L1TE20"                             ,
                            "HLT_noalg_mb_L1TE30"                             ,
                            # "HLT_noalg_mb_L1RD0_FILLED"                       ,
                            ]
else :
  exit()
print(MinBias_triggers)
#------------------------------------------------------------








import AthenaPoolCnvSvc.ReadAthenaPool
import glob
svcMgr.EventSelector.InputCollections = glob.glob(InputFile)
theApp.EvtMax = m_EvtMax


from AthenaCommon.GlobalFlags import globalflags
globalflags.DataSource.set_Value_and_Lock(dataSource) #data geant4
svcMgr += CfgMgr.AthenaEventLoopMgr(EventPrintoutInterval=1)
globalflags.DatabaseInstance = 'CONDBR2' # e.g. if you want run 2 data,  set to COMP200 if you want run1. This is completely ignored in MC.
#globalflags.DatabaseInstance = 'COMP200' # e.g. if you want run 2 data,  set to COMP200 if you want run1. This is completely ignored in MC.



#the tool service
from AthenaCommon.AppMgr import ToolSvc

##------------------------------------------------------------
#Configure Track selection tool
#https://twiki.cern.ch/twiki/bin/view/AtlasProtected/InDetTrackSelectionTool
ToolSvc += CfgMgr.InDet__InDetTrackSelectionTool("TrackSelectionTool_MinBias",CutLevel="MinBias",minPt=400.)
ToolSvc += CfgMgr.InDet__InDetTrackSelectionTool("TrackSelectionTool_HILoose",CutLevel="HILoose",minPt=400.)
ToolSvc += CfgMgr.InDet__InDetTrackSelectionTool("TrackSelectionTool_HITight",CutLevel="HITight",minPt=400.)
##------------------------------------------------------------

##------------------------------------------------------------
# Create a MuonSelectionTool if we do not yet have one
if not hasattr(ToolSvc, "MyMuonSelectionTool"):
    from MuonSelectorTools.MuonSelectorToolsConf import CP__MuonSelectionTool
    ToolSvc += CP__MuonSelectionTool("MyMuonSelectionTool")
    ToolSvc.MyMuonSelectionTool.MaxEta = 2.5
##------------------------------------------------------------

##------------------------------------------------------------
#https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCPAnalysisWinterMC16
#https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MuonMomentumCorrectionsSubgroup
from MuonMomentumCorrections.MuonMomentumCorrectionsConf import CP__MuonCalibrationAndSmearingTool
ToolSvc += CP__MuonCalibrationAndSmearingTool("MyMuonCalibrationAndSmearingTool")
print(ToolSvc.MyMuonCalibrationAndSmearingTool)
#ToolSvc.MyMuonCalibrationAndSmearingTool.Year                 ="Data18"
#if do_hi2015_AcoplanarMuons:
#  ToolSvc.MyMuonCalibrationAndSmearingTool.Year               ="Data15"
#ToolSvc.MyMuonCalibrationAndSmearingTool.StatComb             =False
#ToolSvc.MyMuonCalibrationAndSmearingTool.SagittaRelease       ="sagittaBiasDataAll_03_02_19_Data18"
#ToolSvc.MyMuonCalibrationAndSmearingTool.SagittaCorr          =True
#ToolSvc.MyMuonCalibrationAndSmearingTool.doSagittaMCDistortion=False
#ToolSvc.MyMuonCalibrationAndSmearingTool.Release              ="Recs2018_05_20"
#ToolSvc.MyMuonCalibrationAndSmearingTool.SagittaCorrPhaseSpace=True
ToolSvc.MyMuonCalibrationAndSmearingTool.Year                 ="Data18"
ToolSvc.MyMuonCalibrationAndSmearingTool.StatComb             =False
ToolSvc.MyMuonCalibrationAndSmearingTool.SagittaRelease       ="sagittaBiasDataAll_03_02_19_Data18"
ToolSvc.MyMuonCalibrationAndSmearingTool.SagittaCorr          =True
ToolSvc.MyMuonCalibrationAndSmearingTool.doSagittaMCDistortion=False
ToolSvc.MyMuonCalibrationAndSmearingTool.Release              ="Recs2019_05_30"
ToolSvc.MyMuonCalibrationAndSmearingTool.SagittaCorrPhaseSpace=True
# if do_hi2015_AcoplanarMuons:
#   ToolSvc.MyMuonCalibrationAndSmearingTool.Year               ="Data16"
# if do_pPb2016_8TeV:
#   ToolSvc.MyMuonCalibrationAndSmearingTool.Year               ="Data16"
# if do_HI_MC_OVERLAY:
#   ToolSvc.MyMuonCalibrationAndSmearingTool.SagittaCorr        =False
# if do_pp2017_5TeV or do_pp2017_13TeV:
#   ToolSvc.MyMuonCalibrationAndSmearingTool.Year               ="Data17"
print(ToolSvc.MyMuonCalibrationAndSmearingTool)
##------------------------------------------------------------

##------------------------------------------------------------
from MuonEfficiencyCorrections.CommonToolSetup import *
scale_medium = GetMuonEfficiencyTool("Medium",Release="190312_Winter_r21")
scale_tight  = GetMuonEfficiencyTool("Tight" ,Release="190312_Winter_r21")
##------------------------------------------------------------

##------------------------------------------------------------
# Create trigger matching tool
#https://twiki.cern.ch/twiki/bin/view/Atlas/XAODMatchingTool
ToolSvc += CfgMgr.Trig__MatchingTool("MyTriggerMatchTool", OutputLevel=DEBUG)
##------------------------------------------------------------

##------------------------------------------------------------
#The GRLTool
MyGRLTool=CfgMgr.GoodRunsListSelectionTool("MyGRLTool")
MyGRLTool.GoodRunsListVec=GRL
#MyGRLTool.PassThrough    =True
ToolSvc += MyGRLTool
##------------------------------------------------------------


##------------------------------------------------------------
# UPC Group Gap Tool
clusterSelectionTool=CfgMgr.UPC__TopoClusterSelectionTool("TCSTool")
clusterSelectionTool.MinPt                 = 400.0
clusterSelectionTool.ApplySignificanceCuts = False
clusterSelectionTool.CalibFile             = "HIGapSelector/TCSigCuts.root"
clusterSelectionTool.CalibHisto            = "h1_TC_sig_cuts"
ToolSvc += clusterSelectionTool
##------------------------------------------------------------


##------------------------------------------------------------
#The ZDCTool
#https://twiki.cern.ch/twiki/bin/viewauth/Atlas/ZdcAnalysisTool
Configuration="PbPb2018"
ZdcAnalysisTool=CfgMgr.ZDC__ZdcAnalysisTool("ZdcAnalysisTool")
ZdcAnalysisTool.FlipEMDelay  =  False
ZdcAnalysisTool.LowGainOnly  =  False
ZdcAnalysisTool.DoCalib      =  True
ZdcAnalysisTool.Configuration=Configuration
ZdcAnalysisTool.AuxSuffix    =  "RP"
ZdcAnalysisTool.ForceCalibRun=  -1
if(Configuration ==  "PbPb2018"):
  print("Soumya:: ZDC Tool configured for 2018")
  ZdcAnalysisTool.DoTrigEff    =  False #for 2018 only?
  ZdcAnalysisTool.DoTimeCalib  =  False #for 2018 only?
else:
  print("Soumya:: ZDC Tool configured for 2015")
ToolSvc += ZdcAnalysisTool
##------------------------------------------------------------


##------------------------------------------------------------
#The jetCalibToolEMTopo4
jetCalibToolEMTopo4=CfgMgr.JetCalibrationTool("jetCalibEMTopo4Tool")
jetCalibToolEMTopo4.JetCollection= "AntiKt4EMTopo"
jetCalibToolEMTopo4.ConfigFile   = "JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config"
jetCalibToolEMTopo4.CalibSequence= "JetArea_Residual_EtaJES_GSC_Insitu"
jetCalibToolEMTopo4.CalibArea    = "00-04-82"
jetCalibToolEMTopo4.IsData       = True
ToolSvc += jetCalibToolEMTopo4
##------------------------------------------------------------


##------------------------------------------------------------
#The jetCalibEMPFlow4Tool
jetCalibToolEMPFlow4=CfgMgr.JetCalibrationTool("jetCalibEMPFlow4Tool")
jetCalibToolEMPFlow4.JetCollection= "AntiKt4EMPFlow"
jetCalibToolEMPFlow4.ConfigFile   = "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config"
jetCalibToolEMPFlow4.CalibSequence= "JetArea_Residual_EtaJES_GSC_Insitu"
jetCalibToolEMPFlow4.CalibArea    = "00-04-82"
jetCalibToolEMPFlow4.IsData       = True
ToolSvc += jetCalibToolEMPFlow4
##------------------------------------------------------------


##------------------------------------------------------------
#The jetCleaningToolLoose
jetCleaningToolLoose=CfgMgr.JetCleaningTool("jetCleaningToolLoose")
jetCleaningToolLoose.CutLevel= "LooseBad"
jetCleaningToolLoose.DoUgly  = False
ToolSvc += jetCleaningToolLoose
##------------------------------------------------------------


##------------------------------------------------------------
#The jetCleaningToolTight
jetCleaningToolTight=CfgMgr.JetCleaningTool("jetCleaningToolTight")
jetCleaningToolTight.CutLevel= "TightBad"
jetCleaningToolTight.DoUgly  = False
ToolSvc += jetCleaningToolTight
##------------------------------------------------------------


##------------------------------------------------------------
#The jvtTool
jvtTool=CfgMgr.JetVertexTaggerTool("jvtTool")
# jvtToolEMTopo.JetContainer= "AntiKt4EMTopoJets"
ToolSvc += jvtTool
##------------------------------------------------------------


##------------------------------------------------------------
#The weightPFOTool
weightPFOTool=CfgMgr.CP__WeightPFOTool("weightPFOTool")
weightPFOTool.DoEoverPWeight=True
ToolSvc += weightPFOTool
##------------------------------------------------------------


##------------------------------------------------------------
#Pileup tool
#from HIEventUtils.HIEventUtilsConf import HI__HIPileupTool
#HIPileupTool=HI__HIPileupTool("HIPileupTool")
# HIPileupTool=CfgMgr.HI__HIPileupTool("HIPileupTool")
# HIPileupTool.Year="2018"
# ToolSvc += HIPileupTool
##------------------------------------------------------------


MessageSvc.defaultLimit = 1000

#the main algorithm sequence
algSeq = CfgMgr.AthSequencer("AthAlgSeq")


#------------------------------------------------------------
#main algorithm to run
TrigRatesAlg=CfgMgr.TrigRates("TrigRatesAlg",OutputLevel=INFO)
#------------------------------------------------------------
TrigRatesAlg.TrackSelectionTool_MinBias  =ToolSvc.TrackSelectionTool_MinBias
TrigRatesAlg.TrackSelectionTool_HILoose  =ToolSvc.TrackSelectionTool_HILoose
TrigRatesAlg.TrackSelectionTool_HITight  =ToolSvc.TrackSelectionTool_HITight
# TrigRatesAlg.MuonSelectionTool   =ToolSvc.MyMuonSelectionTool
TrigRatesAlg.TriggerMatchTool    =ToolSvc.MyTriggerMatchTool
TrigRatesAlg.GRLTool             =ToolSvc.MyGRLTool
# TrigRatesAlg.muonCorrectionTool  =ToolSvc.MyMuonCalibrationAndSmearingTool
TrigRatesAlg.effi_SF_tool_medium =scale_medium
TrigRatesAlg.effi_SF_tool_tight  =scale_tight
TrigRatesAlg.clusterSelectionTool=clusterSelectionTool
TrigRatesAlg.ZDCAnalysisTool     =ZdcAnalysisTool
TrigRatesAlg.jetCalibToolEMTopo4 =jetCalibToolEMTopo4
TrigRatesAlg.jetCalibToolEMPFlow4=jetCalibToolEMPFlow4
TrigRatesAlg.jetCleaningToolLoose=jetCleaningToolLoose
TrigRatesAlg.jetCleaningToolTight=jetCleaningToolTight
TrigRatesAlg.jvtTool             =jvtTool
TrigRatesAlg.weightPFOTool       =weightPFOTool
# TrigRatesAlg.HIPileupTool        =HIPileupTool
#------------------------------------------------------------
TrigRatesAlg.UseGRL                  =True                      #Automatically ignored for MC
TrigRatesAlg.StoreAllEvents          =False                     #True only for MC
TrigRatesAlg.UseTrigger              =True                      #True for data False for MC
TrigRatesAlg.TriggerChains           ="|".join(MinBias_triggers)
# TrigRatesAlg.MuonTriggerChains       =""
# TrigRatesAlg.DiMuonTriggerChains     =""
TrigRatesAlg.StorePFlowNeutralCharge =1                         # =1 save pflow neutroal and charge objects. =0 not save
TrigRatesAlg.StoreEventInfo          =1                         #2 requires ZDC info, works only for Pb+Pb
TrigRatesAlg.StoreTracks             =2       #2+4+8            #bitmap
TrigRatesAlg.StoreVtx                =True                      #Always ON
TrigRatesAlg.StoreL1TE               =True                      #ON only for checks
# TrigRatesAlg.StoreSingleMuon         =False                   #Typically ON
# TrigRatesAlg.StoreAcoplanarMuon      =False                   #OFF for pp
TrigRatesAlg.HIEventShapeContainerKey="HIEventShape"            #"HIEventShape" or "CaloSums";="" to turn OFF, for exmple in pp
TrigRatesAlg.METContainerKey         ="MET_Calo"                #"MET_Calo";="" to turn OFF
TrigRatesAlg.TrackJetContainerKeys   =[
                                       #"AntiKt2PV0TrackJets",
                                       #"AntiKt3PV0TrackJets",
                                       #"AntiKt4PV0TrackJets",
                                       "AntiKt4EMPFlowJets",
                                       # "AntiKt4EMTopoJets"
                                       # "HLT_xAOD__JetContainer_a4tcemsubjesISFS"
                                       ]     #=[] or comment to turn OFF
TrigRatesAlg.StoreTruthVtx           =False                     #True only for MC
TrigRatesAlg.StoreTruth              =0                         #0:disable, 1:stable only with pt>"TruthMinPT", 2-all True only for MC
TrigRatesAlg.TruthMinPT              =300                       #cut in MeV above which to store truth particles (only stable and primary particles stored)
# TrigRatesAlg.ApplyMuonCalibrations   =True                      #False for MC (see link above where the tool is initialized)

algSeq += TrigRatesAlg
#------------------------------------------------------------


svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["MYSTREAM DATAFILE='myfile.root' OPT='RECREATE'"]

