pathena TrigRates.py \
        --inDsTxt data15_13TeV_set1.txt   \
        --outDS user.pyin.TrigRates.PP2015_13TeV_set1.20220222 \
        --excludeFile=myfile.root,clean.sh \
        --mergeOutput

pathena TrigRates.py \
        --inDsTxt data16_13TeV_set1.txt   \
        --outDS user.pyin.TrigRates.PP2016_13TeV_set1.20220222 \
        --excludeFile=myfile.root,clean.sh \
        --mergeOutput

pathena TrigRates.py \
        --inDsTxt data17_13TeV_set1.txt   \
        --outDS user.pyin.TrigRates.PP2017_13TeV_set1.20220222 \
        --excludeFile=myfile.root,clean.sh \
        --mergeOutput

pathena TrigRates.py \
        --inDsTxt data17_13TeV_set2.txt   \
        --outDS user.pyin.TrigRates.PP2017_13TeV_set2.20220222 \
        --excludeFile=myfile.root,clean.sh \
        --mergeOutput

pathena TrigRates.py \
        --inDsTxt data17_13TeV_set3.txt   \
        --outDS user.pyin.TrigRates.PP2017_13TeV_set3.20220222 \
        --excludeFile=myfile.root,clean.sh \
        --mergeOutput

pathena TrigRates.py \
        --inDsTxt data18_13TeV_set1.txt   \
        --outDS user.pyin.TrigRates.PP2018_13TeV_set1.20220222 \
        --excludeFile=myfile.root,clean.sh \
        --mergeOutput
