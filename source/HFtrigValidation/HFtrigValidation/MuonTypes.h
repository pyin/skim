// -*- c++ -*-
#ifndef __MuonTypes_H__
#define __MuonTypes_H__
#include "HFtrigValidation/AthenaVersion.h"

#include "AthenaBaseComps/AthAlgorithm.h"
#include "string"
#include "GaudiKernel/ToolHandle.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "TFile.h"

#ifdef __ATHENA_21p2__
  #include "MuonAnalysisInterfaces/IMuonSelectionTool.h"
#else
  #include "MuonSelectorTools/IMuonSelectionTool.h"
#endif


class TTree;

class MuonTypes : public AthAlgorithm{
public:
   MuonTypes(const std::string& name, ISvcLocator* pSvcLocator);
   double MomentumImbalance(const xAOD::Muon* Muon);
   ~MuonTypes() {};


   virtual StatusCode initialize();
   virtual StatusCode execute();
   virtual StatusCode finalize();


private:
   StatusCode CleaningCuts    (bool &pass_cleaning_cuts);
   bool IsPrimaryParticle(const xAOD::TruthParticle* particle);

   std::string m_EventInfo_key    ;
   std::string m_muons_key;

   StatusCode ProcessMuons();
   void InitMuonBranches();
   void ResetMuonBranches();
   TFile *m_OutFile;
   TTree *m_OutTree;
   Float_t m_pt;
   Float_t m_eta;
   Float_t m_phi;
   Int_t   m_quality;
   Float_t m_momentum_imbalance;
	 Int_t   m_elosstype;
   Float_t m_trk_d0;
   Float_t m_trk_z0;
   Int_t   m_id;
   Int_t   m_barcode;
   Int_t   m_primary;
   Float_t m_truth_pt;
   Float_t m_ms_pt;
   Float_t m_ms_p;
   Float_t m_truth_prob;

   //Tools
   ToolHandle<CP::IMuonSelectionTool> m_muonSelection;
};
#endif
