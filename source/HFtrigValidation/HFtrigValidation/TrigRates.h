#ifndef __TrigRates_H__
#define __TrigRates_H__
#include "HFtrigValidation/AthenaVersion.h"

#include "AthenaBaseComps/AthAlgorithm.h"

#include "string"
#include "map"
#include "vector"

#include "GaudiKernel/ToolHandle.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronxAODHelpers.h"
#include "JetInterface/IJetUpdateJvt.h"
#include "JetInterface/IJetModifier.h"
#include "ZdcAnalysis/IZdcAnalysisTool.h"

#include <HIGapSelector/GapCalculator.h>
#include <HIGapSelector/TopoClusterSelectionTool.h>

#include <JetCalibTools/IJetCalibrationTool.h>
#include <xAODJet/JetAuxContainer.h>
#include <xAODJet/JetContainer.h>
#include <xAODJet/Jet.h>
#include <xAODJet/JetConstituentVector.h>
// #include "HIEventUtils/IHIPileupTool.h"
#include "xAODPFlow/PFO.h"
#include <xAODPFlow/PFOContainer.h>
#include <xAODPFlow/PFOAuxContainer.h>

#include "JetSelectorTools/JetCleaningTool.h"
#include "PFlowUtils/IWeightPFOTool.h"

#ifdef __ATHENA_21p2__
  #include "AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h"
  #include "MuonAnalysisInterfaces/IMuonSelectionTool.h"
  #include "TriggerMatchingTool/IMatchingTool.h"
  #include "MuonAnalysisInterfaces/IMuonCalibrationAndSmearingTool.h"
  #include "MuonAnalysisInterfaces/IMuonEfficiencyScaleFactors.h"
#else
  #include "GoodRunsLists/IGoodRunsListSelectionTool.h"
  #include "MuonSelectorTools/IMuonSelectionTool.h"
  #include "TrigMuonMatching/ITrigMuonMatching.h"
  #include "MuonMomentumCorrections/IMuonCalibrationAndSmearingTool.h"
#endif


class TTree;
class Module;

class TrigRates : public AthAlgorithm{
public:
   /** Standard Athena-Algorithm Constructor */
   TrigRates(const std::string& name, ISvcLocator* pSvcLocator);
   /** Default Destructor */
   ~TrigRates() {};


   virtual StatusCode initialize();
   virtual StatusCode execute();
   virtual StatusCode finalize();


private:
   StatusCode CleaningCuts    (bool &pass_cleaning_cuts);


   bool                        m_use_trigger    = true;//=true then use the Trigger (make branches, use decision)
   std::string                 m_Trigger_Chains       ;//Trigger Chains for TrigDecision and branches
   std::string                 m_Muon_Trigger_Chains  ;//Trigger Chains for TrigDecision and branches
   std::string                 m_DiMuon_Trigger_Chains;//Trigger Chains for TrigDecision and branches
   std::vector<std::string>    m_ListOfTriggers       ;
   std::vector<std::string>    m_ListOfMuonTriggers   ;
   std::vector<std::string>    m_ListOfDiMuonTriggers ;
   std::map<std::string,Bool_t*             > m_trigger_map            ;//flags that indicate if the trigger passes,
   std::map<std::string,Float_t*            > m_trigger_prescale_map   ;//trigger prescale values,
   std::map<std::string,Bool_t*             > m_trigger_isPrescaled_map;//if a trigger was prescaled in the present event,
   std::map<std::string,Bool_t*             > m_trigger_L1TBP_map      ;//decision at L1 before prescale,
   std::map<std::string,Bool_t*             > m_trigger_isRerun_map    ;//if Trigger was rerun,
   std::map<std::string,Bool_t*             > m_trigger_rerun_map      ;//Trigger rerun decision,
   std::map<std::string,std::vector<Bool_t>*> m_trigger_match_map      ;//matching of reco objects to trigger,
   std::map<std::string,std::vector<Bool_t>*> m_trigger_match_map_V2   ;//matching of reco objects to trigger; doesnot require trigger passing "Physics"
   Bool_t  m_trigger_Flag                     [1000];//stores if a trigger passes, this is written to the output tree
   Float_t m_trigger_prescale_Value           [1000];//stores trigger prescale
   Bool_t  m_trigger_isPrescaled_Flag         [1000];//stores=1 if trigger didnot fire because it was Prescaled
   Bool_t  m_trigger_L1TBP_Flag               [1000];//stores Trigger decision at L1 before prescale
   Bool_t  m_trigger_isRerun_Flag             [1000];//stores if a trigger was rerun
   Bool_t  m_trigger_rerun_Flag               [1000];//stores if a trigger passes rerun
   std::vector<Bool_t> m_trigger_match_Flag   [1000];//whether a given object fired this trigger
   std::vector<Bool_t> m_trigger_match_Flag_V2[1000];//whether a given object fired this trigger; doesnot require trigger passing "Physics"
   void       AddTriggerBranches();
   StatusCode ProcessTriggers (bool &pass_trigger_cuts);



   int m_store_EventInfo   =1;//>=1 then store even info,  >=2 then also store ZDC
   std::string m_EventInfo_key    ;
   void       InitEventInfo   (TTree *l_OutTree = nullptr);
   StatusCode ProcessEventInfo();
   UInt_t RunNumber=0,lumi_block=0,bunch_crossing_id=0;
   ULong64_t eventNumber=0;
   Float_t AvgIntPerXing=0,ActIntPerXing=0;
   Float_t Trkz0RMS=0;
   Float_t sumGapPos=0;
   Float_t sumGapNeg=0;
   Float_t endGapPos=0;
   Float_t endGapNeg=0;
   Float_t ZdcEtA=0,ZdcEtC=0;
   Int_t   m_NumTrackNoCuts=0, m_NumTrackPPMinBias=0, m_NumTrackHILoose=0, m_NumTrackHITight=0;
   Bool_t  m_is_pileup=false,m_is_oo_pileup=false;
   Int_t   m_numtrk_HITight_pt500;
   Float_t m_FCalET,m_FCalETP,m_FCalETN;


   bool        m_store_Vtx           = true; //=true then store vertex info
   std::string m_vtx_container_key;
   void       InitVertex   (TTree *l_OutTree = nullptr);
   StatusCode ProcessVertex();
   std::vector<float> m_vtx_z     ;
   std::vector<float> m_vtx_x     ;
   std::vector<float> m_vtx_y     ;
   std::vector<int>   m_vtx_ntrk  ;



   int         m_store_PFlowNeutralCharge = 0; //1 save pflow neutral and charge particles
   int         m_store_tracks        =1; //1 then store tracks, 2 then store more details, 4 store link to truth
   std::string m_trk_container_key;
   void       InitTracks   (TTree *l_OutTree = nullptr);
   StatusCode ProcessTracks();
   int GetTrackQuality(const xAOD::TrackParticle* track,const xAOD::Vertex* pv);
   std::vector<int>   m_trk_numqual;
   std::vector<float> m_track_pt     ;
   std::vector<float> m_track_eta    ;
   std::vector<float> m_track_phi    ;
   std::vector<float> m_track_charge ;
   std::vector<int>   m_track_quality;

   std::vector<float> m_neutralPFlowObj_pt  ;
   std::vector<float> m_neutralPFlowObj_EMpt;
   std::vector<float> m_neutralPFlowObj_eta ;
   std::vector<float> m_neutralPFlowObj_phi ;
   std::vector<float> m_neutralPFlowObj_t   ;
   std::vector<float> m_neutralPFlowObj_eEM ;
   std::vector<float> m_neutralPFlowObj_HEC0;
   std::vector<float> m_neutralPFlowObj_HEC1;
   std::vector<float> m_neutralPFlowObj_HEC2;
   std::vector<float> m_neutralPFlowObj_HEC3;
   std::vector<float> m_neutralPFlowObj_EME1;
   std::vector<float> m_neutralPFlowObj_EME2;
   std::vector<float> m_neutralPFlowObj_EME3;
   std::vector<float> m_chargedPFlowObj_pt  ;
   std::vector<float> m_chargedPFlowObj_eta ;
   std::vector<float> m_chargedPFlowObj_phi ;

   std::map<const xAOD::TrackParticle*,int> m_track_index_temp;//to link muons to tracks
   std::vector<float> m_track_d0     ;
   std::vector<float> m_track_z0_wrtPV      ;
   std::vector<float> m_track_vz            ;
   std::vector<int>   m_track_Ipix_hits     ;
   std::vector<int>   m_track_Ipix_expected ;
   std::vector<int>   m_track_NIpix_hits    ;
   std::vector<int>   m_track_NIpix_expected;
   std::vector<int>   m_track_sct_hits      ;
   std::vector<int>   m_track_pix_hits      ;
   std::vector<int>   m_track_sct_holes     ;
   std::vector<int>   m_track_pix_holes     ;
   std::vector<int>   m_track_sct_dead      ;
   std::vector<int>   m_track_pix_dead      ;
   std::vector<int>   m_track_sct_shared    ;
   std::vector<int>   m_track_pix_shared    ;
   std::vector<float> m_track_chi2          ;
   std::vector<float> m_track_ndof          ;
   std::vector<unsigned long> m_track_patternRecoInfo;
   std::map<const xAOD::TruthParticle*,int> m_trk_truth_index_temp1;
   std::vector<int  > m_trk_truth_index;
   std::vector<float> m_trk_truth_prob ;
   std::vector<int  > m_trk_truth_barcode;
   std::vector<Bool_t>m_trk_truth_IsPrimary;
   bool IsPrimaryParticle(const xAOD::TruthParticle* particle);


   std::string m_met_container_key;
   void       InitMET   (TTree *l_OutTree = nullptr);
   StatusCode ProcessMET();
   std::vector<float> m_MET_sumet;


   bool        m_store_truth_Vtx     = 0; //=true then store truth vertex info
   std::string m_truth_vtx_container_key;
   void       InitTruthVertex(TTree *l_OutTree = nullptr);
   StatusCode ProcessTruthVertex();
   std::vector<float> m_truth_vtx_z;
   std::vector<float> m_truth_vtx_x;
   std::vector<float> m_truth_vtx_y;
   std::vector<float> m_truth_vtx_t;



   int         m_store_truth;        //=true then store truth track info
   double      m_min_pT_Truth;
   std::string m_truth_container_key;
   void InitTruth(TTree *l_OutTree = nullptr);
   StatusCode ProcessTruth();
   std::vector<float> m_truth_pt     ;
   std::vector<float> m_truth_eta    ;
   std::vector<float> m_truth_phi    ;
   std::vector<float> m_truth_charge ;
   std::vector<int>   m_truth_id     ;
   std::vector<int>   m_truth_barcode;
   std::vector<int>   m_truth_quality;
   std::vector<bool>  m_truth_status ;
   std::vector<std::vector<int>>   m_truth_parents;
   std::vector<int>   m_truth_trk_index ;
   std::vector<int>   m_truth_muon_index;

   std::vector<float> m_truth_mupair_asym ;
   std::vector<float> m_truth_mupair_acop ;
   std::vector<float> m_truth_mupair_kperp;
   std::vector<float> m_truth_mupair_pt   ;
   std::vector<float> m_truth_mupair_y    ;
   std::vector<float> m_truth_mupair_phi  ;
   std::vector<float> m_truth_mupair_m    ;
   std::vector<float> m_truth_mupair_pt1  ;
   std::vector<float> m_truth_mupair_eta1 ;
   std::vector<float> m_truth_mupair_phi1 ;
   std::vector<float> m_truth_mupair_ch1  ;
   std::vector<float> m_truth_mupair_bar1 ;
   std::vector<float> m_truth_mupair_id1  ;
   std::vector<float> m_truth_mupair_pt2  ;
   std::vector<float> m_truth_mupair_eta2 ;
   std::vector<float> m_truth_mupair_phi2 ;
   std::vector<float> m_truth_mupair_ch2  ;
   std::vector<float> m_truth_mupair_bar2 ;
   std::vector<float> m_truth_mupair_id2  ;


   bool        m_store_L1TE            = true; //=true then store ET info (from MET object)
   std::string m_L1TE_container_key;
   void       InitL1TE   (TTree *l_OutTree = nullptr);
   StatusCode ProcessL1TE();
   Float_t m_L1TE,m_L1TE24;


   std::string m_muons_key;
   bool m_store_single_muon;
   bool m_store_acoplanar_muon;
   void InitMuons(TTree *l_OutTree = nullptr);
   void ClearMuons();
   StatusCode ProcessMuons();
   std::vector<float> m_muon_pt     ;
   std::vector<float> m_muon_eta    ;
   std::vector<float> m_muon_phi    ;
   std::vector<float> m_muon_pt_precorr ;
   std::vector<float> m_muon_eta_precorr;
   std::vector<float> m_muon_phi_precorr;
   std::vector<int>   m_muon_qual   ;
   std::vector<float> m_muon_deltaP_overP;
   std::vector<float> m_muon_eff_corr_medium_data;
   std::vector<float> m_muon_eff_corr_medium_mc  ;
   std::vector<float> m_muon_eff_SF_medium       ;
   std::vector<float> m_muon_eff_corr_tight_data ;
   std::vector<float> m_muon_eff_corr_tight_mc   ;
   std::vector<float> m_muon_eff_SF_tight        ;
   std::vector<float> m_muon_d0     ;
   std::vector<float> m_muon_d0_err ;
   std::vector<float> m_muon_z0     ;
   std::vector<float> m_muon_trk_p  ;
   std::vector<float> m_muon_trk_pt ;
   std::vector<float> m_muon_trk_eta;
   std::vector<float> m_muon_trk_phi;
   std::vector<int>   m_muon_trk_index;
   std::vector<float> m_muon_me_p   ;
   std::vector<int>   m_muon_elosstype;
   std::vector<float> m_muon_mspt;
   std::vector<float> m_muon_msp ;
   std::vector<int  > m_muon_ms_phi_hits ;
   std::vector<int  > m_muon_ms_eta_hits ;
   std::vector<Bool_t>m_muon_trig_match;
   std::map<const xAOD::TruthParticle*,int> m_muon_truth_index_temp1;
   std::vector<int  > m_muon_truth_index;
   std::vector<float> m_muon_truth_prob ;
   std::vector<int  > m_muon_truth_barcode;
   std::vector<Bool_t>m_muon_truth_IsPrimary;

   std::vector<float> m_muon_pair_acop         ;
   std::vector<float> m_muon_pair_d0           ;
   std::vector<int>   m_muon_pair_muon1_index  ;
   std::vector<int>   m_muon_pair_muon2_index  ;
   std::vector<float> m_muon_pair_muon1_pt     ;
   std::vector<float> m_muon_pair_muon1_eta    ;
   std::vector<float> m_muon_pair_muon1_phi    ;
   std::vector<float> m_muon_pair_muon1_pt_precorr ;
   std::vector<float> m_muon_pair_muon1_eta_precorr;
   std::vector<float> m_muon_pair_muon1_phi_precorr;
   std::vector<int>   m_muon_pair_muon1_qual   ;
   std::vector<float> m_muon_pair_muon1_deltaP_overP;
   //std::vector<Bool_t>m_muon_pair_muon1_trig_match;
   std::vector<float> m_muon_pair_muon1_d0     ;
   std::vector<float> m_muon_pair_muon1_d0_err ;
   std::vector<float> m_muon_pair_muon1_z0     ;
   std::vector<int>   m_muon_pair_muon1_trk_index;
   std::vector<float> m_muon_pair_muon1_mspt;
   std::vector<float> m_muon_pair_muon1_msp ;
   std::vector<int>   m_muon_pair_muon1_elosstype;
   std::vector<float> m_muon_pair_muon1_trk_pt ;
   std::vector<float> m_muon_pair_muon1_trk_eta;
   std::vector<float> m_muon_pair_muon1_trk_phi;
   std::vector<float> m_muon_pair_muon2_pt     ;
   std::vector<float> m_muon_pair_muon2_eta    ;
   std::vector<float> m_muon_pair_muon2_phi    ;
   std::vector<float> m_muon_pair_muon2_pt_precorr ;
   std::vector<float> m_muon_pair_muon2_eta_precorr;
   std::vector<float> m_muon_pair_muon2_phi_precorr;
   std::vector<int>   m_muon_pair_muon2_qual   ;
   std::vector<float> m_muon_pair_muon2_deltaP_overP;
   //std::vector<Bool_t>m_muon_pair_muon2_trig_match;
   std::vector<float> m_muon_pair_muon2_d0     ;
   std::vector<float> m_muon_pair_muon2_d0_err ;
   std::vector<float> m_muon_pair_muon2_z0     ;
   std::vector<int>   m_muon_pair_muon2_trk_index;
   std::vector<float> m_muon_pair_muon2_mspt;
   std::vector<float> m_muon_pair_muon2_msp ;
   std::vector<int>   m_muon_pair_muon2_elosstype;
   std::vector<float> m_muon_pair_muon2_trk_pt ;
   std::vector<float> m_muon_pair_muon2_trk_eta;
   std::vector<float> m_muon_pair_muon2_trk_phi;



   std::vector<int> m_electron_trk_index;


   std::vector<Module*> m_modules;
   std::string m_HIEventShapeContainer_key;
   std::vector<std::string> m_track_jet_container_keys;


   //Alg Properties
   bool        m_use_GRL        = true ;     //=true then use GRL
   float       m_MaxZvtx        = 250.f;     //max zvtx in mm
   bool        m_StoreAllEvents = true;     //=true then store all events,=false then store events that pass atleast one of the configured triggers


   //Tree and variables to be written to tree and related stuff
   TTree *m_OutTree = nullptr;


   //Tools
   ToolHandle<IGoodRunsListSelectionTool>      m_grlTool;
   ToolHandle<Trig::TrigDecisionTool>          m_trigTool;
   ToolHandle<InDet::IInDetTrackSelectionTool> m_trkSelTool_HITight;
   ToolHandle<InDet::IInDetTrackSelectionTool> m_trkSelTool_HILoose;
   ToolHandle<InDet::IInDetTrackSelectionTool> m_trkSelTool_MinBias;
   ToolHandle<CP::IMuonSelectionTool>          m_muonSelection;
   ToolHandle<CP::IMuonCalibrationAndSmearingTool> m_muonCalibrationAndSmearingTool;
   ToolHandle<CP::IMuonEfficiencyScaleFactors    > m_effi_SF_tool_medium;
   ToolHandle<CP::IMuonEfficiencyScaleFactors    > m_effi_SF_tool_tight ;
   #ifdef __ATHENA_21p2__
     ToolHandle<Trig::IMatchingTool>             m_matchTool;
   #else
     ToolHandle<Trig::ITrigMuonMatching>         m_matchTool;
   #endif
   bool m_ApplyMuonCalibrations=false;


   ToolHandle<UPC::TopoClusterSelectionTool> m_clusterSelectionTool;

   ToolHandle<ZDC::IZdcAnalysisTool> m_ZDCAnalysisTool;

   ToolHandle<IJetCalibrationTool> m_jetCalibToolEMTopo4;
   ToolHandle<IJetCalibrationTool> m_jetCalibToolEMPFlow4;

   ToolHandle<IJetUpdateJvt> m_jvtTool;

   ToolHandle<IJetSelector> m_jetCleaningToolLoose;
   ToolHandle<IJetSelector> m_jetCleaningToolTight;
   // ToolHandle<HI::IHIPileupTool    > m_HIPileupTool;

   ToolHandle<CP::IWeightPFOTool> m_weightPFOTool;
};
#endif
