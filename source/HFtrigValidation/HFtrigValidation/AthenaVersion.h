/*
 * =====================================================================================
 *
 *       Filename:  AthenaVersion.h
 *
 *    Description:  Athena version #defs
 *
 *        Version:  1.0
 *        Created:  04/27/19 09:12:14
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Soumya Mohapatra (), 
 *   Organization:  
 *
 * =====================================================================================
 */

//Only one line should be uncommented
//#define __ATHENA_21p0__
#define __ATHENA_21p2__
  #define __DISABLE_VERTEXRECO__
