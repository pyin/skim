// -*- c++ -*-
#ifndef __VertexReco_H__
#define __VertexReco_H__
#include "HFtrigValidation/AthenaVersion.h"


//This undefines the algorithm so that we can compile it in AnalysisBase
#ifdef __DISABLE_VERTEXRECO__
#include "AthenaBaseComps/AthAlgorithm.h"
class VertexReco : public AthAlgorithm{
public:
   VertexReco(const std::string& name, ISvcLocator* pSvcLocator);
   ~VertexReco() {};


   virtual StatusCode initialize();
   virtual StatusCode execute();
   virtual StatusCode finalize();
};
#else


#include "AthenaBaseComps/AthAlgorithm.h"

#include "string"
#include "map"
#include "vector"

#include "xAODMuon/MuonContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTruth/TruthParticleContainer.h"

#include "GaudiKernel/ToolHandle.h"
#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"
#include "MuonSelectorTools/IMuonSelectionTool.h"
#include "TrkVKalVrtFitter/TrkVKalVrtFitter.h"

#include "TH1.h"
#include "TH2.h"
#include "TFile.h"



// http://acode-browser2.usatlas.bnl.gov/lxr-rel20/source/atlas/Tracking/TrkVertexFitter/TrkVKalVrtFitter/TrkVKalVrtFitter/TrkVKalVrtFitter.h
// http://acode-browser2.usatlas.bnl.gov/lxr-rel20/source/atlas/Event/xAOD/xAODTracking/xAODTracking/versions/Vertex_v1.h


class VertexReco : public AthAlgorithm{
public:
   VertexReco(const std::string& name, ISvcLocator* pSvcLocator);
   double MomentumImbalance(const xAOD::Muon* Muon);
   ~VertexReco() {};


   virtual StatusCode initialize();
   virtual StatusCode execute();
   virtual StatusCode finalize();


private:
   StatusCode CleaningCuts    (bool &pass_cleaning_cuts);
   bool IsPrimaryParticle(const xAOD::TruthParticle* particle);


   std::string m_EventInfo_key    ;
   std::string m_vtx_container_key;
   std::string m_trk_container_key;
   std::string m_truth_container_key;
   std::string m_muons_key;

	 float  m_MaxZvtx        = 250.f;     //max zvtx in mm
   int    m_pt_cut;

	 StatusCode ProcessTracks();
   int TrackQuality(const xAOD::TrackParticle *track,const xAOD::Vertex *priVtx);

	 StatusCode ProcessMuons();
   void SetBStatus(const xAOD::TruthParticle* track,
			             int b_index,std::map<const xAOD::TruthParticle*,int>& l_truth_b_index);

   void InitMuonBranches();
   void ResetMuonBranches();
   void FillMuon(const xAOD::Muon* Muon,
			           const xAOD::TruthParticle* truth,
								 const xAOD::TruthVertex* muon_prod_vertex, 
								 const xAOD::Vertex* priVtx);
   void FillMuonSV(const xAOD::TrackParticle* track,
		            const xAOD::TruthParticle* track_associated_truth,
		            const xAOD::Vertex* fit_vertex,
								const xAOD::Vertex* priVtx 
								);

   TFile *m_OutFile;
   enum{
		 NTYPE=2, //(track,track) or (muon,track)
       TRACK_TRACK=0, 
			 MUON_TRACK =1, 
		 NCLASSES_TRACK_TRACK=5, 
		   ALL_TRACK    =0,
       BOTH_FROM_PV =1,
       PV_NOT_PV    =2,
       NOT_PV_NOT_PV=3, 
       D0LT1_D0GT2  =4, 
		 NCLASSES_MUON_TRACK =4,
		   ALL_MUON_TRACK       =0,
			 B_EVENTS_ONLY        =1,
			 MUON_FROM_B          =2, 
			 MUON_AND_TRACK_FROM_B=3, 
		 NCLASSES=5,// maximum of NCLASSES_TRACK_TRACK or NCLASSES_MUON_TRACK
	 };
   TH1D* h_FitSuccess[NTYPE];
	 TH1D* h_FitMiss   [NTYPE];
   TH1D* h_ndf       [NTYPE][NCLASSES];
   TH1D* h_chi2_ndf  [NTYPE][NCLASSES];
	 TH1D* h_DZvtx     [NTYPE][NCLASSES];
	 TH1D* h_DXvtx     [NTYPE][NCLASSES];
	 TH1D* h_DYvtx     [NTYPE][NCLASSES];
	 TH1D* h_DRvtx     [NTYPE][NCLASSES];
	 TH1D* h_DTvtx     [NTYPE][NCLASSES];
   TH1D* h_muon_pt;
   TH1D* h_muon_cut;

   //Tools
   ToolHandle<InDet::IInDetTrackSelectionTool> m_trkSelTool;
   ToolHandle<CP::IMuonSelectionTool> m_muonSelection;

	 ToolHandle < Trk::IVertexFitter  >       m_fitterSvc;
	 Trk::TrkVKalVrtFitter* m_VKVFitter; 
};
#endif
#endif
