// -*- c++ -*-
#ifndef __SKIMALG3_H__
#define __SKIMALG3_H__
#include "HFtrigValidation/AthenaVersion.h"

#include "AthenaBaseComps/AthAlgorithm.h"

#include "string"
#include "map"
#include "vector"

#include "GaudiKernel/ToolHandle.h"
#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"
#include "xAODTruth/TruthParticleContainer.h"

//#include "Rtypes.h"

class TTree;

//namespace InDet {
//  class IInDetTrackSelectionTool;
//}


class SkimAlg3 : public AthAlgorithm{
public:
   SkimAlg3(const std::string& name, ISvcLocator* pSvcLocator);
   ~SkimAlg3() {};


   virtual StatusCode initialize();
   virtual StatusCode execute();
   virtual StatusCode finalize();


private:
   StatusCode CleaningCuts    (bool &pass_cleaning_cuts);


   std::string m_EventInfo_key    ;
   void       InitEventInfo   (TTree *l_OutTree = nullptr);
   StatusCode ProcessEventInfo();
   UInt_t RunNumber=0,lumi_block=0,bunch_crossing_id=0;
   ULong64_t eventNumber=0;


   std::string m_muons_key;
   void InitMuons(TTree *l_OutTree);
   StatusCode ProcessMuons();


   std::string m_vtx_container_key;
   void       InitVertex   (TTree *l_OutTree = nullptr);
   StatusCode ProcessVertex();
   std::vector<float> m_vtx_z     ;
   std::vector<float> m_vtx_x     ;
   std::vector<float> m_vtx_y     ;
   std::vector<int>   m_vtx_ntrk  ;



   std::string m_trk_container_key;
   void        InitTracks   (TTree *l_OutTree = nullptr);
   StatusCode ProcessTracks();
   std::vector<int>   m_trk_numqual;
   std::vector<float> m_track_pt     ;
   std::vector<float> m_track_eta    ;
   std::vector<float> m_track_phi    ;
   std::vector<float> m_track_charge ;
   std::vector<int>   m_track_quality;
   std::vector<float> m_track_d0     ;
   std::vector<float> m_track_z0_wrtPV ;
   std::vector<float> m_trk_truth_prob ;
   std::vector<int  > m_trk_truth_index;
   std::map<const xAOD::TruthParticle*,int> m_trk_truth_index_temp1;
   std::vector<Bool_t>m_trk_truth_IsPrimary;
   bool IsPrimaryParticle(const xAOD::TruthParticle* particle);



   std::string m_truth_vtx_container_key;
   void       InitTruthVertex(TTree *l_OutTree = nullptr);
   StatusCode ProcessTruthVertex();
   std::vector<float> m_truth_vtx_z;
   std::vector<float> m_truth_vtx_x;
   std::vector<float> m_truth_vtx_y;
   std::vector<float> m_truth_vtx_t;



   std::string m_truth_container_key;
   void InitTruth(TTree *l_OutTree = nullptr);
   StatusCode ProcessTruth(bool &fill);
   void AddDescendants(const xAOD::TruthParticle* track, int& truth_index,  int& b_index);
   void AddTruthTrack (const xAOD::TruthParticle* track, int& truth_index,  int& b_index);
   std::map<const xAOD::TruthParticle*, bool>m_truth_particles;
   std::vector<float> m_truth_pt       ;
   std::vector<float> m_truth_eta      ;
   std::vector<float> m_truth_phi      ;
   std::vector<float> m_truth_charge   ;
   std::vector<int>   m_truth_id       ;
   std::vector<int>   m_truth_barcode  ;
   std::vector<int>   m_truth_quality  ;
   std::vector<int>   m_truth_b_index  ;
   std::vector<int>   m_truth_status   ;
   std::vector<int>   m_truth_trk_index;




   //Alg Properties
   float       m_MaxZvtx        = 250.f;     //max zvtx in mm

   //Tree and variables to be written to tree and related stuff
   TTree *m_OutTree = nullptr;


   //Tools
   ToolHandle<InDet::IInDetTrackSelectionTool> m_trkSelTool;
};
#endif
