#ifndef __OfflineOnlineComparisons_H__
#define __OfflineOnlineComparisons_H__
#include "HFtrigValidation/AthenaVersion.h"

#include "string"
#include "map"
#include "vector"

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"
#include "TrigDecisionTool/TrigDecisionTool.h"


#ifdef __ATHENA_21p2__
  #include "AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h"
#else
  #include "GoodRunsLists/IGoodRunsListSelectionTool.h"
#endif

#include <xAODMuon/Muon.h>
#include <xAODMuon/MuonContainer.h>
#include "xAODTracking/Vertex.h"
#include "xAODTracking/VertexContainer.h"


class TFile;
class TH1D;
class TH2D;

class OfflineOnlineComparisons : public AthAlgorithm{
public:
   /** Standard Athena-Algorithm Constructor */
   OfflineOnlineComparisons(const std::string& name, ISvcLocator* pSvcLocator);
   /** Default Destructor */
   ~OfflineOnlineComparisons() {};

   enum{
     NTYPE=2,
      OFFLINE=0,
      ONLINE= 1,
     NQUALITY=2,
      MEDIUM=0,
      TIGHT =1,
   };
   std::string m_MuonTypeName[NTYPE]={"Offline","Online"};


   virtual StatusCode initialize();
   virtual StatusCode execute();
   virtual StatusCode finalize();


private: 
   enum{
     NPT1=9,
   };
   double PtBins[NPT1]={0,1,2,3,4,5,6,8,10};
   
   enum{
     NETA=11,
   };
   double EtaBins[NETA]={-2.5,-2,-1.5,-1,-0.5,0,0.5,1,1.5,2,2.5};
   
   StatusCode CleaningCuts   (bool &pass_cleaning_cuts);

   //Alg Properties
   std::string m_EventInfo_key    ;
   std::string m_muons_offline_key;
   std::string m_muons_online_key ;
   std::string m_tracks_key       ;
   std::string m_vertices_key     ;
   std::vector<std::string> m_jets_keys;

   std::string m_CollisionSystemType;//"pp","pA","AA"
   std::string m_DataType;           //"Data","MC"

   bool        m_use_trigger;        //=true then use the Trigger (make branches, use decesion)
   std::string m_Trigger_Chains;     //Trigger Chains for TrigDecesion and branches
   float       m_MaxZvtx;            //max zvtx in mm
   bool        m_use_GRL;            //=true then use GRL

   //Tools
   ToolHandle<IGoodRunsListSelectionTool> m_grlTool;
   ToolHandle<Trig::TrigDecisionTool>     m_trigTool;

   const xAOD::MuonContainer  *m_muons_offline=nullptr;
   const xAOD::MuonContainer  *m_muons_online =nullptr;
   const xAOD::VertexContainer*m_vertices     =nullptr;
   const xAOD::TrackParticleContainer* m_tracks=nullptr;


   TFile *m_OutFile;

   bool DeltaPOverP(const xAOD::Muon *Muon,double &deltaP_overP);

   StatusCode SingleMuonDistributions();
   TH1D* h_muon_cutflow                [NTYPE][NQUALITY];
   TH2D* h_Eloss                       [NTYPE][NQUALITY];
   TH2D* h_MomentumImbalance           [NTYPE][NQUALITY][NETA];
   TH2D* h_MomentumImbalance2          [NTYPE][NQUALITY][NETA];
   TH2D* h_MomentumImbalance_comparison[NTYPE][NQUALITY][NPT1][NETA];


   StatusCode OfflineOnlineCorrelations();
   TH1D* h_NMatch;
   TH1D* h_NAll;
   TH2D* h_Match_DeltaR;
   TH2D* h_Match_DeltaR_cut;
   TH2D* h_Match_DeltapT;
   TH2D* h_Match_pT;
   TH2D* h_Match_ElossCorr;
   TH2D* h_Match_DeltaPOverPCorr;
   
   TH2D* h_Match_ElossCorr_ptbinned      [NPT1];
   TH2D* h_Match_DeltaPOverPCorr_ptbinned[NPT1];
   
   TH2D* h_Match_ElossCorr_etabinned      [NETA];
   TH2D* h_Match_DeltaPOverPCorr_etabinned[NETA];
};

#endif
