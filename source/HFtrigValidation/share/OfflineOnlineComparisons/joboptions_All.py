#All Settings (Default)
m_EvtMax=-1
m_DataSource         ='data'
m_TriggerTool        =True

m_EventInfoKey            ="EventInfo"
m_MuonOfflineContainerKey ="Muons"
m_MuonOnlineContainerKey  ="HLT_xAOD__MuonContainer_MuonEFInfo"
m_TrkContainerKey         ="InDetTrackParticles"
m_VtxContainerKey         ="PrimaryVertices"

m_DataType           ="Data"
m_MaxZvtx            =300                   #           1)max |zvtx| for primary zvtx
m_UseGRL             =True                  #           1)Ignore GRL if false
GRL_XML              ="merged_grl.xml"      #           1)XML File for GRL

#changes for PbPb Data
#m_EvtMax=10
m_InputFile="links/*root*"
#m_InputFile="links/29.root"


##Changes for for Pb+Pb MC
#m_EvtMax=-1
#dataSource           ='geant4'
#m_InputFile          ="/atlas/work/shared/TestSamples2/mc15_5TeV.420000.Hijing_PbPb_5p02TeV_MinBias_Flow_JJFV6.recon.AOD.e3754_s2633_r7232_tid06826513_00/*"
#m_UseGRL             =False
#m_TriggerTool        =False







import AthenaPoolCnvSvc.ReadAthenaPool
import glob
svcMgr.EventSelector.InputCollections = glob.glob(m_InputFile)


theApp.EvtMax = m_EvtMax


from AthenaCommon.GlobalFlags import globalflags
globalflags.DataSource.set_Value_and_Lock(m_DataSource) #data geant4


svcMgr += CfgMgr.AthenaEventLoopMgr(EventPrintoutInterval=1)


if m_TriggerTool==True :
     #TriggerConfigGetter looks at recflags, so it might be a good idea to set those, but I have found not to be always necessary
     from RecExConfig.RecFlags  import rec
     rec.readRDO = False #readRDO is the True one by default. Turn it off before turning others on
     rec.readAOD = True #example for reading AOD

     #need this bit for AOD because AOD doesn't contain all trigger info - so has to connect to DB for some of it
     #ESD/RDO appear not to need this. Only do this bit if running on data, not MC
     # from AthenaCommon.GlobalFlags import globalflags
     # globalflags.DataSource = 'data'

     #every situation needs the next bit
     from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
     athenaCommonFlags.FilesInput = svcMgr.EventSelector.InputCollections
     from TriggerJobOpts.TriggerFlags import TriggerFlags
     TriggerFlags.configurationSourceList=['ds']
     from TriggerJobOpts.TriggerConfigGetter import TriggerConfigGetter
     cfg = TriggerConfigGetter()


#the main algorithm sequence
algSeq = CfgMgr.AthSequencer("AthAlgSeq")

#The GRLTool
MyGRLTool=CfgMgr.GoodRunsListSelectionTool("MyGRLTool")
MyGRLTool.GoodRunsListVec=[GRL_XML]
MyGRLTool.PassThrough    =True
ToolSvc += MyGRLTool

TrkSelTool=CfgMgr.InDet__InDetTrackSelectionTool("TrackSelectionTool",
                                                 CutLevel = "MinBias",
                                                 minPt = 400.)
ToolSvc += TrkSelTool





#algorithm to fill the Validation hists
MyAlg=CfgMgr.OfflineOnlineComparisons()
MyAlg.OutputLevel             =VERBOSE
#from AthenaCommon.AppMgr import ServiceMgr as svcMgr
#svcMgr.MessageSvc.defaultLimit = 9999999


MyAlg.EventInfoKey            =m_EventInfoKey
MyAlg.MuonOfflineContainerKey =m_MuonOfflineContainerKey
MyAlg.MuonOnlineContainerKey  =m_MuonOnlineContainerKey
MyAlg.TrkContainerKey         =m_TrkContainerKey
MyAlg.VtxContainerKey         =m_VtxContainerKey

MyAlg.DataType                =m_DataType
MyAlg.MaxZvtx                 =m_MaxZvtx             #           1)max zvtx for primary zvtx
MyAlg.GRLTool                 =MyGRLTool             #           1)GRL Tool 
MyAlg.UseGRL                  =m_UseGRL              #           1)Ignore GRL if false (automatically false for MC)

algSeq += MyAlg

#svcMgr += CfgMgr.THistSvc()
#svcMgr.THistSvc.Output += ["MYSTREAM DATAFILE='myfile.root' OPT='RECREATE'"]


