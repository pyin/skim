MAXJOBS=501
MAXFILES=3500
FILESPERJOB=7

rm log/*
rm output/*
#MAXJOBS=1
for JOB in `seq 0 ${MAXJOBS}`
do
  FirstFile=$((JOB*FILESPERJOB))
  LastFile=$(($FirstFile+$FILESPERJOB))
  LastFile=$(($LastFile-1))

  if (($FirstFile>=MAXFILES))
  then
    break
  fi

  if (($LastFile>=MAXFILES))
  then
    LastFile=$(($MAXFILES-1))
  fi

  echo "$JOB $FirstFile $LastFile"
  file=c_JOB$JOB.sub



  dir=`pwd`                        
  echo "Initialdir      = $dir" >$file
  echo "Universe        = vanilla" >>$file
  echo "GetEnv          = true" >>$file
  echo "" >>$file
  echo "" >>$file
                
  echo "Executable      = \$(Initialdir)/run.sh" >>$file
  echo "" >>$file
  echo "" >>$file
                
  echo "FirstFile           = $FirstFile" >>$file
  echo "LastFile            = $LastFile" >>$file
  echo "" >>$file
  echo "" >>$file
                
  echo "Output          = \$(Initialdir)/log/$file.out" >>$file
  echo "Error           = \$(Initialdir)/log/$file.err" >>$file
  echo "Log             = \$(Initialdir)/log/$file.log" >>$file
  echo "" >>$file
  echo "" >>$file
                
  echo "Arguments       = \"\$(FirstFile) \$(LastFile) \"" >>$file
  echo "accounting_group = group_atlas.columbia"  >>$file
  echo "queue" >>$file
  condor_submit $file
  rm $file
done
