#All Settings (Default)
m_EvtMax=-1
dataSource           ='geant4'
#m_InputFile          ="links/*.root"
m_InputFile          ="links/0.root"





import AthenaPoolCnvSvc.ReadAthenaPool
import glob
svcMgr.EventSelector.InputCollections = glob.glob(m_InputFile)


theApp.EvtMax = m_EvtMax


from AthenaCommon.GlobalFlags import globalflags
globalflags.DataSource.set_Value_and_Lock(dataSource) #data geant4


svcMgr += CfgMgr.AthenaEventLoopMgr(EventPrintoutInterval=1)


#the main algorithm sequence
algSeq = CfgMgr.AthSequencer("AthAlgSeq")


# Create a MuonSelectionTool if we do not yet have one 
from AthenaCommon.AppMgr import ToolSvc
if not hasattr(ToolSvc, "MyMuonSelectionTool"):
    from MuonSelectorTools.MuonSelectorToolsConf import CP__MuonSelectionTool
    ToolSvc += CP__MuonSelectionTool("MyMuonSelectionTool")
    ToolSvc.MyMuonSelectionTool.MaxEta = 2.5 


#algorithm to fill the Validation hists
MuonTypesAlg=CfgMgr.MuonTypes()
MuonTypesAlg.OutputLevel             =VERBOSE
MuonTypesAlg.MuonSelectionTool       =ToolSvc.MyMuonSelectionTool
algSeq += MuonTypesAlg

