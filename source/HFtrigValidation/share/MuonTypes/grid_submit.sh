#pathena joboptions_MC.py \
#        --athenaTag 20.7.9.7 \
#        --inDsTxt InDs_MC_pPb_Jet.txt \
#        --outDS user.soumya.MuonTypes.pPb_Jet. \
#        --excludeFile=links,MuonTree.root,Condor,vim_backup,figs \
#        --extOutFile MuonTree.root \
#        --mergeOutput \
#        --skipScout 

#        --inDsTxt InDs_MC.txt \
#        --outDS user.soumya.MuonTypes.PP. \

pathena joboptions_MC.py \
        --athenaTag AtlasProduction,21.0.20.1 \
        --inDsTxt InDs_MC_pPb_Jet.txt \
        --outDS user.soumya.MuonTypes.pPb_Jet.05Dec2018. \
        --excludeFile=links,MuonTree.root,Condor,vim_backup,figs \
        --extOutFile MuonTree.root \
        --mergeOutput \
        --skipScout 

pathena joboptions_MC.py \
        --athenaTag AtlasProduction,21.0.20.1 \
        --inDsTxt InDs_MC.txt \
        --outDS user.soumya.MuonTypes.PP.05Dec2018. \
        --excludeFile=links,MuonTree.root,Condor,vim_backup,figs \
        --extOutFile MuonTree.root \
        --mergeOutput \
        --skipScout 

pathena joboptions_MC.py \
        --athenaTag AtlasProduction,21.0.20.1 \
        --inDsTxt InDs_MC_pPb_Muon.txt \
        --outDS user.soumya.MuonTypes.pPb_Muon.05Dec2018. \
        --excludeFile=links,MuonTree.root,Condor,vim_backup,figs \
        --extOutFile MuonTree.root \
        --mergeOutput \
        --skipScout 
