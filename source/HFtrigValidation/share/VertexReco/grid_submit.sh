pathena joboptions_MC.py \
        --athenaTag 20.7.9.3 \
        --inDsTxt InDs_MC.txt \
        --outDS user.soumya.VertexReco. \
        --excludeFile=links,output_ptmin400.root,Condor,vim_backup,figs \
        --extOutFile output_ptmin400.root \
        --mergeOutput \
        --skipScout 

