#!/bin/zsh

ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup asetup


dir=`pwd`
cd ../../../..
asetup 20.7.9.2,here
cd $dir

InputFiles="'"links\\/$1.root"'"
firstfile=$(($1+1))
for File in `seq ${firstfile} ${2}`
do
	InputFiles=$InputFiles,"'"links\\/$File.root"'"
done
echo $InputFiles

rm -rf $1
mkdir $1
cd $1
file=joboptions_MC_FILE${1}.py 
cp ../joboptions_MC.py $file

sed -i "/links/d" $file
sed -i "s/InputCollections = glob\.glob(m_InputFile)/InputCollections = \[$InputFiles\]/" $file

ln -s ../../links
athena $file
mv output_ptmin400.root ../output/output_ptmin400_${1}.root
cd ../
rm -rf $1


