#All Settings (Default)
m_EvtMax=-1
m_EventInfoKey            ="EventInfo"
m_TrkContainerKey         ="InDetTrackParticles"
m_VtxContainerKey         ="PrimaryVertices"
dataSource           ='geant4'
#m_InputFile          ="links/*.root"
#m_InputFile          ="links/0.root"
m_InputFile          ="TestAndrzej/DAOD_TRUTH0.test.pool.root"





import AthenaPoolCnvSvc.ReadAthenaPool
import glob
svcMgr.EventSelector.InputCollections = glob.glob(m_InputFile)


theApp.EvtMax = m_EvtMax


from AthenaCommon.GlobalFlags import globalflags
globalflags.DataSource.set_Value_and_Lock(dataSource) #data geant4


svcMgr += CfgMgr.AthenaEventLoopMgr(EventPrintoutInterval=1)


#the main algorithm sequence
algSeq = CfgMgr.AthSequencer("AthAlgSeq")


# Create a MuonSelectionTool if we do not yet have one 
from AthenaCommon.AppMgr import ToolSvc
if not hasattr(ToolSvc, "MyMuonSelectionTool"):
    from MuonSelectorTools.MuonSelectorToolsConf import CP__MuonSelectionTool
    ToolSvc += CP__MuonSelectionTool("MyMuonSelectionTool")
    ToolSvc.MyMuonSelectionTool.MaxEta = 2.5 


#track selector tool
TrkSelTool=CfgMgr.InDet__InDetTrackSelectionTool("TrackSelectionTool",
                                                 CutLevel = "MinBias",
                                                 minPt = 100.)
ToolSvc += TrkSelTool





#algorithm to fill the Validation hists
VertexRecoAlg=CfgMgr.VertexReco()
VertexRecoAlg.OutputLevel             =VERBOSE
VertexRecoAlg.EventInfoKey            =m_EventInfoKey
VertexRecoAlg.TrkContainerKey         =m_TrkContainerKey
VertexRecoAlg.VtxContainerKey         =m_VtxContainerKey
VertexRecoAlg.TrackSelectionTool      =TrkSelTool            #           1)MinBias track selection tool
VertexRecoAlg.MuonSelectionTool       =ToolSvc.MyMuonSelectionTool
algSeq += VertexRecoAlg

