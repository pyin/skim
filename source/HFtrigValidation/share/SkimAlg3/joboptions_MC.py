#All Settings (Default)
m_EvtMax=-1

#m_EventInfoKey            ="EventInfo"
#m_TrkContainerKey         ="InDetTrackParticles"
#m_VtxContainerKey         ="PrimaryVertices"
#m_TruthVtxContainerKey    ="TruthVertices"
#m_TruthContainerKey       ="TruthParticles"

m_EventInfoKey            ="EventInfo"
m_TruthContainerKey       ="TruthParticles"
m_TrkContainerKey=""
m_VtxContainerKey=""
m_TruthVtxContainerKey=""

m_EvtMax=-1
dataSource           ='geant4'
#m_InputFile          ="links/*.root"
#m_InputFile          ="links/0.root"
m_InputFile          ="TestAndrzej/DAOD_TRUTH0.test.pool.root"


import AthenaPoolCnvSvc.ReadAthenaPool
import glob
svcMgr.EventSelector.InputCollections = glob.glob(m_InputFile)


theApp.EvtMax = m_EvtMax


from AthenaCommon.GlobalFlags import globalflags
globalflags.DataSource.set_Value_and_Lock(dataSource) #data geant4


svcMgr += CfgMgr.AthenaEventLoopMgr(EventPrintoutInterval=1)


#the main algorithm sequence
algSeq = CfgMgr.AthSequencer("AthAlgSeq")

TrkSelTool=CfgMgr.InDet__InDetTrackSelectionTool("TrackSelectionTool",
                                                 CutLevel = "MinBias",
                                                 minPt = 100.)
ToolSvc += TrkSelTool





#algorithm to fill the Validation hists
SkimAlg3Alg=CfgMgr.SkimAlg3()
SkimAlg3Alg.EventInfoKey            =m_EventInfoKey
SkimAlg3Alg.TrkContainerKey         =m_TrkContainerKey
SkimAlg3Alg.VtxContainerKey         =m_VtxContainerKey
SkimAlg3Alg.TruthVtxContainerKey    =m_TruthVtxContainerKey
SkimAlg3Alg.TruthContainerKey       =m_TruthContainerKey
SkimAlg3Alg.TrackSelectionTool      =TrkSelTool            #           1)MinBias track selection tool
algSeq += SkimAlg3Alg

svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["MYSTREAM DATAFILE='myfile.root' OPT='RECREATE'"]


