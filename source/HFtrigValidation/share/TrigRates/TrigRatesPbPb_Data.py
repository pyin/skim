#All Settings (Default)
m_EvtMax    =100
dataSource  ='data'
InputFile   ="/eos/atlas/atlastier0/tzero/prod/data18_hi/express_express/*/data18_hi.00*.express_express.recon.AOD.x586/*"


import AthenaPoolCnvSvc.ReadAthenaPool
import glob
svcMgr.EventSelector.InputCollections = glob.glob(InputFile)
theApp.EvtMax = m_EvtMax


from AthenaCommon.GlobalFlags import globalflags
globalflags.DataSource.set_Value_and_Lock(dataSource) #data geant4
svcMgr += CfgMgr.AthenaEventLoopMgr(EventPrintoutInterval=1)
globalflags.DatabaseInstance = 'CONDBR2' # e.g. if you want run 2 data,  set to COMP200 if you want run1. This is completely ignored in MC.
#globalflags.DatabaseInstance = 'COMP200' # e.g. if you want run 2 data,  set to COMP200 if you want run1. This is completely ignored in MC.


#the tool service
from AthenaCommon.AppMgr import ToolSvc


#Configure Track selection tool
##------------------------------------------------------------
ToolSvc += CfgMgr.InDet__InDetTrackSelectionTool("TrackSelectionTool",CutLevel="MinBias",minPt=100.)
##------------------------------------------------------------



# Create a MuonSelectionTool if we do not yet have one 
##------------------------------------------------------------
from AthenaCommon.AppMgr import ToolSvc
if not hasattr(ToolSvc, "MyMuonSelectionTool"):
    from MuonSelectorTools.MuonSelectorToolsConf import CP__MuonSelectionTool
    ToolSvc += CP__MuonSelectionTool("MyMuonSelectionTool")
    ToolSvc.MyMuonSelectionTool.MaxEta = 2.5
##------------------------------------------------------------



# Create muon trigger matching tool
##------------------------------------------------------------
from TrigMuonMatching.TrigMuonMatchingConf import Trig__TrigMuonMatching
ToolSvc += Trig__TrigMuonMatching("MyMuonTriggerMatchTool")
##------------------------------------------------------------



#The GRLTool
##------------------------------------------------------------
MyGRLTool=CfgMgr.GoodRunsListSelectionTool("MyGRLTool")                                                             
MyGRLTool.GoodRunsListVec=["LB_collection_PbPb2018.xml"]
#MyGRLTool.PassThrough    =True                                                                                      
ToolSvc += MyGRLTool  
##------------------------------------------------------------



MessageSvc.defaultLimit = 1000

#the main algorithm sequence
algSeq = CfgMgr.AthSequencer("AthAlgSeq")

#main algorithm to run
TrigRatesAlg=CfgMgr.TrigRates("TrigRatesAlg",OutputLevel=INFO)

TrigRatesAlg.TrackSelectionTool  =ToolSvc.TrackSelectionTool
TrigRatesAlg.MuonSelectionTool   =ToolSvc.MyMuonSelectionTool
TrigRatesAlg.MuonTriggerMatchTool=ToolSvc.MyMuonTriggerMatchTool
TrigRatesAlg.GRLTool             =ToolSvc.MyGRLTool

MinBias_triggers=["HLT_hi_hipeb_L1TE600","HLT_mb_sptrk_hipeb_L1ZDC_A_C_VTE50","HLT_hi_hipeb_L1TE50_VTE600","HLT_noalg_cc_L1TE600.0ETA49","HLT_mb_sptrk_L1ZDC_A_C_VTE50","HLT_noalg_pc_L1TE50_VTE600.0ETA49 "]
Muon_triggers   =["HLT_mu4","HLT_mu6","HLT_mu8","HLT_mu10","HLT_mu14"]
Di_Muon_triggers=["HLT_2mu3","HLT_2mu4"]
All_trigs="|".join(MinBias_triggers+Muon_triggers+Di_Muon_triggers)
print(All_trigs)

TrigRatesAlg.UseGRL            =True
TrigRatesAlg.StoreAllEvents    =False
TrigRatesAlg.UseTrigger        =True
TrigRatesAlg.TriggerChains     =All_trigs
TrigRatesAlg.StoreEventInfo    =1
TrigRatesAlg.StoreTracks       =True
TrigRatesAlg.StoreVtx          =True
TrigRatesAlg.StoreL1TE         =False
TrigRatesAlg.StoreSingleMuon   =True
TrigRatesAlg.StoreAcoplanarMuon=True
algSeq += TrigRatesAlg



svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["MYSTREAM DATAFILE='myfile.root' OPT='RECREATE'"]

