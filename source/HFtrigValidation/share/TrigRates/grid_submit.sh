#2018 Pb+Pb data
#pathena TrigRates.py \
#        --athenaTag AthAnalysis,21.2.70 \
#        --inDsTxt InDstxt_PbPb2018_HP.txt \
#        --outDS user.soumya.TrigRates.physics_HP.PbPb2018.27July2019. \
#        --excludeFile=myfile.root \
#        --excludeFile=clean.sh \
#        --excludeFile=vim_backup

#2015 Pb+Pb data
#pathena TrigRates.py \
#        --athenaTag AthAnalysis,21.2.70 \
#        --inDsTxt InDstxt_PbPb2015_HP.txt \
#        --outDS user.soumya.TrigRates.physics_HP.PbPb2015.27July2019. \
#        --excludeFile=myfile.root \
#        --excludeFile=clean.sh \
#        --excludeFile=vim_backup \
#        --mergeOutput

#Minbias 2018 for trig effs
#pathena TrigRates.py \
#        --athenaTag AthAnalysis,21.2.70 \
#        --inDsTxt InDstxt_PbPb2018_MinBias.txt \
#        --outDS user.soumya.TrigRates.physics_MinBias.PbPb2018.15May2019. \
#        --excludeFile=myfile.root \
#        --excludeFile=clean.sh \
#        --excludeFile=vim_backup

#STARLight MC
#pathena TrigRates.py \
#        --athenaTag AtlasProduction,21.0.20.1 \
#        --inDS mc16_5TeV.420051.Starlight_r193_gammagamma2mumu_breakupMode5_filter.recon.AOD.e4990_s3428_r11320_tid17610562_00 \
#        --outDS user.soumya.TrigRates.StarLight.07April2019.merge. \
#        --excludeFile=myfile.root \
#        --excludeFile=clean.sh \
#        --excludeFile=vim_backup \
#        --mergeOutput 

#2016 p+Pb data
#pathena TrigRates.py \
#        --athenaTag AthAnalysis,21.2.70 \
#        --inDsTxt InDstxt_pPb2016_8TeV_Main.txt \
#        --outDS user.soumya.TrigRates.physics_Main.pPb2016_8TeV. \
#        --excludeFile=myfile.root \
#        --excludeFile=clean.sh \
#        --excludeFile=vim_backup \
#        --mergeOutput

#2017 PP 5 TeV data
pathena TrigRates.py \
        --athenaTag=AthAnalysis,21.2.70 \
        --inDsTxt InDstxt_PP2017_5TeV_reference.txt  \
        --outDS user.soumya.TrigRates.physics_Main.PP2017_5TeV.27November2019.v3 \
        --excludeFile=myfile.root,clean.sh,vim_backup,2015,2018,MC_NoOverlay,MC_Overlay,MC_Overlay2,Plots \
        --mergeOutput
