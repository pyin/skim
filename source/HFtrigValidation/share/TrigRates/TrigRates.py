do_ppMinBias                =False #For tracks only; muons should always be unused
do_pp2017_13TeV             =False
do_pp2017_5TeV              =False
do_hi2018                   =False
do_hi2018_AcoplanarMuons    =False
do_hi2015_AcoplanarMuons    =False
do_hi2018_AcoplanarMuons_PEB=False
do_hi2018_AcoplanarMuons_MB =False
do_ppMC13TeV                =False
do_HI_MC_OVERLAY            =True
do_pPb2016_8TeV             =False


#------------------------------------------------------------
m_EvtMax    =-1
dataSource  ='data'
GRL=[]
MinBias_triggers=[]
Muon_triggers   =[]
DiMuon_triggers =[]
if do_hi2018:
  GRL=["LB_collection_PbPb2018.xml"]
  InputFile   ="/atlas/work/shared/JobTestData/data18_hi/data18_hi.00366142.physics_HardProbes.merge.AOD.f1027_m2037._lb0570._0003.1"
  MinBias_triggers=["HLT_hi_hipeb_L1TE600",
                    "HLT_mb_sptrk_hipeb_L1ZDC_A_C_VTE50",
                    "HLT_hi_hipeb_L1TE50_VTE600",
                    "HLT_noalg_cc_L1TE600.0ETA49",
                    "HLT_mb_sptrk_L1ZDC_A_C_VTE50",
                    "HLT_noalg_pc_L1TE50_VTE600.0ETA49"
                   ]
  Muon_triggers   =["HLT_mu4","HLT_mu6","HLT_mu8","HLT_mu10","HLT_mu14"]
elif do_hi2018_AcoplanarMuons:
  GRL=["LB_collection_PbPb2018.xml"]
  InputFile   ="/atlas/work/shared/JobTestData/data18_hi/data18_hi.00366142.physics_HardProbes.merge.AOD.f1027_m2037._lb0570._0003.1"
  #InputFile   ="/migration/atlas01scratch/soumya/user.soumya.HION2.Data2018_HP.08Aug2019.2._EXT0/user.soumya.18819873.EXT0._016562.DAOD_HION2.output.pool.root"
  DiMuon_triggers =["HLT_2mu3","HLT_2mu4","HLT_2mu6","HLT_2mu8","HLT_mu4_mu4noL1"]
elif do_hi2015_AcoplanarMuons:
  GRL=["LB_collection_PbPb2015.xml"]
  InputFile   ="/atlas/work/shared/JobTestData/data15_hi/AOD.16615737._000065.pool.root.1"
  DiMuon_triggers =["HLT_2mu3","HLT_2mu4","HLT_2mu6","HLT_2mu8","HLT_mu4_mu4noL1"]
elif do_hi2018_AcoplanarMuons_PEB:
  GRL=["LB_collection_PbPb2018.xml"]
  InputFile   ="/atlas/work/soumya/AcoplanarMuon/data18_hi/AOD.16205981._001088.pool.root.1"
  MinBias_triggers=["HLT_hi_hipeb_L1TE600","HLT_mb_sptrk_hipeb_L1ZDC_A_C_VTE50","HLT_hi_hipeb_L1TE50_VTE600"]
  Muon_triggers   =["HLT_mu4","HLT_mu3"]
  DiMuon_triggers =["HLT_2mu3","HLT_2mu4","HLT_2mu6","HLT_2mu8","HLT_mu4_mu4noL1"]
elif do_hi2018_AcoplanarMuons_MB:
  GRL=["LB_collection_PbPb2018.xml"]
  InputFile   ="/atlas/work/soumya/AcoplanarMuon/data18_hi_CC/data18_hi.00365678.physics_CC.merge.AOD.f1021_m2037._lb0346._0001.1"
  MinBias_triggers=["HLT_noalg_cc_L1TE600.0ETA49"," HLT_mb_sptrk_L1ZDC_A_C_VTE50","HLT_noalg_pc_L1TE50_VTE600.0ETA49"]
  Muon_triggers   =["HLT_mu4","HLT_mu3","L1_MU4","HLT_noalg_L1MU4"]
  DiMuon_triggers =["HLT_2mu3","HLT_2mu4","HLT_2mu6","HLT_2mu8","HLT_mu4_mu4noL1"]
elif do_pp2017_13TeV:
  GRL=["ppmu2_GRL.xml"]
  InputFile   ="/atlas/work/shared/JobTestData/data17_13TeV/AOD.13676295._000370.pool.root.1"
  MinBias_triggers        =["HLT_mb_sptrk", 
                            "HLT_mb_sp700_pusup350_trk50_hmt_L1TE10", 
                            "HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20", 
                            "HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30", 
                            "HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40", 
                            "HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50"]
  Muon_triggers           =["HLT_mu4_L1MU4", 
                            "HLT_mu4_mb_sp600_pusup300_trk40_hmt_L1MU4_TE10", 
                            "HLT_mu4_mb_sp1100_pusup450_trk70_hmt_L1MU4_TE20", 
                            "HLT_mu4_mb_sp1200_pusup500_trk80_hmt_L1MU4_TE30", 
                            "HLT_mu4_mb_sp1400_pusup550_trk90_hmt_L1MU4_TE40"]
  DiMuon_triggers         =[]
elif do_ppMinBias:
  GRL=["AllLowMu13TeVRuns.xml"]
  InputFile   ="/atlas/work/shared/JobTestData/AllLowMuRuns/01--06_267358_f597_m1441/data15_13TeV.00267358.physics_MinBias.merge.AOD.f597_m1441._lb0190._0003.2"
  MinBias_triggers        =[
                            "HLT_mb_sptrk",
                            "HLT_noalg_mb_L1MBTS_1",
                            "HLT_noalg_mb_L1MBTS_1_1",
                            "HLT_mb_sp400_trk40_hmt_L1MBTS_1_1",
                            "HLT_mb_sp600_trk40_hmt_L1TE5",
                            "HLT_mb_sp600_trk40_hmt_L1TE10",
                            "HLT_mb_sp700_trk50_hmt_L1MBTS_1_1"
                            "HLT_mb_sp700_trk50_hmt_L1TE5",
                            "HLT_mb_sp900_trk50_hmt_L1TE5",
                            "HLT_mb_sp700_trk50_hmt_L1TE10",
                            "HLT_mb_sp700_trk50_hmt_L1TE20",
                            "HLT_mb_sp900_trk60_hmt_L1MBTS_1_1",
                            "HLT_mb_sp900_trk60_hmt_L1TE5",
                            "HLT_mb_sp900_pusup400_trk60_hmt_L1TE5",
                            "HLT_mb_sp900_trk60_hmt_L1TE10",
                            "HLT_mb_sp900_trk60_hmt_L1TE20",
                            "HLT_mb_sp1000_trk70_hmt_L1TE5",
                            "HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5",
                            "HLT_mb_sp1100_trk70_hmt_L1TE10",
                            "HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10.0ETA24",
                            "HLT_mb_sp1000_trk70_hmt_L1TE10.0ETA24",
                            "HLT_mb_sp1100_trk70_hmt_L1TE20",
                            "HLT_mb_sp1100_trk70_hmt_L1TE30",
                            "HLT_mb_sp1200_trk80_hmt_L1TE20",
                            "HLT_mb_sp1200_trk80_hmt_L1TE30",
                            "HLT_mb_sp1400_trk90_hmt_L1TE5",
                            "HLT_mb_sp1400_trk90_hmt_L1TE10",
                            "HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10",
                            "HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10.0ETA24",
                            "HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20.0ETA24",
                            "HLT_mb_sp1400_trk90_hmt_L1TE20",
                            "HLT_mb_sp1400_trk90_hmt_L1TE20.0ETA24",
                            "HLT_mb_sp1400_trk90_hmt_L1TE30",
                            "HLT_mb_sp1400_trk90_hmt_L1TE40",
                            "HLT_mb_sp1600_trk100_hmt_L1TE40",
                            "HLT_mb_sp1600_trk100_hmt_L1TE50",
                            "HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30",
                            "HLT_mb_sp1700_trk110_hmt_L1TE50",
                            "HLT_mb_sp1900_trk120_hmt_L1TE40",
                            "HLT_mb_sp1900_trk120_hmt_L1TE50",
                            "HLT_mb_sp1900_trk120_hmt_L1TE70"
                            ] 
elif do_ppMC13TeV:
  dataSource  ='geant4'
  InputFile   ="/atlas/work/shared/JobTestData/MC_13TeV/links/0000.root"
elif do_pp2017_5TeV:
  GRL=["data17_5TeV.periodAllYear_DetStatus-v98-pro21-16_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml"]
  InputFile   ="/atlas/work/shared/JobTestData/data17_5TeV.periodM.physics_Main.PhysCont.AOD.pro23_v02/AOD.17352905._000740.pool.root.1"
  DiMuon_triggers =["HLT_2mu4"]
elif do_HI_MC_OVERLAY: 
  GRL=["LB_collection_PbPb2018.xml"]
  dataSource  ='geant4'
  InputFile   ="/migration/atlas01scratch01/soumya/MuonValidation/ATLHI-304/mc16_5TeV.420051.Starlight_r193_gammagamma2mumu_breakupMode5_filter.recon.AOD.e4990_s2873_r11775/AOD.20474399._000001.pool.root.1"
elif do_pPb2016_8TeV:
  GRL=["LB_collection_pPb2016_8TeV.xml"]
  InputFile   ="/atlas/work/shared/JobTestData/data16_hip8TeV/AOD.18205937._001186.pool.root.1"
  DiMuon_triggers =["HLT_2mu3","HLT_2mu4","HLT_2mu6","HLT_2mu8","HLT_mu4_mu4noL1"]
else :
  exit()
print(MinBias_triggers+Muon_triggers+DiMuon_triggers)
#------------------------------------------------------------








import AthenaPoolCnvSvc.ReadAthenaPool
import glob
svcMgr.EventSelector.InputCollections = glob.glob(InputFile)
theApp.EvtMax = m_EvtMax


from AthenaCommon.GlobalFlags import globalflags
globalflags.DataSource.set_Value_and_Lock(dataSource) #data geant4
svcMgr += CfgMgr.AthenaEventLoopMgr(EventPrintoutInterval=1)
globalflags.DatabaseInstance = 'CONDBR2' # e.g. if you want run 2 data,  set to COMP200 if you want run1. This is completely ignored in MC.
#globalflags.DatabaseInstance = 'COMP200' # e.g. if you want run 2 data,  set to COMP200 if you want run1. This is completely ignored in MC.



#the tool service
from AthenaCommon.AppMgr import ToolSvc

##------------------------------------------------------------
#Configure Track selection tool
#https://twiki.cern.ch/twiki/bin/view/AtlasProtected/InDetTrackSelectionTool
ToolSvc += CfgMgr.InDet__InDetTrackSelectionTool("TrackSelectionTool_MinBias",CutLevel="MinBias",minPt=400.)
ToolSvc += CfgMgr.InDet__InDetTrackSelectionTool("TrackSelectionTool_HILoose",CutLevel="HILoose",minPt=400.)
ToolSvc += CfgMgr.InDet__InDetTrackSelectionTool("TrackSelectionTool_HITight",CutLevel="HITight",minPt=400.)
##------------------------------------------------------------

##------------------------------------------------------------
# Create a MuonSelectionTool if we do not yet have one 
if not hasattr(ToolSvc, "MyMuonSelectionTool"):
    from MuonSelectorTools.MuonSelectorToolsConf import CP__MuonSelectionTool
    ToolSvc += CP__MuonSelectionTool("MyMuonSelectionTool")
    ToolSvc.MyMuonSelectionTool.MaxEta = 2.5
##------------------------------------------------------------

##------------------------------------------------------------
#https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCPAnalysisWinterMC16
#https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MuonMomentumCorrectionsSubgroup
from MuonMomentumCorrections.MuonMomentumCorrectionsConf import CP__MuonCalibrationAndSmearingTool
ToolSvc += CP__MuonCalibrationAndSmearingTool("MyMuonCalibrationAndSmearingTool")
print(ToolSvc.MyMuonCalibrationAndSmearingTool)
#ToolSvc.MyMuonCalibrationAndSmearingTool.Year                 ="Data18"
#if do_hi2015_AcoplanarMuons:
#  ToolSvc.MyMuonCalibrationAndSmearingTool.Year               ="Data15"
#ToolSvc.MyMuonCalibrationAndSmearingTool.StatComb             =False
#ToolSvc.MyMuonCalibrationAndSmearingTool.SagittaRelease       ="sagittaBiasDataAll_03_02_19_Data18"
#ToolSvc.MyMuonCalibrationAndSmearingTool.SagittaCorr          =True
#ToolSvc.MyMuonCalibrationAndSmearingTool.doSagittaMCDistortion=False
#ToolSvc.MyMuonCalibrationAndSmearingTool.Release              ="Recs2018_05_20"
#ToolSvc.MyMuonCalibrationAndSmearingTool.SagittaCorrPhaseSpace=True
ToolSvc.MyMuonCalibrationAndSmearingTool.Year                 ="Data18"
ToolSvc.MyMuonCalibrationAndSmearingTool.StatComb             =False
ToolSvc.MyMuonCalibrationAndSmearingTool.SagittaRelease       ="sagittaBiasDataAll_03_02_19_Data18"
ToolSvc.MyMuonCalibrationAndSmearingTool.SagittaCorr          =True
ToolSvc.MyMuonCalibrationAndSmearingTool.doSagittaMCDistortion=False
ToolSvc.MyMuonCalibrationAndSmearingTool.Release              ="Recs2019_05_30"
ToolSvc.MyMuonCalibrationAndSmearingTool.SagittaCorrPhaseSpace=True
if do_hi2015_AcoplanarMuons:
  ToolSvc.MyMuonCalibrationAndSmearingTool.Year               ="Data16"
if do_pPb2016_8TeV:
  ToolSvc.MyMuonCalibrationAndSmearingTool.Year               ="Data16"
if do_HI_MC_OVERLAY:
  ToolSvc.MyMuonCalibrationAndSmearingTool.SagittaCorr        =False
if do_pp2017_5TeV or do_pp2017_13TeV:
  ToolSvc.MyMuonCalibrationAndSmearingTool.Year               ="Data17"
print(ToolSvc.MyMuonCalibrationAndSmearingTool)
##------------------------------------------------------------

##------------------------------------------------------------
from MuonEfficiencyCorrections.CommonToolSetup import *
scale_medium = GetMuonEfficiencyTool("Medium",Release="190312_Winter_r21")
scale_tight  = GetMuonEfficiencyTool("Tight" ,Release="190312_Winter_r21")
##------------------------------------------------------------

##------------------------------------------------------------
# Create trigger matching tool
#https://twiki.cern.ch/twiki/bin/view/Atlas/XAODMatchingTool
ToolSvc += CfgMgr.Trig__MatchingTool("MyTriggerMatchTool", OutputLevel=DEBUG)
##------------------------------------------------------------

##------------------------------------------------------------
#The GRLTool
MyGRLTool=CfgMgr.GoodRunsListSelectionTool("MyGRLTool")                                                             
MyGRLTool.GoodRunsListVec=GRL
#MyGRLTool.PassThrough    =True                                                                                      
ToolSvc += MyGRLTool  
##------------------------------------------------------------


##------------------------------------------------------------
#The ZDCTool
#https://twiki.cern.ch/twiki/bin/viewauth/Atlas/ZdcAnalysisTool
Configuration="PbPb2018" 
ZdcAnalysisTool=CfgMgr.ZDC__ZdcAnalysisTool("ZdcAnalysisTool")
ZdcAnalysisTool.FlipEMDelay  =  False
ZdcAnalysisTool.LowGainOnly  =  False     
ZdcAnalysisTool.DoCalib      =  True       
ZdcAnalysisTool.Configuration=Configuration
ZdcAnalysisTool.AuxSuffix    =  "RP"       
ZdcAnalysisTool.ForceCalibRun=  -1
if(Configuration ==  "PbPb2018"): 
  print("Soumya:: ZDC Tool configured for 2018")
  ZdcAnalysisTool.DoTrigEff    =  False #for 2018 only? 
  ZdcAnalysisTool.DoTimeCalib  =  False #for 2018 only?
else:
  print("Soumya:: ZDC Tool configured for 2015")
ToolSvc += ZdcAnalysisTool 
##------------------------------------------------------------

##------------------------------------------------------------
#Pileup tool
#from HIEventUtils.HIEventUtilsConf import HI__HIPileupTool
#HIPileupTool=HI__HIPileupTool("HIPileupTool")
HIPileupTool=CfgMgr.HI__HIPileupTool("HIPileupTool")
HIPileupTool.Year="2018"
ToolSvc += HIPileupTool
##------------------------------------------------------------


MessageSvc.defaultLimit = 1000

#the main algorithm sequence
algSeq = CfgMgr.AthSequencer("AthAlgSeq")


#------------------------------------------------------------
#main algorithm to run
TrigRatesAlg=CfgMgr.TrigRates("TrigRatesAlg",OutputLevel=INFO)
#------------------------------------------------------------
TrigRatesAlg.TrackSelectionTool_MinBias  =ToolSvc.TrackSelectionTool_MinBias
TrigRatesAlg.TrackSelectionTool_HILoose  =ToolSvc.TrackSelectionTool_HILoose
TrigRatesAlg.TrackSelectionTool_HITight  =ToolSvc.TrackSelectionTool_HITight
TrigRatesAlg.MuonSelectionTool   =ToolSvc.MyMuonSelectionTool
TrigRatesAlg.TriggerMatchTool    =ToolSvc.MyTriggerMatchTool
TrigRatesAlg.GRLTool             =ToolSvc.MyGRLTool
TrigRatesAlg.muonCorrectionTool  =ToolSvc.MyMuonCalibrationAndSmearingTool
TrigRatesAlg.effi_SF_tool_medium =scale_medium
TrigRatesAlg.effi_SF_tool_tight  =scale_tight
TrigRatesAlg.ZDCAnalysisTool     =ZdcAnalysisTool
TrigRatesAlg.HIPileupTool        =HIPileupTool
#------------------------------------------------------------
TrigRatesAlg.UseGRL                  =False                     #Automatically ignored for MC
TrigRatesAlg.StoreAllEvents          =True                      #True only for MC
TrigRatesAlg.UseTrigger              =False                     #True for data False for MC
TrigRatesAlg.TriggerChains           ="|".join(MinBias_triggers)
TrigRatesAlg.MuonTriggerChains       ="|".join(Muon_triggers)
TrigRatesAlg.DiMuonTriggerChains     ="|".join(DiMuon_triggers)
TrigRatesAlg.StoreEventInfo          =1                         #2 requires ZDC info, works only for Pb+Pb
TrigRatesAlg.StoreTracks             =0 #1+2+4                  #bitmap
TrigRatesAlg.StoreVtx                =True                      #Always ON
TrigRatesAlg.StoreL1TE               =False                     #ON only for checks 
TrigRatesAlg.StoreSingleMuon         =True                      #Typically ON
TrigRatesAlg.StoreAcoplanarMuon      =False                     #OFF for pp     
TrigRatesAlg.HIEventShapeContainerKey="HIEventShape"            #"HIEventShape" or "CaloSums";="" to turn OFF, for exmple in pp
TrigRatesAlg.METContainerKey         ="MET_Calo"                #"MET_Calo";="" to turn OFF
TrigRatesAlg.TrackJetContainerKeys   =[
                                       #"AntiKt2PV0TrackJets", 
                                       #"AntiKt3PV0TrackJets",
                                       #"AntiKt4PV0TrackJets",
                                       #"AntiKt4EMPFlowJets",  
                                       #"AntiKt4EMTopoJets"
                                       ]     #=[] or comment to turn OFF
TrigRatesAlg.StoreTruthVtx           =True                      #True only for MC
TrigRatesAlg.StoreTruth              =2                         #0:disable, 1:stable only with pt>"TruthMinPT", 2-all True only for MC
TrigRatesAlg.TruthMinPT              =300                       #cut in MeV above which to store truth particles (only stable and primary particles stored)
TrigRatesAlg.ApplyMuonCalibrations   =True                      #False for MC (see link above where the tool is initialized)

algSeq += TrigRatesAlg
#------------------------------------------------------------


svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["MYSTREAM DATAFILE='myfile.root' OPT='RECREATE'"]

