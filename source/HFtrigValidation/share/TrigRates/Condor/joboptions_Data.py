#All Settings (Default)

m_EvtMax=500
dataSource         ='data'
#InputFile          ="links/0.root"
#InputFile          ="/eos/atlas/atlastier0/rucio/data16_hip5TeV/express_express/00312649/data16_hip5TeV.00312649.express_express.recon.AOD.x477_m1725/*"
#InputFile          ="/eos/atlas/atlastier0/rucio/data16_hip5TeV/physics_Main/00312649/data16_hip5TeV.00312649.physics_Main.recon.AOD.f770_m1730/*"

InputFile          ="/eos/atlas/atlastier0/rucio/data16_hip8TeV/physics_Main/00313063/data16_hip8TeV.00313063.physics_Main.recon.AOD.f774_m1736/data16_hip8TeV.00313063.physics_Main.recon.AOD.f774_m1736._lb0269._0001.1"
#InputFile          ="/eos/atlas/atlastier0/rucio/data16_hip8TeV/physics_Main/00313063/data16_hip8TeV.00313063.physics_Main.recon.AOD.f774_m1736/*"


import AthenaPoolCnvSvc.ReadAthenaPool
import glob
svcMgr.EventSelector.InputCollections = glob.glob(InputFile)
theApp.EvtMax = m_EvtMax


from AthenaCommon.GlobalFlags import globalflags
globalflags.DataSource.set_Value_and_Lock(dataSource) #data geant4
svcMgr += CfgMgr.AthenaEventLoopMgr(EventPrintoutInterval=1)
globalflags.DatabaseInstance = 'CONDBR2' # e.g. if you want run 2 data,  set to COMP200 if you want run1. This is completely ignored in MC.
#globalflags.DatabaseInstance = 'COMP200' # e.g. if you want run 2 data,  set to COMP200 if you want run1. This is completely ignored in MC.


#the tool service
from AthenaCommon.AppMgr import ToolSvc


#Configure Track selection tool
##------------------------------------------------------------
ToolSvc += CfgMgr.InDet__InDetTrackSelectionTool("TrackSelectionTool",CutLevel="MinBias",minPt=100.)
##------------------------------------------------------------



##configure trigger tool
##------------------------------------------------------------
##TriggerConfigGetter looks at recflags, so it might be a good idea 
##to set those, but I have found not to be always necessary
from RecExConfig.RecFlags  import rec
rec.readRDO = False #readRDO is the True one by default. Turn it off before turning others on
rec.readAOD = True #example for reading AOD

#need this bit for AOD because AOD doesn't contain all trigger info 
#- so has to connect to DB for some of it
#ESD/RDO appear not to need this. Only do this bit if running on data, not MC
## from AthenaCommon.GlobalFlags import globalflags
## globalflags.DataSource = 'data'
#
##every situation needs the next bit
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
athenaCommonFlags.FilesInput = svcMgr.EventSelector.InputCollections
from TriggerJobOpts.TriggerFlags import TriggerFlags
TriggerFlags.configurationSourceList=['ds']
from TriggerJobOpts.TriggerConfigGetter import TriggerConfigGetter
cfg = TriggerConfigGetter()


#from TrigDecisionTool.TrigDecisionToolConf import Trig__TrigDecisionTool
#ToolSvc += Trig__TrigDecisionTool( "TrigDecisionTool" )
#from TrigEDMConfig.TriggerEDM import EDMLibraries
#ToolSvc.TrigDecisionTool.Navigation.Dlls = [e for e in  EDMLibraries if 'TPCnv' not in e]
##------------------------------------------------------------


# Create a MuonSelectionTool if we do not yet have one 
##------------------------------------------------------------
from AthenaCommon.AppMgr import ToolSvc
if not hasattr(ToolSvc, "MyMuonSelectionTool"):
    from MuonSelectorTools.MuonSelectorToolsConf import CP__MuonSelectionTool
    ToolSvc += CP__MuonSelectionTool("MyMuonSelectionTool")
    ToolSvc.MyMuonSelectionTool.MaxEta = 2.5
##------------------------------------------------------------



# Create muon trigger matching tool
##------------------------------------------------------------
from TrigMuonMatching.TrigMuonMatchingConf import Trig__TrigMuonMatching
ToolSvc += Trig__TrigMuonMatching("MyMuonTriggerMatchTool")
##------------------------------------------------------------



#The GRLTool
##------------------------------------------------------------
MyGRLTool=CfgMgr.GoodRunsListSelectionTool("MyGRLTool")                                                             
MyGRLTool.GoodRunsListVec=["merge_GRL.xml"]
#MyGRLTool.PassThrough    =True                                                                                      
ToolSvc += MyGRLTool  
##------------------------------------------------------------



MessageSvc.defaultLimit = 9999999

#the main algorithm sequence
algSeq = CfgMgr.AthSequencer("AthAlgSeq")

#algorithm to fill the Validation hists
TrigRatesAlg=CfgMgr.TrigRates("TrigRatesAlg",OutputLevel=INFO)

TrigRatesAlg.TrackSelectionTool  =ToolSvc.TrackSelectionTool
TrigRatesAlg.MuonSelectionTool   =ToolSvc.MyMuonSelectionTool
TrigRatesAlg.MuonTriggerMatchTool=ToolSvc.MyMuonTriggerMatchTool
TrigRatesAlg.GRLTool             =ToolSvc.MyGRLTool

TrigRatesAlg.UseTrigger       =True
#TrigRatesAlg.TriggerChains    ="HLT_mu4_mb_sp.*|HLT_mu4|L1_MU4_TE.*|L1_TE..|L1_TE...|L1_TE.|L1_MBTS_._.|HLT_mb_sp.*L1TE.*|HLT_mb_sptrk|L1_MU4"
#TrigRatesAlg.TriggerChains    ="HLT_mu4_mb_sp.*|HLT_mu4|HLT_mb_sptrk|HLT_mb_sptrk_L1MBTS_1|HLT_mb_sp2400_pusup500_trk120_hmt_L1TE20|HLT_mb_sp2800_pusup600_trk140_hmt_L1TE20|HLT_mb_sp2800_pusup600_trk140_hmt_L1TE50|HLT_mb_sp3100_pusup700_trk160_hmt_L1TE50|HLT_mb_sp3100_pusup700_trk160_hmt_L1TE70|HLT_mb_sp3500_pusup800_trk180_hmt_L1TE70|HLT_mb_sp3500_pusup800_trk180_hmt_L1TE90|HLT_mb_sp4100_pusup900_trk200_hmt_L1TE90|HLT_mb_sp4100_pusup900_trk200_hmt_L1TE120|HLT_mb_sp4500_pusup1000_trk220_hmt_L1TE120|HLT_mb_sp4800_pusup1100_trk240_hmt_L1TE160|HLT_mb_sp5000_pusup1100_trk240_hmt_L1TE200|HLT_mb_sp5000_pusup1200_trk260_hmt_L1TE160|HLT_mb_sp5600_pusup1300_trk260_hmt_L1TE200|HLT_mb_sp5200_pusup1300_trk280_hmt_L1TE160|HLT_mb_sp6000_pusup1400_trk280_hmt_L1TE200"
TrigRatesAlg.TriggerChains    ="HLT_noalg_mb_L1MBTS_1|HLT_mb_sp100_trk60_hmt_L1MBTS_1_1|HLT_mb_sp100_trk80_hmt_L1MBTS_1_1|HLT_mb_sp100_trk100_hmt_L1MBTS_1_1|HLT_mb_sp100_trk110_hmt_L1MBTS_1_1"

TrigRatesAlg.StoreAllEvents   =False
TrigRatesAlg.UseGRL           =True

TrigRatesAlg.StoreEventInfo   =True
TrigRatesAlg.StoreTracks      =True
TrigRatesAlg.StoreTrackDetails=True
TrigRatesAlg.StoreVtx         =True
TrigRatesAlg.StoreL1TE        =True
algSeq += TrigRatesAlg



svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["MYSTREAM DATAFILE='myfile.root' OPT='RECREATE'"]

