#include "HFtrigValidation/SkimAlg3.h"

#include <GaudiKernel/ITHistSvc.h>
#include <GaudiKernel/ServiceHandle.h>

#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTruth/TruthVertexContainer.h"

#include <TTree.h>
#include <cmath>


//Things to do
//1. Add InitMuon() and ProcessMuon() functions to store, pt,eta,phi, quality and index to matched ID track
//2. For truth particles, only store the truth particles that have the "B" as the parent


SkimAlg3::SkimAlg3(const std::string& name, ISvcLocator* pSvcLocator)
  : AthAlgorithm(name,pSvcLocator)
{
  declareProperty("EventInfoKey"          ,  m_EventInfo_key               ="EventInfo"          );
  declareProperty("MuonsKey"              ,  m_muons_key                   ="Muons"              );
  declareProperty("TrkContainerKey"       ,  m_trk_container_key           ="InDetTrackParticles");
  declareProperty("VtxContainerKey"       ,  m_vtx_container_key           ="PrimaryVertices"    );
  declareProperty("TruthVtxContainerKey"  ,  m_truth_vtx_container_key     ="TruthVertices"      );
  declareProperty("TruthContainerKey"     ,  m_truth_container_key         ="TruthParticles"     );
  declareProperty("TrackSelectionTool"    ,  m_trkSelTool                                        );
}

bool SkimAlg3::IsPrimaryParticle(const xAOD::TruthParticle* particle){
  if(!particle) return false;
  if(particle->status()!=1                             ) return false;
  if(particle->barcode()>=2e5 || particle->barcode()==0) return false;
  if(particle->charge()==0                             ) return false;
  return true;
}


StatusCode SkimAlg3::initialize(){
   ATH_MSG_INFO("Starting initialize()");

//retreive the tools //THIS IS OPTIONAL!
//-------------------------------------------------------------------------
   CHECK( m_trkSelTool.retrieve() );
//-------------------------------------------------------------------------


//Create and Book OutPutTree
//-------------------------------------------------------------------------
   ServiceHandle<ITHistSvc> histSvc("THistSvc",name()); 
   CHECK( histSvc.retrieve() );

   m_OutTree=new TTree("HeavyIonD3PD","HeavyIonD3PD");
   CHECK(histSvc->regTree("/MYSTREAM/HeavyIonD3PD",m_OutTree));

   if(m_EventInfo_key          !="") InitEventInfo   (m_OutTree);
   if(m_muons_key              !="") InitMuons       (m_OutTree);
   if(m_trk_container_key      !="") InitTracks      (m_OutTree);
   if(m_vtx_container_key      !="") InitVertex      (m_OutTree);
   if(m_truth_vtx_container_key!="") InitTruthVertex (m_OutTree);
   if(m_truth_container_key    !="") InitTruth       (m_OutTree);
//-------------------------------------------------------------------------


   return StatusCode::SUCCESS;
}







StatusCode SkimAlg3::execute(){
   ATH_MSG_DEBUG("Starting execute()");


   bool pass_cleaning_cuts=true;
   if(m_EventInfo_key!="" && m_vtx_container_key!="") CHECK(CleaningCuts(pass_cleaning_cuts));
   if(!pass_cleaning_cuts) return StatusCode::SUCCESS;


   bool fill=false;
   if(m_EventInfo_key          !="") CHECK(ProcessEventInfo   ());
   if(m_muons_key              !="") CHECK(ProcessMuons       ());
   if(m_trk_container_key      !="") CHECK(ProcessTracks      ());
   if(m_vtx_container_key      !="") CHECK(ProcessVertex      ());
   if(m_truth_vtx_container_key!="") CHECK(ProcessTruthVertex ());
   if(m_truth_container_key    !="") CHECK(ProcessTruth       (fill));

   if(fill) m_OutTree->Fill();
   ATH_MSG_DEBUG("Filling RunNumber="<<RunNumber);
   return StatusCode::SUCCESS;
}





StatusCode SkimAlg3::finalize(){
  return StatusCode::SUCCESS;
}





StatusCode SkimAlg3::CleaningCuts(bool &pass_cleaning_cuts){
   ATH_MSG_DEBUG("Starting CleaningCuts()");
   pass_cleaning_cuts=true;
   static int event_num=-1;
   event_num++;

//Event Info
//------------------------------------------------------  
   const xAOD::EventInfo* l_EventInfo = nullptr;
   if(evtStore()->retrieve(l_EventInfo,m_EventInfo_key).isFailure()){
      ATH_MSG_ERROR(" Could not retrieve EventInfo with key "<<m_EventInfo_key.c_str());
      return StatusCode::FAILURE;
   }

   if(!l_EventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
      ATH_MSG_ERROR("This code should be run over MC only");
      return StatusCode::FAILURE;
   }  
//------------------------------------------------------  



//------------------------------------------------------  
//Event by Event Cleaning
    if((l_EventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error ) || 
       (l_EventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error ) || 
       (l_EventInfo->errorState(xAOD::EventInfo::SCT)==xAOD::EventInfo::Error ) || 
       (l_EventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) ) ){
        pass_cleaning_cuts=false;
        ATH_MSG_DEBUG("Event FAILED EventLevelCleaning");
        return StatusCode::SUCCESS;
    }
    else ATH_MSG_DEBUG("Event PASSED EventLevelCleaning");
//------------------------------------------------------  



//Vertex
//------------------------------------------------------  
//Must have reconstructed vertex
   const xAOD::VertexContainer *l_VertexContainer = nullptr;
   if(evtStore()->retrieve(l_VertexContainer,m_vtx_container_key).isFailure()){
      ATH_MSG_ERROR("Could not retrieve VxContainer with key "<<m_vtx_container_key.c_str());
      return StatusCode::FAILURE;
   }
   ATH_MSG_DEBUG("Num_Vertices="<<l_VertexContainer->size());
   if(l_VertexContainer->size()<=1) {pass_cleaning_cuts=false;}
//------------------------------------------------------  
   
   return StatusCode::SUCCESS;
}





//EventInfo
void SkimAlg3::InitEventInfo(TTree *l_OutTree){
   l_OutTree->Branch("RunNumber"  , &RunNumber        ,"RunNumber/i");
   l_OutTree->Branch("lbn"        , &lumi_block       ,"lbn/i");
   l_OutTree->Branch("bcid"       , &bunch_crossing_id,"bcid/i");
   l_OutTree->Branch("eventNumber", &eventNumber      ,"eventNumber/l");
}

StatusCode SkimAlg3::ProcessEventInfo(){
   const xAOD::EventInfo* l_EventInfo = nullptr;
   if(evtStore()->retrieve(l_EventInfo,m_EventInfo_key).isFailure()){
      ATH_MSG_ERROR(" Could not retrieve EventInfo with key "<<m_EventInfo_key.c_str());
      return StatusCode::FAILURE;
   }

   //Set Variables written to output Tree
   RunNumber        =l_EventInfo->runNumber();
   lumi_block       =l_EventInfo->lumiBlock();
   bunch_crossing_id=l_EventInfo->bcid();
   eventNumber      =l_EventInfo->eventNumber();

   return StatusCode::SUCCESS;
}


//Vertex
void SkimAlg3::InitVertex(TTree *l_OutTree){
     l_OutTree->Branch("vtx_z"     ,&m_vtx_z     );
     l_OutTree->Branch("vtx_x"     ,&m_vtx_x     );
     l_OutTree->Branch("vtx_y"     ,&m_vtx_y     );
     l_OutTree->Branch("vtx_ntrk"  ,&m_vtx_ntrk  );
}

StatusCode SkimAlg3::ProcessVertex(){
   const xAOD::VertexContainer *l_VertexContainer = nullptr;
   if(evtStore()->retrieve(l_VertexContainer,m_vtx_container_key).isFailure()){
      ATH_MSG_ERROR("Could not retrieve VxContainer with key "<<m_vtx_container_key.c_str());
      return StatusCode::FAILURE;
   }

   m_vtx_z     .clear();
   m_vtx_x     .clear();
   m_vtx_y     .clear();
   m_vtx_ntrk  .clear();
   for(const auto* vtx : *l_VertexContainer){
      m_vtx_z     .push_back(vtx->z());
      m_vtx_x     .push_back(vtx->x());
      m_vtx_y     .push_back(vtx->y());
      m_vtx_ntrk  .push_back(vtx->nTrackParticles());
    }
   return StatusCode::SUCCESS;
}




void SkimAlg3::InitTruthVertex(TTree *l_OutTree){
     l_OutTree->Branch("truth_vtx_z"     ,&m_truth_vtx_z     );
     l_OutTree->Branch("truth_vtx_x"     ,&m_truth_vtx_x     );
     l_OutTree->Branch("truth_vtx_y"     ,&m_truth_vtx_y     );
     l_OutTree->Branch("truth_vtx_t"     ,&m_truth_vtx_t     );
}

StatusCode SkimAlg3::ProcessTruthVertex(){
   const xAOD::TruthVertexContainer *l_TruthVertexContainer = nullptr;
   if(evtStore()->retrieve(l_TruthVertexContainer,m_truth_vtx_container_key).isFailure()){
      ATH_MSG_ERROR("Could not retrieve TruthVxContainer with key "<<m_truth_vtx_container_key.c_str());
      return StatusCode::FAILURE;
   }
   m_truth_vtx_z     .clear();
   m_truth_vtx_x     .clear();
   m_truth_vtx_y     .clear();
   m_truth_vtx_t     .clear();
   for(const auto* vtx : *l_TruthVertexContainer){
      m_truth_vtx_z     .push_back(vtx->z());
      m_truth_vtx_x     .push_back(vtx->x());
      m_truth_vtx_y     .push_back(vtx->y());
      m_truth_vtx_t     .push_back(vtx->t());
   }
   return StatusCode::SUCCESS;
}


void SkimAlg3::InitMuons(TTree *l_OutTree){
  l_OutTree=l_OutTree;
}
StatusCode SkimAlg3::ProcessMuons(){
 return StatusCode::SUCCESS;
}


//Tracks
void SkimAlg3::InitTracks(TTree *l_OutTree){
     l_OutTree->Branch("trk_numqual"        ,&m_trk_numqual        );
     l_OutTree->Branch("trk_pt"             ,&m_track_pt           );
     l_OutTree->Branch("trk_eta"            ,&m_track_eta          );
     l_OutTree->Branch("trk_phi"            ,&m_track_phi          );
     l_OutTree->Branch("trk_charge"         ,&m_track_charge       );
     l_OutTree->Branch("trk_qual"           ,&m_track_quality      );
     l_OutTree->Branch("trk_d0"             ,&m_track_d0           );
     l_OutTree->Branch("trk_z0_wrtPV"       ,&m_track_z0_wrtPV     );
     l_OutTree->Branch("trk_truth_index"    ,&m_trk_truth_index    ); 
     l_OutTree->Branch("trk_truth_prob"     ,&m_trk_truth_prob     ); 
     l_OutTree->Branch("trk_truth_IsPrimary",&m_trk_truth_IsPrimary); 
}

StatusCode SkimAlg3::ProcessTracks(){
   const xAOD::TrackParticleContainer *l_TrackParticleContainer = nullptr;
   if(evtStore()->retrieve(l_TrackParticleContainer,m_trk_container_key).isFailure()){
     ATH_MSG_ERROR("Could not retrieve TrackParticleContainer with key "<<m_trk_container_key.c_str());
     return StatusCode::FAILURE;
   }
   ATH_MSG_DEBUG("Num Tracks="<<l_TrackParticleContainer->size());

   const xAOD::VertexContainer *l_VertexContainer = nullptr;
   if(evtStore()->retrieve(l_VertexContainer,m_vtx_container_key).isFailure()){
      ATH_MSG_ERROR("Could not retrieve VxContainer with key "<<m_vtx_container_key.c_str());
      return StatusCode::FAILURE;
   }
   const auto* priVtx = *(l_VertexContainer->cbegin());
   if (priVtx->vertexType() != xAOD::VxType::PriVtx) {
     ATH_MSG_ERROR( "First vertex is not of type \"Primary Vertex\"." );
     return StatusCode::FAILURE;
   }
   float z_vtx = priVtx->z();

   m_trk_numqual          .clear();m_trk_numqual.assign(3,0);
   m_track_pt             .clear();
   m_track_eta            .clear();
   m_track_phi            .clear();
   m_track_charge         .clear();
   m_track_quality        .clear();
   m_track_d0             .clear();
   m_track_z0_wrtPV       .clear();
   m_trk_truth_index      .clear();
   m_trk_truth_prob       .clear();
   m_trk_truth_IsPrimary  .clear();
   m_trk_truth_index_temp1.clear();
 
   int trk_index=0;
   for(const auto* track : *l_TrackParticleContainer){
     float pt    =track->pt    ();
     float eta   =track->eta   ();
     float phi   =track->phi   ();
     float charge=track->charge();

     int   quality=-1;
     if (m_trkSelTool->accept(*track, priVtx)) {
       quality = 1;
     } else {
       // if the track passes everything except for D0 and Z0SinTheta cuts, give it quality 0
       const auto& taccept = m_trkSelTool->getTAccept();
       static const auto d0Index = taccept.getCutPosition("D0");
       static const auto z0Index = taccept.getCutPosition("Z0SinTheta");
       static const auto nCuts = taccept.getNCuts();
       auto cutBitset = taccept.getCutResultBitSet();
       cutBitset |= (1 << d0Index) | (1 << z0Index);
       if (cutBitset.count() == nCuts) quality = 0;
       // looking up the cut results by name is slow but safe and readable
       // we've checked that this gives the same result.
       // now comment out for speed
       // bool rest = taccept.getCutResult("Pt") &&
       //    taccept.getCutResult("Eta") &&
       //    taccept.getCutResult("InnermostLayersHits") &&
       //    taccept.getCutResult("PixelHits") &&
       //    taccept.getCutResult("SctHits") &&
       //    taccept.getCutResult("FitQuality");
       // if (rest != (cutBitset.count() == nCuts)) return StatusCode::FAILURE;
     }
     if(quality>=-1) {m_trk_numqual[0]++;}
     if(quality>= 0) {m_trk_numqual[1]++;}
     if(quality>= 1) {m_trk_numqual[2]++;}
     //int quality=MyUtils::TrackQuality(track,z_vtx);


     
     m_track_pt     .push_back(pt);
     m_track_eta    .push_back(eta);
     m_track_phi    .push_back(phi);
     m_track_charge .push_back(charge);
     m_track_quality.push_back(quality);
     m_track_d0            .push_back(track->d0());
     m_track_z0_wrtPV      .push_back(track->z0()+track->vz() - z_vtx);

     //For reco particles that have matched truth particle
     //provide index to matched truth particle as -1, and set up the actual index later on while looping over all truth particles
     const ElementLink<xAOD::TruthParticleContainer> ptruthContainer=(track->auxdata<ElementLink<xAOD::TruthParticleContainer>>("truthParticleLink" ));
     if(ptruthContainer.isValid()){
       const xAOD::TruthParticle *associated_truth=*ptruthContainer;
       m_trk_truth_index_temp1[associated_truth]=trk_index; 
       m_trk_truth_index    .push_back(-1);
       m_trk_truth_prob     .push_back(track->auxdata<float>("truthMatchProbability"));
       m_trk_truth_IsPrimary.push_back(IsPrimaryParticle(associated_truth));
     }
     else{
       m_trk_truth_index    .push_back(-1);
       m_trk_truth_prob     .push_back( 0); 
       m_trk_truth_IsPrimary.push_back(false);
     }
     trk_index++;
   }

   return StatusCode::SUCCESS;
}





void SkimAlg3::InitTruth(TTree *l_OutTree){
     l_OutTree->Branch("truth_pt"       ,&m_truth_pt);
     l_OutTree->Branch("truth_eta"      ,&m_truth_eta);
     l_OutTree->Branch("truth_phi"      ,&m_truth_phi);
     l_OutTree->Branch("truth_charge"   ,&m_truth_charge );

     l_OutTree->Branch("truth_id"       ,&m_truth_id     );
     l_OutTree->Branch("truth_barcode"  ,&m_truth_barcode);
     l_OutTree->Branch("truth_qual"     ,&m_truth_quality);
     l_OutTree->Branch("truth_status"   ,&m_truth_status );
     l_OutTree->Branch("truth_b_index"  ,&m_truth_b_index);
     if(m_trk_container_key!="") l_OutTree->Branch("truth_trk_index",&m_truth_trk_index);
}


StatusCode SkimAlg3::ProcessTruth(bool &fill){
   m_truth_pt       .clear();
   m_truth_eta      .clear();
   m_truth_phi      .clear();
   m_truth_charge   .clear();
   m_truth_id       .clear();
   m_truth_barcode  .clear();
   m_truth_quality  .clear();
   m_truth_status   .clear();
   m_truth_b_index  .clear();
   m_truth_trk_index.clear();
   int truth_index=0;
   int b_index    =0;
   m_truth_particles.clear();


   const xAOD::TruthParticleContainer *l_TruthParticleContainer;
   if(evtStore()->retrieve(l_TruthParticleContainer,m_truth_container_key).isFailure()){
     ATH_MSG_ERROR("Could not retrieve TruthParticleContainer with key"<<m_truth_container_key);
     return StatusCode::FAILURE;
   }

   for(auto track_itr=l_TruthParticleContainer->begin();track_itr!=l_TruthParticleContainer->end();track_itr++){
     auto track=(*track_itr);

     if(track->absPdgId()!=5) continue; //b-quark
     b_index++;
     AddDescendants(track, truth_index, b_index);//add b-quark and children to outputlist
     fill=true;
   }

   //ensure that there in an electron or muon with pt>3GeV in the decay
   bool fill2=false;
   for(auto& track:m_truth_particles){
     if(track.first->absPdgId()==11 || track.first->absPdgId()==13){
       if(track.first->pt()>=3000) fill2=true;
     }
   }
   fill=fill&&fill2;

   //store all stable particles above 400MeV
   if(fill){
     b_index    =-1;
     for(auto track_itr=l_TruthParticleContainer->begin();track_itr!=l_TruthParticleContainer->end();track_itr++){
       auto track=(*track_itr);
       if(m_truth_particles.find(track)!=m_truth_particles.end()) continue;//skip already stored tracks
       if(track->pt()>400 && track->status()==1 && track->charge()!=0 && track->barcode()<2e5) AddTruthTrack(track,truth_index,b_index);
     }
   }

   return StatusCode::SUCCESS;
}

void SkimAlg3::AddDescendants(const xAOD::TruthParticle* track, int& truth_index,  int& b_index){
     AddTruthTrack (track, truth_index, b_index); 
     size_t Nchildren= track->nChildren();
     for(size_t i=0;i<Nchildren;i++){
       const xAOD::TruthParticle* child=track->child(i);
       AddDescendants(child, truth_index, b_index);
     }
}

void SkimAlg3::AddTruthTrack(const xAOD::TruthParticle* track, int& truth_index, int& b_index){
     if(m_truth_particles.find(track)!=m_truth_particles.end()) return;
     m_truth_particles[track]=true;

     float pt    =track->pt    ();
     float eta   =0.0;
     if(fabs(pt)>0.00001) eta   =track->eta   ();
     float phi   =track->phi   ();
     float charge=track->charge();
     int   id    =track->pdgId ();
     int quality=1;
     if(track->isStrangeBaryon() ||track->barcode()<=0) quality=-1;

     m_truth_pt      .push_back(pt);
     m_truth_eta     .push_back(eta);
     m_truth_phi     .push_back(phi);
     m_truth_charge  .push_back(charge);
     m_truth_id      .push_back(id);
     m_truth_barcode .push_back(track->barcode());
     m_truth_quality .push_back(quality);
     m_truth_status  .push_back(track->status());
     m_truth_b_index .push_back(b_index);

     //For truth particles that have matched reco particle
     //provide index to matched reconstructed particle
     static bool l_trk_index=(m_trk_container_key!="")? true:false;
     if(l_trk_index){
       int trk_index=-1;
       if(m_trk_truth_index_temp1.find(track)!=m_trk_truth_index_temp1.end()){
         trk_index=m_trk_truth_index_temp1[track];
         m_trk_truth_index[trk_index]=truth_index;
       }
       m_truth_trk_index.push_back(trk_index);
     }
     truth_index++;
}
