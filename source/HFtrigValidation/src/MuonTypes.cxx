#include "HFtrigValidation/MuonTypes.h"

#include <GaudiKernel/ITHistSvc.h>
#include <GaudiKernel/ServiceHandle.h>

#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthVertex.h"

#include <TTree.h>
#include <cmath>



MuonTypes::MuonTypes(const std::string& name, ISvcLocator* pSvcLocator)
  : AthAlgorithm(name,pSvcLocator)
{
  declareProperty("EventInfoKey"      , m_EventInfo_key       ="EventInfo"          );
  declareProperty("MuonsKey"          , m_muons_key           ="Muons"              );
  declareProperty("MuonSelectionTool" , m_muonSelection                             );
}



StatusCode MuonTypes::initialize(){
   ATH_MSG_INFO("Starting initialize()");

//retreive the tools //THIS IS OPTIONAL!
//-------------------------------------------------------------------------
   CHECK( m_muonSelection.retrieve() );
//-------------------------------------------------------------------------


//Create and Book OutPutTree
//-------------------------------------------------------------------------
   m_OutFile=new TFile("MuonTree.root","recreate");
   InitMuonBranches();
//-------------------------------------------------------------------------
   return StatusCode::SUCCESS;
}


StatusCode MuonTypes::execute(){
   ATH_MSG_DEBUG("Starting execute()");

   bool pass_cleaning_cuts=true;
   CHECK(CleaningCuts(pass_cleaning_cuts));
   if(!pass_cleaning_cuts) return StatusCode::SUCCESS;

   //CHECK(ProcessTracks());
   CHECK(ProcessMuons ());

   return StatusCode::SUCCESS;
}

StatusCode MuonTypes::finalize(){
  m_OutFile->Write();
  return StatusCode::SUCCESS;
}


void MuonTypes::InitMuonBranches(){
   m_OutTree=new TTree("MuonTree","MuonTree");
   m_OutTree->Branch("m_pt"                ,&m_pt                , "m_pt/F");
   m_OutTree->Branch("m_eta"               ,&m_eta               , "m_eta/F");
   m_OutTree->Branch("m_phi"               ,&m_phi               , "m_phi/F");
   m_OutTree->Branch("m_quality"           ,&m_quality           , "m_quality/I");
   m_OutTree->Branch("m_momentum_imbalance",&m_momentum_imbalance, "m_momentum_imbalance/F");
   m_OutTree->Branch("m_elosstype"         ,&m_elosstype         , "m_elosstype/I");

   m_OutTree->Branch("m_ms_pt"             ,&m_ms_pt             , "m_ms_pt/F"     );
   m_OutTree->Branch("m_ms_p"              ,&m_ms_p              , "m_ms_p/F"      );
   m_OutTree->Branch("m_truth_prob"        ,&m_truth_prob        , "m_truth_prob/F");

   m_OutTree->Branch("m_trk_d0"            ,&m_trk_d0            , "m_trk_d0/F");
   m_OutTree->Branch("m_trk_z0"            ,&m_trk_z0            , "m_trk_z0/F");

   m_OutTree->Branch("m_id"                ,&m_id                , "m_id/I");
   m_OutTree->Branch("m_barcode"           ,&m_barcode           , "m_barcode/I");
   m_OutTree->Branch("m_primary"           ,&m_primary           , "m_primary/I");
   m_OutTree->Branch("m_truth_pt"          ,&m_truth_pt          , "m_truth_pt/F");
}


void MuonTypes::ResetMuonBranches(){
  m_pt=-1000;
  m_eta=-1000;
  m_phi=-1000;
  m_quality = 0;
  m_momentum_imbalance=-1000;
  m_elosstype=-1;

  m_ms_pt=-1000;
  m_ms_p =-1000;
  m_truth_prob=-1;

  m_trk_d0=-1000;
  m_trk_z0=-1000;

  m_id      = 0;
  m_barcode =-1;
  m_primary = 0;

  m_truth_pt=-1000;
}




bool MuonTypes::IsPrimaryParticle(const xAOD::TruthParticle* particle){
  if(!particle) return false;
  if(particle->status()!=1                             ) return false;
  if(particle->barcode()>=2e5 || particle->barcode()==0) return false;
  if(particle->charge()==0                             ) return false;
  return true;
}


StatusCode MuonTypes::ProcessMuons(){
   static int count=0;
   std::cout<<"SOUMYA "<<count++<<std::endl;

   /*-----------------------------------------------------------------------------
    *  Get the Muon container and loop over all muons
    *-----------------------------------------------------------------------------*/
   const xAOD::MuonContainer *l_muons;
   CHECK(evtStore()->retrieve(l_muons,m_muons_key));
   for(auto Muon:*l_muons){
     ResetMuonBranches();  

     m_pt =Muon->pt()/1000.0;
     m_eta=Muon->eta();
     m_phi=Muon->phi();

     xAOD::Muon::Quality my_quality = m_muonSelection->getQuality(*Muon);
     if(Muon->muonType() ==xAOD::Muon::MuonType::Combined) m_quality+= 1;//NO BUG
     if(my_quality<=xAOD::Muon::VeryLoose)                 m_quality+= 2;
     if(my_quality<=xAOD::Muon::Loose)                     m_quality+= 4;
     if(my_quality<=xAOD::Muon::Medium)                    m_quality+= 8;
     if(my_quality<=xAOD::Muon::Tight)                     m_quality+=16;
     if(m_muonSelection->passedIDCuts(*Muon))              m_quality+=32;

     const xAOD::TrackParticle*idTrk= Muon->trackParticle(xAOD::Muon::InnerDetectorTrackParticle);//ID Track
     const xAOD::TrackParticle*meTrk= Muon->trackParticle(xAOD::Muon::ExtrapolatedMuonSpectrometerTrackParticle);//ME Track
     if(meTrk) m_quality+=64;
     if(idTrk) m_quality+=128;
     if(!meTrk || !idTrk){
       if(!meTrk) {ATH_MSG_ERROR("meTrk Not Found for muon");}
       if(!idTrk) {ATH_MSG_ERROR("idTrk Not Found for muon");}
     }
     else{
       m_momentum_imbalance=(idTrk->p4().P() - meTrk->p4().P())/idTrk->p4().P();
       m_trk_d0=idTrk->d0();
       m_trk_z0=idTrk->z0();//+idTrk->vz()-z_vtx;
     }

     const xAOD::TrackParticle*msTrk= Muon->trackParticle(xAOD::Muon::MuonSpectrometerTrackParticle);//MS Track
     if(msTrk){
       m_ms_pt=msTrk->pt();
       m_ms_p =msTrk->p4().P();
     }

//see http://acode-browser2.usatlas.bnl.gov/lxr/source/r21/atlas/Event/xAOD/xAODMuon/xAODMuon/versions/Muon_v1.h (or latest file)
///// Defines how the energy loss was handled for this muon 
//enum EnergyLossType { 
//   Parametrized=0,  
//   NotIsolated=1,   //!< Reconstruction configured to use the parametrization w/o looking in the calo (eg calo off)
//   MOP=2,           //!< Measurement found to be compatible with most probable value --> mop used as more reliable at this region of the eloss
//   Tail=3,          //!< Measured eloss significantly higher than mop --> the calo measurement used
//   FSRcandidate=4  //!< In standalone reconstruction the Tail option was used. 
//                   //but an imbalance is observed when comparing Pstandalone and Pinnerdetector (Pstandalone>Pinnerdetector) 
//                   //--> if using the mop resolves the imbalance the excess energy loss is stored as fsrEnergy and the mop is used as the eloss.
//};
     m_elosstype=Muon->auxdata<uint8_t>("energyLossType");

     /*-----------------------------------------------------------------------------
      *  Truth cuts on the reconstructed muon
      *-----------------------------------------------------------------------------*/
     if(idTrk){
       const xAOD::TruthParticle *muon_associated_truth=nullptr;
       auto ptruthContainer=(idTrk->auxdata<ElementLink<xAOD::TruthParticleContainer>>("truthParticleLink"));
       if(ptruthContainer.isValid()){
         m_truth_prob=(idTrk->auxdata<float>("truthMatchProbability")); 
         muon_associated_truth=*ptruthContainer;
       } 

       if(muon_associated_truth){
         m_id     =muon_associated_truth->absPdgId();
         m_barcode=muon_associated_truth->barcode();
         if(IsPrimaryParticle(muon_associated_truth)) m_primary= 1;
         else                                         m_primary=-1;
         m_truth_pt=muon_associated_truth->pt()/1000.0;
       }
     }
     m_OutTree->Fill();
   }

   return StatusCode::SUCCESS;
}









StatusCode MuonTypes::CleaningCuts(bool &pass_cleaning_cuts){
   ATH_MSG_DEBUG("Starting CleaningCuts()");
   pass_cleaning_cuts=true;
   static int event_num=-1;
   event_num++;

//Event Info
//------------------------------------------------------  
   const xAOD::EventInfo* l_EventInfo = nullptr;
   if(evtStore()->retrieve(l_EventInfo,m_EventInfo_key).isFailure()){
      ATH_MSG_ERROR(" Could not retrieve EventInfo with key "<<m_EventInfo_key.c_str());
      return StatusCode::FAILURE;
   }

   if(!l_EventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
      ATH_MSG_ERROR("This code should be run over MC only");
      return StatusCode::FAILURE;
   }  
//------------------------------------------------------  

   return StatusCode::SUCCESS;
}



