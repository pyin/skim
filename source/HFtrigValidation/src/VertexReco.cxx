#include "HFtrigValidation/VertexReco.h"

#ifdef __DISABLE_VERTEXRECO__
#include <GaudiKernel/ITHistSvc.h>
#include <GaudiKernel/ServiceHandle.h>
VertexReco::VertexReco(const std::string& name, ISvcLocator* pSvcLocator): AthAlgorithm(name,pSvcLocator){}
StatusCode VertexReco::initialize(){return StatusCode::SUCCESS;}
StatusCode VertexReco::execute   (){return StatusCode::SUCCESS;}
StatusCode VertexReco::finalize  (){return StatusCode::SUCCESS;}
#else




#include <GaudiKernel/ITHistSvc.h>
#include <GaudiKernel/ServiceHandle.h>

#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthVertex.h"

#include <TTree.h>
#include <cmath>


enum{
	BAD                =0,
	MINBIAS_TOPOLOGICAL=1,
	MINBIAS_ALL        =2,
};

enum{
  MATCHED_OR_UNMATCHED=2,
    NO_B_MATCH=0,
    B_MATCH   =1,

  EVENT_TYPE=2,
    INCLUSIVE=0,
    B_DECAY=1,
	PID_TYPE=2,
	  PID_PION=0,
		PID_MUON=1,
};

TH2D* h_pt_match ;
TH2D* h_eta_match;
TH2D* h_phi_match;

TH1D* h_b_Event;
TH1D* h_truth_spectra[EVENT_TYPE][PID_TYPE];
TH1D* h_momentum_imbalance_truth[PID_TYPE];

TH1D* h_bmuon_track_cut;

TH1D* h_muon_d0[MATCHED_OR_UNMATCHED];
TH1D* h_muon_z0[MATCHED_OR_UNMATCHED];
TH1D* h_muon_momentum_imbalance[MATCHED_OR_UNMATCHED];
TH1D* h_muon_truth_dx[MATCHED_OR_UNMATCHED];
TH1D* h_muon_truth_dy[MATCHED_OR_UNMATCHED];
TH1D* h_muon_truth_dz[MATCHED_OR_UNMATCHED];
TH1D* h_muon_truth_dT[MATCHED_OR_UNMATCHED];
TH1D* h_muon_truth_dR[MATCHED_OR_UNMATCHED];


	 TTree *m_OutTree;
	 float m_muon_pt;
	 float m_muon_eta;
	 float m_muon_phi;
	 float m_muon_trk_d0;
	 float m_muon_trk_z0sintheta;
	 float m_muon_momentum_imbalance;
	 int   m_muon_truth_id;
	 int   m_muon_truth_barcode;
	 float m_muon_truth_dx;
	 float m_muon_truth_dy;
	 float m_muon_truth_dz;
	 float m_muon_truth_dt;
	 float m_muon_truth_dr;
	 int   m_muon_parent_pdgid;
	 int   m_muon_parent_barcode;
	 int   m_muon_type;//b-decay, c-decay, neither
	 std::vector<float> m_track_pt;
	 std::vector<float> m_track_eta;
	 std::vector<float> m_track_phi;
	 std::vector<int>   m_track_truth_pdgid;
	 std::vector<int>   m_track_truth_barcode;
	 std::vector<int>   m_track_parent_pdgid;
	 std::vector<int>   m_track_parent_barcode;
	 std::vector<int>   m_track_type;
	 std::vector<int>   m_track_vtx_chi2_ndf;
	 std::vector<int>   m_track_vtx_dx;
	 std::vector<int>   m_track_vtx_dy;
	 std::vector<int>   m_track_vtx_dz;
	 std::vector<int>   m_track_vtx_dr;
	 std::vector<int>   m_track_vtx_dt;



VertexReco::VertexReco(const std::string& name, ISvcLocator* pSvcLocator)
  : AthAlgorithm(name,pSvcLocator)
  , m_fitterSvc("Trk::TrkVKalVrtFitter/VertexFitterTool", this)
  , m_VKVFitter(0)
//  , m_muonSelection("CP::MuonSelectionTool")
{
  declareProperty("EventInfoKey"      , m_EventInfo_key       ="EventInfo"          );
  declareProperty("TrkContainerKey"   , m_trk_container_key   ="InDetTrackParticles");
  declareProperty("VtxContainerKey"   , m_vtx_container_key   ="PrimaryVertices"    );
  declareProperty("MuonsKey"          , m_muons_key           ="Muons"              );
  declareProperty("TruthContainerKey" , m_truth_container_key ="TruthParticles"     );
  declareProperty("TrackMinPT"        , m_pt_cut              =400                  );
  declareProperty("TrackSelectionTool", m_trkSelTool                                );
  declareProperty("MuonSelectionTool" , m_muonSelection                             );
}



StatusCode VertexReco::initialize(){
   ATH_MSG_INFO("Starting initialize()");

//retreive the tools //THIS IS OPTIONAL!
//-------------------------------------------------------------------------
   CHECK( m_trkSelTool.retrieve() );
   CHECK( m_muonSelection.retrieve() );
//-------------------------------------------------------------------------


// retrieving the vertex fitting tool
  if(m_fitterSvc.retrieve().isFailure()) {
    ATH_MSG_ERROR("Can't find Trk::TrkVKalVrtFitter");
    return StatusCode::FAILURE;
  } 
  else{
    ATH_MSG_DEBUG("SOUMYA:: Trk::TrkVKalVrtFitter found");
    m_VKVFitter = dynamic_cast< Trk::TrkVKalVrtFitter* > (&(*m_fitterSvc));
  }


//Create and Book OutPutTree
//-------------------------------------------------------------------------
   char name[600];
   sprintf(name,"output_ptmin%d.root",m_pt_cut);
   m_OutFile=new TFile(name,"recreate");
   InitMuonBranches();




   h_pt_match =new TH2D("h_pt_match" ,";p_{T}^{truth} [GeV];#Delta p_{T} [geV]",100,0,10,100,-5,5);
   h_eta_match=new TH2D("h_eta_match",";p_{T}^{truth} [GeV];#Delta #eta"       ,100,0,10,100,-5,5);
   h_phi_match=new TH2D("h_phi_match",";p_{T}^{truth} [GeV];#Delta phi"        ,100,0,10,100,-5,5);

   h_b_Event=new TH1D("h_b_Event","h_b_Event",12,-0.5,11.5);
	 {
		 int  icut=1;
     h_b_Event->GetXaxis()->SetBinLabel(icut++,"All-events");
     h_b_Event->GetXaxis()->SetBinLabel(icut++,"b-events");
     h_b_Event->GetXaxis()->SetBinLabel(icut++,"b->#mu-events");
     h_b_Event->GetXaxis()->SetBinLabel(icut++,"b->#mu4-events");
     h_b_Event->GetXaxis()->SetBinLabel(icut++,"b->#mu4 |#eta|<2.5");
     h_b_Event->GetXaxis()->SetBinLabel(icut++,"c-events");
     h_b_Event->GetXaxis()->SetBinLabel(icut++,"c->#mu-events");
     h_b_Event->GetXaxis()->SetBinLabel(icut++,"c->#mu4-events");
     h_b_Event->GetXaxis()->SetBinLabel(icut++,"c->#mu4 |#eta|<2.5");
     h_b_Event->GetXaxis()->SetBinLabel(icut++,"b && c");
     h_b_Event->GetXaxis()->SetBinLabel(icut++,"b || c");
     h_b_Event->GetXaxis()->SetBinLabel(icut++,"!(b||c)");
   }

   for(int itype=0;itype<EVENT_TYPE;itype++){
     for(int ipid=0;ipid<PID_TYPE;ipid++){
			 sprintf(name,"h_truth_spectra_type%d_pid%d",itype,ipid);
       h_truth_spectra[itype][ipid]=new TH1D(name,";p_{T} [GeV];N",300,0,30);
		 }
	 }
   for(int ipid=0;ipid<PID_TYPE;ipid++){
	   sprintf(name,"h_momentum_imbalance_truth%d",ipid);
	   h_momentum_imbalance_truth[ipid]=new TH1D(name,";#Delta p/p_{ID};N_{#mu}",100,-1,1);
	 }


   h_muon_pt =new TH1D("h_muon_pt" ,";pT",100,0  ,100);
   h_muon_cut=new TH1D("h_muon_cut",";Cut Flow",18,-0.5,17.5);
	 {
     TAxis *axis=h_muon_cut->GetXaxis();
     int icut=1;
     axis->SetBinLabel(icut++, "All Muons");//0
     axis->SetBinLabel(icut++, "Combined");//1
     axis->SetBinLabel(icut++, "p_{T}>4GeV");//2
     axis->SetBinLabel(icut++, "Tight");//3
     axis->SetBinLabel(icut++, "IdTrack");//4
     axis->SetBinLabel(icut++, "Idtrack>400MeV");//5
     axis->SetBinLabel(icut++, "Idtrack Quality");//6
     axis->SetBinLabel(icut++, "Truth");//7
     axis->SetBinLabel(icut++, "Truth=primary");//8
     axis->SetBinLabel(icut++, "Truth=#pi/muon");//9
     axis->SetBinLabel(icut++, "Truth=muon");//10
     axis->SetBinLabel(icut++, "b/c-event");//11
     axis->SetBinLabel(icut++, "b-event");//12
     axis->SetBinLabel(icut++, "b/c-match");//13
     axis->SetBinLabel(icut++, "b-match");//14
	 }

   h_bmuon_track_cut=new TH1D("h_bmuon_track_cut",";;",10,-0.5,9.5);

   for(int itype=0;itype<MATCHED_OR_UNMATCHED;itype++){
     sprintf(name,"h_d0_type%d",itype);
     h_muon_d0[itype]=new TH1D(name,";|d0| [mm];N_{#mu}",1000, 0, 50);
     sprintf(name,"h_z0_type%d",itype);
     h_muon_z0[itype]=new TH1D(name,";|z0sin(#theta)| [mm];N_{#mu}",1000, 0, 50);
     sprintf(name,"h_muon_momentum_imbalance_type%d",itype);
     h_muon_momentum_imbalance[itype]=new TH1D(name,";#Delta p/p_{ID};N_{#mu}",100,-2,2);

     sprintf(name,"h_muon_truth_dx_type%d",itype);
     h_muon_truth_dx[itype]=new TH1D(name,";|#Delta x| [mm];N_{#mu}",1000, 0, 50);
     sprintf(name,"h_muon_truth_dy_type%d",itype);
     h_muon_truth_dy[itype]=new TH1D(name,";|#Delta y| [mm];N_{#mu}",1000, 0, 50);
     sprintf(name,"h_muon_truth_dz_type%d",itype);
     h_muon_truth_dz[itype]=new TH1D(name,";|#Delta z| [mm];N_{#mu}",1000, 0, 50);
     sprintf(name,"h_muon_truth_dT_type%d",itype);
     h_muon_truth_dT[itype]=new TH1D(name,";#Delta T [mm];N_{#mu}",1000, 0, 50);
     sprintf(name,"h_muon_truth_dR_type%d",itype);
     h_muon_truth_dR[itype]=new TH1D(name,";#Delta R [mm];N_{#mu}",1000, 0, 50);
   }

   
   for(int itype=0;itype<NTYPE;itype++){
     int nclasses=0;
     if     (itype==TRACK_TRACK) nclasses=NCLASSES_TRACK_TRACK;
     else if(itype==MUON_TRACK ) nclasses=NCLASSES_MUON_TRACK ;
     else{
       std::cout<<"Unknown class"<<std::endl;
       throw std::exception();
     }

     sprintf(name,"h_FitSuccess_type%d",itype);
     h_FitSuccess[itype]=new TH1D(name,";Pair Type;N_{pairs}",nclasses,-0.5,nclasses-0.5);
     sprintf(name,"h_FitMiss_type%d",itype);
     h_FitMiss   [itype]=new TH1D(name,";Pair Type;N_{pairs}",nclasses,-0.5,nclasses-0.5);
     for(int pair_class=0;pair_class<nclasses;pair_class++){
       sprintf(name,"h_DZvtx_type%d_class%d",itype,pair_class);
       h_DZvtx[itype][pair_class]=new TH1D(name,";#Delta Z [mm];N_{vtx}", 100, -5, 5);
       sprintf(name,"h_DXvtx_type%d_class%d",itype,pair_class);
       h_DXvtx[itype][pair_class]=new TH1D(name,";#Delta X [mm];N_{vtx}", 100, -5, 5);
       sprintf(name,"h_DYvtx_type%d_class%d",itype,pair_class);
       h_DYvtx[itype][pair_class]=new TH1D(name,";#Delta Y [mm];N_{vtx}", 100, -5, 5);
       sprintf(name,"h_DRvtx_type%d_class%d",itype,pair_class);
       h_DRvtx[itype][pair_class]=new TH1D(name,";#Delta R [mm];N_{vtx}", 100,  0,10);
       sprintf(name,"h_DTvtx_type%d_class%d",itype,pair_class);
       h_DTvtx[itype][pair_class]=new TH1D(name,";#Delta T [mm];N_{vtx}", 100,  0,10);
  
       sprintf(name,"h_chi2_ndf_type%d_class%d",itype,pair_class);
       h_chi2_ndf[itype][pair_class]=new TH1D(name,";#chi^{2}/ndf;N_{vtx}",100,0,5);
  
       sprintf(name,"h_ndf_type%d_class%d",itype,pair_class);
       h_ndf     [itype][pair_class]=new TH1D(name,";ndf;N_{vtx}",10,0,10);
     }
   }
//-------------------------------------------------------------------------


   return StatusCode::SUCCESS;
}


void VertexReco::InitMuonBranches(){
	 m_OutTree=new TTree("MuonTree","MuonTree");
	 m_OutTree->Branch("m_muon_pt"								,&m_muon_pt                , "m_muon_pt/F");
	 m_OutTree->Branch("m_muon_eta"								,&m_muon_eta               , "m_muon_eta/F");
	 m_OutTree->Branch("m_muon_phi"								,&m_muon_phi							 , "m_muon_phi/F");
	 m_OutTree->Branch("m_muon_trk_d0"						,&m_muon_trk_d0						 , "m_muon_trk_d0/F");
	 m_OutTree->Branch("m_muon_trk_z0sintheta"		,&m_muon_trk_z0sintheta		 , "m_muon_trk_z0sintheta/F");
	 m_OutTree->Branch("m_muon_momentum_imbalance",&m_muon_momentum_imbalance, "m_muon_momentum_imbalance/F");
	 m_OutTree->Branch("m_muon_truth_id"      		,&m_muon_truth_id          , "m_muon_truth_id/I");
	 m_OutTree->Branch("m_muon_truth_barcode"     ,&m_muon_truth_barcode     , "m_muon_truth_barcode/I");
	 m_OutTree->Branch("m_muon_truth_dx"      		,&m_muon_truth_dx          , "m_muon_truth_dx/F");
	 m_OutTree->Branch("m_muon_truth_dy"      		,&m_muon_truth_dy          , "m_muon_truth_dy/F");
	 m_OutTree->Branch("m_muon_truth_dz"      		,&m_muon_truth_dz          , "m_muon_truth_dz/F");
	 m_OutTree->Branch("m_muon_truth_dt"      		,&m_muon_truth_dt          , "m_muon_truth_dt/F");
	 m_OutTree->Branch("m_muon_truth_dr"      		,&m_muon_truth_dr          , "m_muon_truth_dr/F");
	 m_OutTree->Branch("m_muon_parent_pdgid"  		,&m_muon_parent_pdgid      , "m_muon_parent_pdgid/I");
	 m_OutTree->Branch("m_muon_parent_barcode"		,&m_muon_parent_barcode    , "m_muon_parent_barcode/I");
	 m_OutTree->Branch("m_muon_type"          		,&m_muon_type              , "m_muon_type/F");
	 m_OutTree->Branch("m_track_pt"								,&m_track_pt);
	 m_OutTree->Branch("m_track_eta"							,&m_track_eta);
	 m_OutTree->Branch("m_track_phi"							,&m_track_phi);
	 m_OutTree->Branch("m_track_truth_pdgid"			,&m_track_truth_pdgid);
	 m_OutTree->Branch("m_track_truth_barcode" 	  ,&m_track_truth_barcode);
	 m_OutTree->Branch("m_track_parent_pdgid"			,&m_track_parent_pdgid);
	 m_OutTree->Branch("m_track_parent_barcode"		,&m_track_parent_barcode);
	 m_OutTree->Branch("m_track_type"							,&m_track_type);
	 m_OutTree->Branch("m_track_vtx_chi2_ndf"			,&m_track_vtx_chi2_ndf);
	 m_OutTree->Branch("m_track_vtx_dx"						,&m_track_vtx_dx);
	 m_OutTree->Branch("m_track_vtx_dy"						,&m_track_vtx_dy);
	 m_OutTree->Branch("m_track_vtx_dz"						,&m_track_vtx_dz);
	 m_OutTree->Branch("m_track_vtx_dr"						,&m_track_vtx_dr);
	 m_OutTree->Branch("m_track_vtx_dt"						,&m_track_vtx_dt);
	 //m_OutTree->Branch("",&);
}


void VertexReco::ResetMuonBranches(){
  m_muon_pt=-1000;
	m_muon_eta=-1000;
	m_muon_phi=-1000;
	m_muon_momentum_imbalance=-1000;
  m_muon_trk_d0        =-1000;
  m_muon_trk_z0sintheta=-1000;
	m_muon_type          =-1000;

	m_muon_truth_id      =-1000;
	m_muon_truth_barcode =-1000;
  m_muon_truth_dx      =-1000; 
  m_muon_truth_dy      =-1000; 
  m_muon_truth_dz      =-1000; 
  m_muon_truth_dt      =-1000; 
  m_muon_truth_dr      =-1000; 

	m_muon_parent_pdgid  =-1000;
	m_muon_parent_barcode=-1000;

	m_track_pt  .clear();
  m_track_eta .clear();
	m_track_phi .clear();
	m_track_type.clear();

	m_track_truth_pdgid   .clear();
	m_track_truth_barcode .clear();
	m_track_parent_pdgid  .clear();
	m_track_parent_barcode.clear();

	m_track_vtx_chi2_ndf  .clear();
	m_track_vtx_dx.clear();
	m_track_vtx_dy.clear();
	m_track_vtx_dz.clear();
  m_track_vtx_dr.clear();
	m_track_vtx_dt.clear();
}


void VertexReco::FillMuon(const xAOD::Muon* Muon,
		                      const xAOD::TruthParticle* truth,
													const xAOD::TruthVertex* muon_prod_vertex, 
													const xAOD::Vertex* priVtx){
	m_muon_pt =Muon->pt()/1e3;
	m_muon_eta=Muon->eta();
	m_muon_phi=Muon->phi();
	m_muon_momentum_imbalance=MomentumImbalance(Muon);
  //m_muon_type=

  const xAOD::TrackParticle*idTrk= Muon->trackParticle( xAOD::Muon::InnerDetectorTrackParticle);//ID Track
	if(!idTrk) {ATH_MSG_ERROR("idTrk not found");throw std::exception();}
  m_muon_trk_d0        =idTrk->d0();
	m_muon_trk_z0sintheta=(idTrk->z0()+idTrk->vz()-priVtx->z())*sin(idTrk->theta());

  

	if(!truth) return;
	m_muon_truth_id     =truth->pdgId();
	m_muon_truth_barcode=truth->barcode();

  if(muon_prod_vertex){
	  double dx=muon_prod_vertex->x()-priVtx->x();
	  double dy=muon_prod_vertex->y()-priVtx->y();
	  double dz=muon_prod_vertex->z()-priVtx->z();
	  m_muon_truth_dx     =dx;
	  m_muon_truth_dy     =dy;
	  m_muon_truth_dz     =dz;
	  m_muon_truth_dt     =sqrt(dx*dx+dy*dy);
	  m_muon_truth_dr     =sqrt(dx*dx+dy*dy+dz*dz);
	}

  const xAOD::TruthParticle* parent=truth->parent();
	if(!parent) return;
  m_muon_parent_pdgid  =parent->pdgId();
	m_muon_parent_barcode=parent->barcode();
}



void VertexReco::FillMuonSV(const xAOD::TrackParticle* track,
		            const xAOD::TruthParticle* track_associated_truth,
		            const xAOD::Vertex* fit_vertex,
								const xAOD::Vertex* priVtx 
								){

	m_track_pt						.push_back(-1000);
	m_track_eta						.push_back(-1000);
	m_track_phi						.push_back(-1000);
	m_track_type					.push_back(-1000);
  m_track_truth_pdgid		.push_back(-1000);
  m_track_truth_barcode	.push_back(-1000);
	m_track_parent_pdgid	.push_back(-1000);
	m_track_parent_barcode.push_back(-1000);
	m_track_vtx_chi2_ndf	.push_back(-1000);
	m_track_vtx_dx				.push_back(-1000);
	m_track_vtx_dy				.push_back(-1000);
	m_track_vtx_dz				.push_back(-1000);
	m_track_vtx_dt				.push_back(-1000);
	m_track_vtx_dr				.push_back(-1000);

  const unsigned int size=m_track_pt.size()-1;

	m_track_pt [size]=(track->pt()/1e3);
	m_track_eta[size]=(track->eta());
	m_track_phi[size]=(track->phi());
  //m_track_type[size]=


	if(track_associated_truth){
		m_track_truth_pdgid  [size]=(track_associated_truth->pdgId  ());
		m_track_truth_barcode[size]=(track_associated_truth->barcode());
    const xAOD::TruthParticle* parent=track_associated_truth->parent();
		if(parent){
			m_track_parent_pdgid  [size]=(parent->pdgId());
			m_track_parent_barcode[size]=(parent->barcode());
		}
	}

	if(fit_vertex){
    double ndf=fit_vertex->numberDoF();
    if(ndf!=0.0){m_track_vtx_chi2_ndf[size]=fit_vertex->chiSquared()/ndf;}

		double dx=priVtx->x()-fit_vertex->x();
		double dy=priVtx->y()-fit_vertex->y();
		double dz=priVtx->z()-fit_vertex->z();

		m_track_vtx_dx[size]=dx;
		m_track_vtx_dy[size]=dy;
		m_track_vtx_dz[size]=dz;
		m_track_vtx_dt[size]=sqrt(dx*dx +dy*dy+ dz*dz);
		m_track_vtx_dr[size]=sqrt(dx*dx +dy*dy);
	}
}


StatusCode VertexReco::execute(){
   ATH_MSG_DEBUG("Starting execute()");

   bool pass_cleaning_cuts=true;
   if(m_EventInfo_key!="" && m_vtx_container_key!="") CHECK(CleaningCuts(pass_cleaning_cuts));
   if(!pass_cleaning_cuts) return StatusCode::SUCCESS;

   //CHECK(ProcessTracks());
   CHECK(ProcessMuons ());

   return StatusCode::SUCCESS;
}


void VertexReco::SetBStatus(const xAOD::TruthParticle* track,
                            int b_index,
                            std::map<const xAOD::TruthParticle*,int>& l_truth_b_index){
  if(l_truth_b_index.find(track)!=l_truth_b_index.end()) return;
  l_truth_b_index[track]=b_index;
  size_t Nchildren= track->nChildren();
  for(size_t i=0;i<Nchildren;i++){
    const xAOD::TruthParticle* child=track->child(i);
    SetBStatus(child,b_index,l_truth_b_index);
  }
}


bool VertexReco::IsPrimaryParticle(const xAOD::TruthParticle* particle){
  if(!particle) return false;
	if(particle->status()!=1                             ) return false;
  if(particle->barcode()>=2e5 || particle->barcode()==0) return false;
  if(particle->charge()==0                             ) return false;
  return true;
}


double VertexReco::MomentumImbalance(const xAOD::Muon* Muon)
{
  const xAOD::TrackParticle*idTrk= Muon->trackParticle( xAOD::Muon::InnerDetectorTrackParticle);//ID Track
  const xAOD::TrackParticle*meTrk= Muon->trackParticle( xAOD::Muon::ExtrapolatedMuonSpectrometerTrackParticle);//ME Track
  if(!meTrk || !idTrk){
    if(!meTrk) ATH_MSG_ERROR("meTrk Not Found for muon");
    if(!idTrk) ATH_MSG_ERROR("idTrk Not Found for muon");
    return -1;
  }
  double deltaP_Over_P=(idTrk->p4().P() - meTrk->p4().P())/idTrk->p4().P();
  return deltaP_Over_P;
}


StatusCode VertexReco::ProcessMuons(){
   static int count=0;
   std::cout<<"SOUMYA "<<count++<<std::endl;


   /*-----------------------------------------------------------------------------
    *  Get The truth particle container
    *-----------------------------------------------------------------------------*/
   const xAOD::TruthParticleContainer *l_TruthParticleContainer=nullptr;                               
   CHECK(evtStore()->retrieve(l_TruthParticleContainer, m_truth_container_key));       


   /*-----------------------------------------------------------------------------
    *  Check if event is b-event
    *  if so,  mark the truth particles coming from the b's
    *  Also check if the event is b->mu event
    *-----------------------------------------------------------------------------*/
   std::map<const xAOD::TruthParticle*,int>l_truth_b_index;
   l_truth_b_index.clear();
   bool is_b_event    =false;
   bool is_b_mu_event =false;
   bool is_b_mu4_event=false;
   bool is_b_mu4_eta25_event=false;
   {
     int b_index=0;
     for(const auto* truth_particle:*l_TruthParticleContainer){
       if(truth_particle->absPdgId()!=5) continue; //b-quark
       if(l_truth_b_index.find(truth_particle) != l_truth_b_index.end()) continue;
       SetBStatus(truth_particle,b_index,l_truth_b_index);
       b_index++;
       is_b_event=true;
     }
     for(auto& pair:l_truth_b_index){
        auto child=pair.first;
        if(!IsPrimaryParticle(child)) continue;
        if(child->absPdgId()==13){
					is_b_mu_event=true;
          if(child->pt()>4000){
						is_b_mu4_event=true;
            if(fabs(child->eta())<2.5){
							is_b_mu4_eta25_event=true;
						  break;
					  }
					}
				}
     }
   }


   /*-----------------------------------------------------------------------------
    *  Check if the event is c event (exclude case where the c comes from b)
    *  if so,  mark the truth particles coming from the c's
    *  Also check if the event is c->mu event
    *-----------------------------------------------------------------------------*/
   std::map<const xAOD::TruthParticle*,int>l_truth_c_index;
   l_truth_c_index.clear();
   bool is_c_event   =false;
   bool is_c_mu_event=false;
   bool is_c_mu4_event=false;
   bool is_c_mu4_eta25_event=false;
   {
     int c_index=0;
     for(const auto* truth_particle:*l_TruthParticleContainer){
       if(truth_particle->absPdgId()!=4) continue; //c-quark
       if(l_truth_b_index.find(truth_particle) != l_truth_b_index.end()) continue;//exclude b decays
       if(l_truth_c_index.find(truth_particle) != l_truth_c_index.end()) continue;
       SetBStatus(truth_particle,c_index,l_truth_c_index);
       c_index++;
       is_c_event=true;
     }
     for(auto& pair:l_truth_c_index){
        auto child=pair.first;
        if(!IsPrimaryParticle(child)) continue;
        if(child->absPdgId()==13){
					is_c_mu_event=true;
          if(child->pt()>4000){
						is_c_mu4_event=true;
            if(fabs(child->eta())<2.5){
							is_c_mu4_eta25_event=true;
					    break;
					  }
					}
				}
     }
   }


   /*-----------------------------------------------------------------------------
    *  Fill Event-Type histogram
    *-----------------------------------------------------------------------------*/
                                     h_b_Event->Fill(0);
	 if     (is_b_event              ) h_b_Event->Fill(1);
   if     (is_b_mu_event           ) h_b_Event->Fill(2);
   if     (is_b_mu4_event          ) h_b_Event->Fill(3);
   if     (is_b_mu4_eta25_event    ) h_b_Event->Fill(4);
   if     (is_c_event              ) h_b_Event->Fill(5);
   if     (is_c_mu_event           ) h_b_Event->Fill(6);
   if     (is_c_mu4_event          ) h_b_Event->Fill(7);
   if     (is_c_mu4_eta25_event    ) h_b_Event->Fill(8);
   if     (is_c_event && is_b_event) h_b_Event->Fill(9);
   if     (is_c_event || is_b_event) h_b_Event->Fill(10);
   else                              h_b_Event->Fill(11);


   /*-----------------------------------------------------------------------------
    *  Fill spectra of truth primary muons and pions
    *-----------------------------------------------------------------------------*/
   for(const auto* truth_particle:*l_TruthParticleContainer){
     if(!IsPrimaryParticle(truth_particle)) continue;
     int   id=truth_particle->absPdgId();
     float pt=truth_particle->pt()/1e3;
     if(id==13 ) h_truth_spectra[INCLUSIVE][PID_MUON]->Fill(pt);
     if(id==211) h_truth_spectra[INCLUSIVE][PID_PION]->Fill(pt);
   }
   for(auto& pair:l_truth_b_index){
     auto truth_particle=pair.first;
     if(!IsPrimaryParticle(truth_particle)) continue;
     int   id=truth_particle->absPdgId();
     float pt=truth_particle->pt()/1e3;
     if(id==13 ) h_truth_spectra[B_DECAY][PID_MUON]->Fill(pt);
     if(id==211) h_truth_spectra[B_DECAY][PID_PION]->Fill(pt);
   }



   /*-----------------------------------------------------------------------------
    *  Get the primary vertex
    *-----------------------------------------------------------------------------*/
   const xAOD::VertexContainer *l_VertexContainer = nullptr;
   CHECK(evtStore()->retrieve(l_VertexContainer,m_vtx_container_key));
   const auto* priVtx = *(l_VertexContainer->cbegin());
   if (priVtx->vertexType() != xAOD::VxType::PriVtx) {
     ATH_MSG_ERROR( "First vertex is not of type \"Primary Vertex\"." );
     return StatusCode::FAILURE;
   }


   /*-----------------------------------------------------------------------------
    *  Get The TrackParticle container
		*  Make distributions of pt, eta, phi wrt truth
    *-----------------------------------------------------------------------------*/
   const xAOD::TrackParticleContainer *l_TrackParticleContainer = nullptr;
   CHECK(evtStore()->retrieve(l_TrackParticleContainer,m_trk_container_key));
   for(const auto* track:*l_TrackParticleContainer){
		 if(TrackQuality(track,priVtx)<MINBIAS_ALL) continue; //MinBias tracks (with pointing cuts)
     const xAOD::TruthParticle *associated_truth=nullptr;
		 auto ptruthContainer=(track->auxdata<ElementLink<xAOD::TruthParticleContainer>>("truthParticleLink"));
     if(ptruthContainer.isValid() && track->auxdata<float>("truthMatchProbability")>0.5) associated_truth=*ptruthContainer;
     if(!associated_truth){ 
			 continue;
		 }

		 double pt1 =track->pt()/1e3, pt2 =associated_truth->pt()/1e3;
		 double eta1=track->eta()   , eta2=associated_truth->eta();
		 double phi1=track->phi()   , phi2=associated_truth->phi();

		 h_pt_match ->Fill(pt1,pt1-pt2);
		 h_eta_match->Fill(pt1,eta1-eta2);
		 h_phi_match->Fill(pt1,phi1-phi2);
	 }


   /*-----------------------------------------------------------------------------
    *  Get the Muon container and loop over all muons
    *-----------------------------------------------------------------------------*/
   const xAOD::MuonContainer *l_muons;
   CHECK(evtStore()->retrieve(l_muons,m_muons_key));
   for(auto Muon:*l_muons){
     int cut=0;
     h_muon_cut->Fill(cut++);//0
     if(Muon->muonType() !=xAOD::Muon::MuonType::Combined) continue;//Use only combined muons
     h_muon_cut->Fill(cut++);//1
     h_muon_pt ->Fill(Muon->pt()/1e3);
     if(Muon->pt()<4000                                  ) continue;//Only use 4GeV and above muons;
     h_muon_cut->Fill(cut++);//2

     //if(Muon->quality()  !=xAOD::Muon::Tight             ) continue;//use Tight muons, should tighten this further
     xAOD::Muon::Quality my_quality = m_muonSelection->getQuality(*Muon);
     if(my_quality!=xAOD::Muon::Tight) continue;
     h_muon_cut->Fill(cut++);//3
     /*-----------------------------------------------------------------------------
      * The id-track corresponding to the muon
      * it should pass all MB tracking cuts except for pointing cuts (quality>=0)
      *-----------------------------------------------------------------------------*/
     const xAOD::TrackParticle*idTrk= Muon->trackParticle(xAOD::Muon::InnerDetectorTrackParticle);//ID Track
     if(!idTrk) {ATH_MSG_ERROR("idTrk Not Found for muon"); continue;}
     h_muon_cut->Fill(cut++);//4
     if(idTrk->pt()<m_pt_cut) continue;
     h_muon_cut->Fill(cut++);//5
		 int idTrk_quality=TrackQuality(idTrk,priVtx);
     if(idTrk_quality<MINBIAS_TOPOLOGICAL) continue;//id track passes all topological cuts
     h_muon_cut->Fill(cut++);//6


		 /*-----------------------------------------------------------------------------
			*  Truth cuts on the reconstructed muon
			*  These cannot be applied on the data and have been removed
			*-----------------------------------------------------------------------------*/
     bool is_true_muon=false;
		 {
       const xAOD::TruthParticle *muon_associated_truth=nullptr;
       auto ptruthContainer=(idTrk->auxdata<ElementLink<xAOD::TruthParticleContainer>>("truthParticleLink"));
       if(ptruthContainer.isValid() && (idTrk->auxdata<float>("truthMatchProbability")>0.5)) muon_associated_truth=*ptruthContainer;

			 if(muon_associated_truth) h_muon_cut->Fill(cut);//7 "Truth"
			 cut++;
       if(IsPrimaryParticle(muon_associated_truth)) h_muon_cut->Fill(cut);//8 "Truth=primary"
       cut++;

       int id=-1000;
			 if(muon_associated_truth) id=muon_associated_truth->absPdgId();

			 if(id==13 || id==211){
				 h_muon_cut->Fill(cut);//9 "Truth=pi/mu"
			   if(id==211){
				   h_momentum_imbalance_truth[PID_PION]->Fill(MomentumImbalance(Muon));
			   }
			   else if(id==13 ){
					 is_true_muon=true;
				   h_momentum_imbalance_truth[PID_MUON]->Fill(MomentumImbalance(Muon));
			   }
			 }
			 cut++;

       if(id==13) h_muon_cut->Fill(cut);//10 "Truth=mu"
       cut++;
     }



     if(is_b_event|| is_c_event) h_muon_cut->Fill(cut);//11
     cut++;
     if(is_b_event) h_muon_cut->Fill(cut);//12
     cut++;

     /*-----------------------------------------------------------------------------
      *  The "b" index of this muon
      *-----------------------------------------------------------------------------*/
     int muon_b_index=-1;
     int muon_c_index_for_b_event=-1;
     if(is_b_event){
       auto ptruthContainer=(idTrk->auxdata<ElementLink<xAOD::TruthParticleContainer>>("truthParticleLink" ));
       if(ptruthContainer.isValid()){
         const xAOD::TruthParticle *associated_truth=*ptruthContainer;

         if(l_truth_b_index.find(associated_truth)!=l_truth_b_index.end()){
           if(idTrk->auxdata<float>("truthMatchProbability")>0.5){
             muon_b_index=l_truth_b_index[associated_truth];
           }
         }
         if(l_truth_c_index.find(associated_truth)!=l_truth_c_index.end()){
           if(idTrk->auxdata<float>("truthMatchProbability")>0.5){
             muon_c_index_for_b_event=l_truth_c_index[associated_truth];
           }
         }
       }
     }
     if(is_b_event && (muon_b_index>=0 || muon_c_index_for_b_event>=0)) h_muon_cut->Fill(cut);//13
     cut++;
     if(is_b_event && muon_b_index>=0) h_muon_cut->Fill(cut);//14
     cut++;


     /*-----------------------------------------------------------------------------
      *  The truth track and production vertex for this muon
      *-----------------------------------------------------------------------------*/
     const xAOD::TruthParticle *muon_associated_truth=nullptr;
     const xAOD::TruthVertex   *muon_prod_vertex     =nullptr;
     auto ptruthContainer=(idTrk->auxdata<ElementLink<xAOD::TruthParticleContainer>>("truthParticleLink" ));
     if(ptruthContainer.isValid() && idTrk->auxdata<float>("truthMatchProbability")>0.5) muon_associated_truth=*ptruthContainer;

     if(muon_associated_truth) muon_prod_vertex=muon_associated_truth->prodVtx();
     float truth_vertex_dx=1000;
     float truth_vertex_dy=1000;
     float truth_vertex_dz=1000;
     float truth_vertex_dT=1000;
     float truth_vertex_dR=1000;
     if(muon_prod_vertex){
       truth_vertex_dx=fabs(muon_prod_vertex->x()-priVtx->x());
       truth_vertex_dy=fabs(muon_prod_vertex->y()-priVtx->y());
       truth_vertex_dz=fabs(muon_prod_vertex->z()-priVtx->z());
       truth_vertex_dT=sqrt(pow(truth_vertex_dx,2.0)+pow(truth_vertex_dy,2.0));
       truth_vertex_dR=sqrt(pow(truth_vertex_dT,2.0)+pow(truth_vertex_dz,2.0));
     }



     /*-----------------------------------------------------------------------------
      *  Compare distributions for b-decay and non-b decay muons
      *  d0, z0sin(theta) 
      *  Also the Momentum imbalance && pT spectra 
      *  as well as the position of the muon's truth production vertex
      *-----------------------------------------------------------------------------*/
     float d0      =fabs(idTrk->d0());
     float z0_wrtPV=fabs((idTrk->z0()+idTrk->vz()-priVtx->z())*idTrk->theta());
     int                 muon_type=NO_B_MATCH;
     if(muon_b_index>=0) muon_type=B_MATCH; 
     h_muon_d0                [muon_type]->Fill(d0);
     h_muon_z0                [muon_type]->Fill(z0_wrtPV);
     h_muon_momentum_imbalance[muon_type]->Fill(MomentumImbalance(Muon));


     if(muon_prod_vertex){
       h_muon_truth_dx[muon_type]->Fill(truth_vertex_dx);
       h_muon_truth_dy[muon_type]->Fill(truth_vertex_dy);
       h_muon_truth_dz[muon_type]->Fill(truth_vertex_dz);
       h_muon_truth_dT[muon_type]->Fill(truth_vertex_dT);
       h_muon_truth_dR[muon_type]->Fill(truth_vertex_dR);
     }


		 /*-----------------------------------------------------------------------------
			*  Fill in the Muon Branches
			*-----------------------------------------------------------------------------*/
     ResetMuonBranches();
		 FillMuon(Muon,muon_associated_truth,muon_prod_vertex,priVtx);


     /*-----------------------------------------------------------------------------
      *  Loop over all tracks to form muon-track pairs for vertex reconstruction
      *-----------------------------------------------------------------------------*/
     for(const auto* track:*l_TrackParticleContainer){
       int cut_trk=0;
       h_bmuon_track_cut->Fill(cut_trk++);//0
       if(track==idTrk) continue;
       h_bmuon_track_cut->Fill(cut_trk++);//1
       if(track->pt()<m_pt_cut) continue;
       h_bmuon_track_cut->Fill(cut_trk++);//2
       int quality=TrackQuality(track,priVtx);
       if(quality<MINBIAS_TOPOLOGICAL) continue;
       h_bmuon_track_cut->Fill(cut_trk++);//3

       /*-----------------------------------------------------------------------------
        * The "b" index of this track 
        *-----------------------------------------------------------------------------*/
       int track_b_index=-1;
       const xAOD::TruthParticle *track_associated_truth=nullptr;
       if(is_b_event){
         auto ptruthContainer=(track->auxdata<ElementLink<xAOD::TruthParticleContainer>>("truthParticleLink" ));
         if(ptruthContainer.isValid()){
           if(track->auxdata<float>("truthMatchProbability")>0.5){
						 track_associated_truth=*ptruthContainer;
             if(l_truth_b_index.find(track_associated_truth)!=l_truth_b_index.end()){
                 track_b_index=l_truth_b_index[track_associated_truth];
             }
           }
         }
         h_bmuon_track_cut->Fill(cut_trk);//4
       }
       cut_trk++;
       if(track_b_index>=0 && track_b_index==muon_b_index) h_bmuon_track_cut->Fill(cut_trk);//5
       cut_trk++;


       /*-----------------------------------------------------------------------------
        * Form SV from muon(id-track for muon) and track 
        *-----------------------------------------------------------------------------*/
       std::vector<const xAOD::TrackParticle*> collection;
       collection.push_back(idTrk);
       collection.push_back(track);
       Amg::Vector3D pos_initial(priVtx->x(),priVtx->y(),priVtx->z());
       //Trk::Vertex v1(pos_initial);
       //xAOD::Vertex *fit_vertex=m_VKVFitter->fit(collection,v1);
       xAOD::Vertex *fit_vertex=m_VKVFitter->fit(collection,pos_initial);

			 /*-----------------------------------------------------------------------------
				*  Fill the SV info for the output muon Tree
				*-----------------------------------------------------------------------------*/
       FillMuonSV(track,track_associated_truth,fit_vertex,priVtx);

       for(int pair_class=0;pair_class<NCLASSES_MUON_TRACK;pair_class++){
         //cases: 
         //0. all muons+tracks
         //1. all muons+tracks (from b-events)
         //2.     muons+tracks (muon from b)
         //3.     muons+tracks both from same b
         if(pair_class==B_EVENTS_ONLY         && !is_b_event                           ) continue;
         if(pair_class==MUON_FROM_B           && muon_b_index==-1                      ) continue;
         if(pair_class==MUON_AND_TRACK_FROM_B && (muon_b_index==-1 ||track_b_index==-1)) continue;

         const int itype=MUON_TRACK;
         if(fit_vertex){
           h_FitSuccess[itype]->Fill(pair_class);
           double dx=fit_vertex->x()-priVtx->x();
           double dy=fit_vertex->y()-priVtx->y();
           double dz=fit_vertex->z()-priVtx->z();
           double dr=sqrt(dx*dx +dy*dy+ dz*dz);
           double dt=sqrt(dx*dx +dy*dy);
           h_DXvtx[itype][pair_class]->Fill(dx);     
           h_DYvtx[itype][pair_class]->Fill(dy);     
           h_DZvtx[itype][pair_class]->Fill(dz);     
           h_DRvtx[itype][pair_class]->Fill(dr);     
           h_DTvtx[itype][pair_class]->Fill(dt);     
  
           double ndf=fit_vertex->numberDoF();
                        h_ndf     [itype][pair_class]->Fill(ndf);
           if(ndf!=0.0) h_chi2_ndf[itype][pair_class]->Fill(fit_vertex->chiSquared()/ndf);
         }
         else{
           h_FitMiss[itype]->Fill(pair_class);
         }
       }
       if(fit_vertex) delete fit_vertex;
     }
		 m_OutTree->Fill();
   }

   return StatusCode::SUCCESS;
}




StatusCode VertexReco::ProcessTracks(){
   static int eventnum=0;
   eventnum++;

   const xAOD::TrackParticleContainer *l_TrackParticleContainer = nullptr;
   if(evtStore()->retrieve(l_TrackParticleContainer,m_trk_container_key).isFailure()){
     ATH_MSG_ERROR("Could not retrieve TrackParticleContainer with key "<<m_trk_container_key.c_str());
     return StatusCode::FAILURE;
   }
   ATH_MSG_DEBUG("Num Tracks="<<l_TrackParticleContainer->size());

   const xAOD::VertexContainer *l_VertexContainer = nullptr;
   if(evtStore()->retrieve(l_VertexContainer,m_vtx_container_key).isFailure()){
      ATH_MSG_ERROR("Could not retrieve VxContainer with key "<<m_vtx_container_key.c_str());
      return StatusCode::FAILURE;
   }
   const auto* priVtx = *(l_VertexContainer->cbegin());
   if (priVtx->vertexType() != xAOD::VxType::PriVtx) {
     ATH_MSG_ERROR( "First vertex is not of type \"Primary Vertex\"." );
     return StatusCode::FAILURE;
   }


   std::cout<<"Starting vertex fit for event "<<eventnum<<std::endl;
   int trk1=0;
   for(auto track_itr1 =l_TrackParticleContainer->begin();track_itr1!=l_TrackParticleContainer->end();track_itr1++){
     trk1++;
     auto track1 =*track_itr1;
     int quality1=TrackQuality(track1,priVtx);
     if(quality1<MINBIAS_TOPOLOGICAL || track1->pt()<m_pt_cut) continue;


     int trk2=0;
     for(auto track_itr2 =track_itr1+1;track_itr2!=l_TrackParticleContainer->end();track_itr2++){
     trk2++;
       auto track2 =*track_itr2;
       int quality2=TrackQuality(track2,priVtx);
       if(quality2<MINBIAS_TOPOLOGICAL || track2->pt()<m_pt_cut) continue;


       std::vector<const xAOD::TrackParticle*> collection;
       collection.push_back(track1);
       collection.push_back(track2);


       Amg::Vector3D pos_initial(priVtx->x(),priVtx->y(),priVtx->z());
       //Trk::Vertex v1(pos_initial);
       //xAOD::Vertex *fit_vertex=m_VKVFitter->fit(collection,v1);
       xAOD::Vertex *fit_vertex=m_VKVFitter->fit(collection,pos_initial);

       float d0_1      = track1->d0();
       float z0_wrtPV_1= track1->z0()+track1->vz()-priVtx->z();
       float theta_1   = track1->theta();
       float d0_2      = track2->d0();
       float z0_wrtPV_2= track2->z0()+track1->vz()-priVtx->z();
       float theta_2   = track2->theta();

       for(int pair_class=0;pair_class<NCLASSES;pair_class++){
         if((pair_class==BOTH_FROM_PV ) && !(quality1==1 && quality2==1)) continue;
         if((pair_class==PV_NOT_PV    ) && !((quality1==0 && quality2==1)||(quality1==1 && quality2==0))) continue;
         if((pair_class==NOT_PV_NOT_PV) && !(quality1==0 && quality2==0)) continue;
         if((pair_class==D0LT1_D0GT2  ) && !(fabs(d0_1)<1.0 && fabs(d0_2)>2.0)) continue;

         const int itype=TRACK_TRACK;
         if(fit_vertex){
           h_FitSuccess[itype]->Fill(pair_class);
           double dx=fit_vertex->x()-priVtx->x();
           double dy=fit_vertex->y()-priVtx->y();
           double dz=fit_vertex->z()-priVtx->z();
           double dr=sqrt(dx*dx +dy*dy+ dz*dz);
           double dt=sqrt(dx*dx +dy*dy);
           h_DXvtx[itype][pair_class]->Fill(dx);     
           h_DYvtx[itype][pair_class]->Fill(dy);     
           h_DZvtx[itype][pair_class]->Fill(dz);     
           h_DRvtx[itype][pair_class]->Fill(dr);     
           h_DTvtx[itype][pair_class]->Fill(dt);     

           double ndf=fit_vertex->numberDoF();
                        h_ndf     [itype][pair_class]->Fill(ndf);
           if(ndf!=0.0) h_chi2_ndf[itype][pair_class]->Fill(fit_vertex->chiSquared()/ndf);
         
         }
         else{
           h_FitMiss[itype]->Fill(pair_class);
         }
       }
       if(fit_vertex) delete fit_vertex;
     }
   }
   std::cout<<"Ending vertex fit for event "<<eventnum<<std::endl;
   return StatusCode::SUCCESS;
}


StatusCode VertexReco::finalize(){
  m_OutFile->Write();
  return StatusCode::SUCCESS;
}





StatusCode VertexReco::CleaningCuts(bool &pass_cleaning_cuts){
   ATH_MSG_DEBUG("Starting CleaningCuts()");
   pass_cleaning_cuts=true;
   static int event_num=-1;
   event_num++;

//Event Info
//------------------------------------------------------  
   const xAOD::EventInfo* l_EventInfo = nullptr;
   if(evtStore()->retrieve(l_EventInfo,m_EventInfo_key).isFailure()){
      ATH_MSG_ERROR(" Could not retrieve EventInfo with key "<<m_EventInfo_key.c_str());
      return StatusCode::FAILURE;
   }

   if(!l_EventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
      ATH_MSG_ERROR("This code should be run over MC only");
      return StatusCode::FAILURE;
   }  
//------------------------------------------------------  



//------------------------------------------------------  
//Event by Event Cleaning
    if((l_EventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error ) || 
       (l_EventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error ) || 
       (l_EventInfo->errorState(xAOD::EventInfo::SCT)==xAOD::EventInfo::Error ) || 
       (l_EventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) ) ){
        pass_cleaning_cuts=false;
        ATH_MSG_DEBUG("Event FAILED EventLevelCleaning");
        return StatusCode::SUCCESS;
    }
    else ATH_MSG_DEBUG("Event PASSED EventLevelCleaning");
//------------------------------------------------------  



//Vertex
//------------------------------------------------------  
//Must have reconstructed vertex
   const xAOD::VertexContainer *l_VertexContainer = nullptr;
   if(evtStore()->retrieve(l_VertexContainer,m_vtx_container_key).isFailure()){
      ATH_MSG_ERROR("Could not retrieve VxContainer with key "<<m_vtx_container_key.c_str());
      return StatusCode::FAILURE;
   }
   ATH_MSG_DEBUG("Num_Vertices="<<l_VertexContainer->size());
   if(l_VertexContainer->size()<=1) {pass_cleaning_cuts=false;}
//------------------------------------------------------  
   
   return StatusCode::SUCCESS;
}



int VertexReco::TrackQuality(const xAOD::TrackParticle*track,const xAOD::Vertex *priVtx){
     int   quality=BAD;
     if (m_trkSelTool->accept(*track, priVtx)) {
       quality = MINBIAS_ALL;
     } 
     else{
       // if the track passes everything except for D0 and Z0SinTheta cuts, give it quality 0
       const auto& taccept = m_trkSelTool->getTAccept();
       static const auto d0Index = taccept.getCutPosition("D0");
       static const auto z0Index = taccept.getCutPosition("Z0SinTheta");
       static const auto nCuts = taccept.getNCuts();
       auto cutBitset = taccept.getCutResultBitSet();
       cutBitset |= (1 << d0Index) | (1 << z0Index);
       if (cutBitset.count() == nCuts) quality = MINBIAS_TOPOLOGICAL;
     }
     return quality;
}
#endif
