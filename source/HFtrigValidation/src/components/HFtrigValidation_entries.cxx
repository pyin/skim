#include "HFtrigValidation/OfflineOnlineComparisons.h"
#include "HFtrigValidation/SkimAlg3.h"
#include "HFtrigValidation/TrigRates.h"
#include "HFtrigValidation/VertexReco.h"
#include "HFtrigValidation/MuonTypes.h"
#include "GaudiKernel/DeclareFactoryEntries.h"

DECLARE_ALGORITHM_FACTORY( OfflineOnlineComparisons )
DECLARE_ALGORITHM_FACTORY( SkimAlg3 )
DECLARE_ALGORITHM_FACTORY( TrigRates )
DECLARE_ALGORITHM_FACTORY( VertexReco )
DECLARE_ALGORITHM_FACTORY( MuonTypes )

DECLARE_FACTORY_ENTRIES( HFtrigValidation ) {
    DECLARE_ALGORITHM( OfflineOnlineComparisons )
    DECLARE_ALGORITHM( SkimAlg3 )
    DECLARE_ALGORITHM( TrigRates )
    DECLARE_ALGORITHM( VertexReco )
    DECLARE_ALGORITHM( MuonTypes )
}

