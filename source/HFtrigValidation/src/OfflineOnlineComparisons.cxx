#include <HFtrigValidation/OfflineOnlineComparisons.h>

#include <GaudiKernel/ITHistSvc.h>
#include <GaudiKernel/ServiceHandle.h>

#include "xAODTracking/VertexContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTrigger/TrigDecision.h"

#include "cmath"
#include "TH1.h"
#include "TH2.h"
#include "TFile.h"

//Useful links
//http://acode-browser.usatlas.bnl.gov/lxr/source/atlas/Event/xAOD/xAODMuon/xAODMuon/versions/Muon_v1.h
//http://acode-browser2.usatlas.bnl.gov/lxr-rel20/source/atlas/InnerDetector/InDetTrigRecAlgs/InDetTrigVxSecondary/src/TrigVxSecondary.cxx


OfflineOnlineComparisons::OfflineOnlineComparisons(const std::string& name, ISvcLocator* pSvcLocator) : AthAlgorithm(name,pSvcLocator){
  declareProperty("EventInfoKey"            ,  m_EventInfo_key         ="EventInfo"          );
  declareProperty("MuonOfflineContainerKey" ,  m_muons_offline_key     ="Muons"              );
  declareProperty("MuonOnlineContainerKey"  ,  m_muons_online_key      ="HLT_xAOD__MuonContainer_MuonEFInfo");
  declareProperty("TrkContainerKey"         ,  m_tracks_key            ="InDetTrackParticles");
  declareProperty("VtxContainerKey"         ,  m_vertices_key          ="PrimaryVertices"    );
  //declareProperty("JetContainerKeys"        ,  m_jets_keys={"AntiKt2PV0TrackJets","AntiKt3PV0TrackJets","AntiKt4PV0TrackJets"});

  declareProperty("DataType"                ,  m_DataType              ="Data"           );//Data,MC
  declareProperty("MaxZvtx"                 ,  m_MaxZvtx               =250              );
  declareProperty("GRLTool"                 ,  m_grlTool                                 );
  declareProperty("UseGRL"                  ,  m_use_GRL               =true             );//true/false
}



StatusCode OfflineOnlineComparisons::initialize(){
   ATH_MSG_INFO("Starting initialize()");

//retreive the tools //THIS IS OPTIONAL!
//-------------------------------------------------------------------------
   CHECK(m_grlTool.retrieve() );
   CHECK(m_trigTool.retrieve() );
//-------------------------------------------------------------------------



//Create and Book OutPutFile
//-------------------------------------------------------------------------
   //ServiceHandle<ITHistSvc> histSvc("THistSvc",name()); 
   //CHECK( histSvc.retrieve() );

   //m_OutTree=new TTree("HeavyIonD3PD","HeavyIonD3PD");
   //CHECK(histSvc->regTree("/MYSTREAM/HeavyIonD3PD",m_OutTree));//.ignore(); //or check the statuscode;
   m_OutFile=new TFile("output.root","recreate");
//-------------------------------------------------------------------------



   return StatusCode::SUCCESS;
}





StatusCode OfflineOnlineComparisons::execute(){
   ATH_MSG_INFO("Starting execute()");

   CHECK(evtStore()->retrieve(m_muons_offline,m_muons_offline_key));
   CHECK(evtStore()->retrieve(m_muons_online ,m_muons_online_key  ));
   CHECK(evtStore()->retrieve(m_tracks       ,m_tracks_key       ));
   CHECK(evtStore()->retrieve(m_vertices     ,m_vertices_key     ));
   ATH_MSG_INFO("  NOnlineMuons=" <<m_muons_online->size()<<
                "  NOfflineMuons="<<m_muons_offline->size()<<
                "  NTracks="      <<m_tracks->size()<<
                "  NVertices="    <<m_vertices->size() );

   bool pass_cleaning_cuts=true;
   CHECK(CleaningCuts(pass_cleaning_cuts));
   if(!pass_cleaning_cuts) return StatusCode::SUCCESS;

   CHECK(SingleMuonDistributions());
   CHECK(OfflineOnlineCorrelations());

   return StatusCode::SUCCESS;
   ATH_MSG_INFO("");
}


StatusCode OfflineOnlineComparisons::SingleMuonDistributions(){
   static bool init=false;
   if(!init){
     m_OutFile->cd();
     char name1[600],name2[600],name3[600],name4[600],name5[600];
     for(int itype:{ONLINE,OFFLINE}){
       for(int iquality:{MEDIUM,TIGHT}){
         sprintf(name1,"h_CutFlow_qual%d_%s"                     ,iquality,m_MuonTypeName[itype].c_str());
         sprintf(name2,"h_Eloss_qual%d_%s"                       ,iquality,m_MuonTypeName[itype].c_str());

         h_muon_cutflow                [itype][iquality]=new TH1D(name1,";Cut;N_{muons}",10,0,10);
         h_Eloss                       [itype][iquality]=new TH2D(name2,";p_{T} [GeV];Eloss [GeV];N_{muons}"    ,20,0,20,500,1,6);
         
         for(int ieta=0;ieta<NETA;ieta++){
           sprintf(name3,"h_MomentumImbalance_qual%d_%s_%d"           ,iquality,m_MuonTypeName[itype].c_str(),ieta);
           sprintf(name4,"h_MomentumImbalance2_qual%d_%s_%d"          ,iquality,m_MuonTypeName[itype].c_str(),ieta);
           
           h_MomentumImbalance           [itype][iquality][ieta]=new TH2D(name3,";p_{T} [GeV];(p_{ID}-p_{ME})/p_{ID};N_{muons}",20,0,20,200,-1.0,1.0);
           h_MomentumImbalance2          [itype][iquality][ieta]=new TH2D(name4,";p_{T} [GeV];(p_{ID}-p_{MS}-E_{loss})/p_{ID};N_{muons}",20,0,20,200,-1.0,1.0);
            
           for(int ipt1=0;ipt1<NPT1;ipt1++){
             sprintf(name5,"h_MomentumImbalance_comparison_qual%d_%s_%d_%d",iquality,m_MuonTypeName[itype].c_str(),ipt1,ieta);
             h_MomentumImbalance_comparison[itype][iquality][ipt1][ieta]=new TH2D(name5,";(p_{ID}-p_{ME})/p_{ID};(p_{ID}-p_{MS}-E_{loss})/p_{ID};N_{muons}",200,-1.0,1.0,200,-1.0,1.0);
           }
         }
       }
     }
     init=true;
   }

   for(const int itype:{ONLINE,OFFLINE}){
     const xAOD::MuonContainer  *l_muons=m_muons_offline;//this variable "m_muons_offline" is initialized in execute() method
     if(itype==ONLINE)           l_muons=m_muons_online ;//this variable "m_muons_online"  is initialized in execute() method

     for(auto Muon:*l_muons){
       for(int iquality:{MEDIUM,TIGHT}){
         float icut=0.5;
         h_muon_cutflow[itype][iquality]->Fill(icut++);//0.5
 
         double pt=Muon->pt()/1e3;
         double eta=Muon->eta();
 
         //User only combined muons
         if(Muon->muonType() !=xAOD::Muon::MuonType::Combined) continue;
         h_muon_cutflow[itype][iquality]->Fill(icut++);//1.5
  
         //Quality
         if(iquality==MEDIUM && !(Muon->quality()==xAOD::Muon::Medium || Muon->quality()==xAOD::Muon::Tight)) continue; 
         if(iquality==TIGHT  && !(Muon->quality()==xAOD::Muon::Tight)                                       ) continue; 
         h_muon_cutflow[itype][iquality]->Fill(icut++);//2.5
  
         //Determine Eloss
         float eloss_parameterized;//paramaterized eloss
         if(!(Muon->parameter(eloss_parameterized,xAOD::Muon::ParamEnergyLoss))){
          ATH_MSG_ERROR("The parameter xAOD::Muon::ParamEnergyLoss is inaccessible");
          return StatusCode::SUCCESS;
         }
         h_muon_cutflow[itype][iquality]->Fill(icut++);//3.5
         h_Eloss       [itype][iquality]->Fill(pt,eloss_parameterized/1e3);//MeV->GeV
  
  
         const xAOD::TrackParticle*msTrk= Muon->trackParticle(xAOD::Muon::MuonSpectrometerTrackParticle);//MS Only Track
         const xAOD::TrackParticle*idTrk= Muon->trackParticle(xAOD::Muon::InnerDetectorTrackParticle);//ID Track
         const xAOD::TrackParticle*trk01= Muon->trackParticle(xAOD::Muon::CombinedTrackParticle);//Combined Track
         const xAOD::TrackParticle*meTrk= Muon->trackParticle(xAOD::Muon::ExtrapolatedMuonSpectrometerTrackParticle);//ME Track
  
         if(msTrk) h_muon_cutflow[itype][iquality]->Fill(icut);//4.5
         icut++;
         if(idTrk) h_muon_cutflow[itype][iquality]->Fill(icut);//5.5
         icut++;
         if(trk01) h_muon_cutflow[itype][iquality]->Fill(icut);//6.5
         icut++; 
         if(meTrk) h_muon_cutflow[itype][iquality]->Fill(icut);//7.5
         icut++; 
  
         double deltaP_overP;
         if(!DeltaPOverP(Muon,deltaP_overP)) return StatusCode::SUCCESS;
             
         int ieta=0;
         for(ieta=0;ieta<(NETA-1);ieta++){
           if(eta<EtaBins[ieta+1]) break;
         }
         if(ieta>=NETA) ieta=NETA-1; 
         
         int ipt1=0;
         for(ipt1=0;ipt1<(NPT1-1);ipt1++){
           if(pt<PtBins[ipt1+1]) break;
         }
         if(ipt1>=NPT1) ipt1=NPT1-1;
         
         h_MomentumImbalance[itype][iquality][ieta]->Fill(pt,deltaP_overP);
  
         if(msTrk && idTrk){
           double deltaP_overP_type2=(idTrk->p4().P() - msTrk->p4().P()-eloss_parameterized)/idTrk->p4().P();
           h_MomentumImbalance2          [itype][iquality][ieta]->Fill(pt,deltaP_overP_type2);
           h_MomentumImbalance_comparison[itype][iquality][ipt1][ieta]->Fill(deltaP_overP,deltaP_overP_type2);
         }
       }
     }
   }
   return StatusCode::SUCCESS;
}








StatusCode OfflineOnlineComparisons::OfflineOnlineCorrelations(){
   static bool init=false;
   if(!init){
     m_OutFile->cd();
     char name1[600],name2[600],name3[600],name4[600];

     sprintf(name1,"h_NMatch");
     sprintf(name2,"h_NAll"  );
     h_NMatch=new TH1D(name1,";p_{T}^{offline} [GeV];N_{muons}",50,0,20);
     h_NAll  =new TH1D(name2,";p_{T}^{offline} [GeV];N_{muons}",50,0,20);

     sprintf(name1,"h_Match_DeltaR"     );//delta R^2 = delta eta^2 +delta phi^2
     sprintf(name2,"h_Match_DeltaR_cut" );
     sprintf(name3,"h_Match_DeltapT"    );
     sprintf(name4,"h_Match_pT"         );
     h_Match_DeltaR    =new TH2D(name1,";p_{T}^{offline} [GeV];#Delta R;N_{muons}"            ,200,0,20, 400,  0, 4);
     h_Match_DeltaR_cut=new TH2D(name2,";p_{T}^{offline} [GeV];#Delta R;N_{muons}"            ,200,0,20, 400,  0, 4);
     h_Match_DeltapT   =new TH2D(name3,";p_{T}^{offline} [GeV];#Delta p_{T} [GeV];N_{muons}"  ,200,0,20, 200,-10,10);
     h_Match_pT        =new TH2D(name4,";p_{T}^{offline} [GeV];p_{T}^{online} [GeV];N_{muons}",200,0,20, 200,  0,20);

     sprintf(name1,"h_Match_ElossCorr");
     sprintf(name2,"h_Match_DeltaPOverPCorr");
     h_Match_ElossCorr      =new TH2D(name1,";Eloss Offline [GeV];Eloss Online [GeV];N_{muons}",500,   1,  6,500,   1,  6);
     h_Match_DeltaPOverPCorr=new TH2D(name2,";#Delta p/p_{ID} offline;#Delta p/p_{ID} Online"  ,200,-1.0,1.0,200,-1.0,1.0);

     for(int ipt1=0;ipt1<NPT1;ipt1++){
       sprintf(name1,"h_Match_ElossCorr_pt%d"      ,ipt1);
       sprintf(name2,"h_Match_DeltaPOverPCorr_pt%d",ipt1);
       h_Match_ElossCorr_ptbinned      [ipt1]=new TH2D(name1,";Eloss Offline [GeV];Eloss Online [GeV];N_{muons}",500,   1,  6,500,   1,  6);
       h_Match_DeltaPOverPCorr_ptbinned[ipt1]=new TH2D(name2,";#Delta p/p_{ID} offline;#Delta p/p_{ID} Online"  ,200,-1.0,1.0,200,-1.0,1.0);
     }

     for(int ieta=0;ieta<NETA;ieta++){
       sprintf(name1,"h_Match_ElossCorr_eta%d"      ,ieta);
       sprintf(name2,"h_Match_DeltaPOverPCorr_eta%d",ieta);
       h_Match_ElossCorr_etabinned      [ieta]=new TH2D(name1,";Eloss Offline [GeV];Eloss Online [GeV];N_{muons}",500,   1,  6,500,   1,  6);
       h_Match_DeltaPOverPCorr_etabinned[ieta]=new TH2D(name2,";#Delta p/p_{ID} offline;#Delta p/p_{ID} Online"  ,200,-1.0,1.0,200,-1.0,1.0);
     }
     init=true;
   }


   for(auto Muon:*m_muons_offline){
     if(Muon->muonType() !=xAOD::Muon::MuonType::Combined) continue;//Use only combined muons
     if(!(Muon->quality()==xAOD::Muon::Medium || Muon->quality()==xAOD::Muon::Tight)) continue; //Quality

//----------------------------------------------------------------------
     //pT
     double pt_offline=Muon->pt()/1e3;
     double eta_offline=Muon->eta();

     //Determine Eloss
     float eloss_parameterized_offline;//paramaterized eloss
     if(!(Muon->parameter(eloss_parameterized_offline,xAOD::Muon::ParamEnergyLoss))){
      ATH_MSG_ERROR("The parameter xAOD::Muon::ParamEnergyLoss is inaccessible");
      return StatusCode::SUCCESS;
     }

     //Delta P over P
     double deltaP_overP_offline=0;
     if(!DeltaPOverP(Muon,deltaP_overP_offline)) return StatusCode::SUCCESS;
//----------------------------------------------------------------------


     h_NAll         ->Fill(pt_offline);


     //loop over all online muons to find matched online muon
     double DeltaR=100;
     const xAOD::Muon* matched_online_muon=nullptr;
     for(auto Muon2:*m_muons_online){
       if(Muon->muonType() !=xAOD::Muon::MuonType::Combined) continue;//Use only combined muons
       if(!(Muon->quality()==xAOD::Muon::Medium || Muon->quality()==xAOD::Muon::Tight)) continue; //Quality

       double deltaR_new=Muon2->p4().DeltaR(Muon->p4());//"Muon2->p4()" gives a TLorentzVector Object 
       if(deltaR_new<DeltaR){
         DeltaR=deltaR_new;
         matched_online_muon=Muon2; 
       }
     }

     if(!matched_online_muon){
       ATH_MSG_INFO("No matching online muon found");
       continue;
     }


//----------------------------------------------------------------------
     //pT
     double pt_online=matched_online_muon->pt()/1e3;

     //Determine Eloss
     float eloss_parameterized_online;//paramaterized eloss
     if(!(matched_online_muon->parameter(eloss_parameterized_online,xAOD::Muon::ParamEnergyLoss))){
      ATH_MSG_ERROR("The parameter xAOD::Muon::ParamEnergyLoss is inaccessible for Online Muon");
      return StatusCode::FAILURE;
     }

     //Delta P over P
     double deltaP_overP_online=0;
     if(!DeltaPOverP(matched_online_muon,deltaP_overP_online)) return StatusCode::SUCCESS;
//----------------------------------------------------------------------


     h_Match_DeltaR ->Fill(pt_offline,DeltaR);
     if(DeltaR>0.02) continue;
     h_Match_DeltaR_cut->Fill(pt_offline,DeltaR);

     h_NMatch       ->Fill(pt_offline);
     h_Match_DeltapT->Fill(pt_offline,pt_offline-pt_online);
     h_Match_pT     ->Fill(pt_offline,pt_online);


     h_Match_ElossCorr      ->Fill(eloss_parameterized_offline/1e3,eloss_parameterized_online/1e3);
     h_Match_DeltaPOverPCorr->Fill(deltaP_overP_offline           ,deltaP_overP_online           );

     int ipt1=0;
     for(ipt1=0;ipt1<(NPT1-1);ipt1++){
       if(pt_offline<PtBins[ipt1+1]) break;
     }
     if(ipt1>=NPT1) ipt1=NPT1-1;
     h_Match_ElossCorr_ptbinned[ipt1]      ->Fill(eloss_parameterized_offline/1e3,eloss_parameterized_online/1e3);
     h_Match_DeltaPOverPCorr_ptbinned[ipt1]->Fill(deltaP_overP_offline           ,deltaP_overP_online           );
   
     int ieta=0;
     for(ieta=0;ieta<(NETA-1);ieta++){
       if(eta_offline<EtaBins[ieta+1]) break;
     }
     if(ieta>=NETA) ieta=NETA-1;
     h_Match_ElossCorr_etabinned[ieta]      ->Fill(eloss_parameterized_offline/1e3,eloss_parameterized_online/1e3);
     h_Match_DeltaPOverPCorr_etabinned[ieta]->Fill(deltaP_overP_offline           ,deltaP_overP_online           );
   }
   return StatusCode::SUCCESS;
}



StatusCode OfflineOnlineComparisons::finalize(){
  m_OutFile->Write();
  return StatusCode::SUCCESS;
}



bool OfflineOnlineComparisons::DeltaPOverP(const xAOD::Muon *Muon,double &deltaP_overP){
   deltaP_overP=-1;
   const xAOD::TrackParticle*idTrk= Muon->trackParticle( xAOD::Muon::InnerDetectorTrackParticle);//ID Track
   const xAOD::TrackParticle*meTrk= Muon->trackParticle( xAOD::Muon::ExtrapolatedMuonSpectrometerTrackParticle);//ME Track
   if(!meTrk || !idTrk){
     if(!meTrk) ATH_MSG_ERROR("meTrk Not Found for muon");
     if(!idTrk) ATH_MSG_ERROR("idTrk Not Found for muon");
     return false;
   }
   deltaP_overP=(idTrk->p4().P() - meTrk->p4().P())/idTrk->p4().P();
   return true;
}




StatusCode OfflineOnlineComparisons::CleaningCuts(bool &pass_cleaning_cuts){
   ATH_MSG_INFO("Starting CleaningCuts()");
   pass_cleaning_cuts=true;
   static int event_num=-1;
   event_num++;

//Event Info
//------------------------------------------------------  
//Must Pass good lumi-block
   const xAOD::EventInfo* l_EventInfo;
   if(evtStore()->retrieve(l_EventInfo,m_EventInfo_key).isFailure()){
      ATH_MSG_ERROR(" Could not retrieve EventInfo with key :"<<m_EventInfo_key.c_str());
      return StatusCode::FAILURE;
   }

   bool isMC=false;
   if(l_EventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
     isMC = true; // can do something with this later
   }  
   //check if data or MC
   if(isMC && m_DataType=="Data") {ATH_MSG_ERROR("Running over MC with Data joboptions"); return StatusCode::FAILURE;}

   UInt_t RunNumber        =l_EventInfo->runNumber();
   UInt_t lumi_block       =l_EventInfo->lumiBlock();
   UInt_t bunch_crossing_id=l_EventInfo->bcid();
   UInt_t eventNumber      =l_EventInfo->eventNumber();

   ATH_MSG_INFO("Running Event "<<event_num<<":: EventNumber="<<eventNumber<<" RunNumber="<<RunNumber<<" LumiBlock="<<lumi_block<<" BCID="<<bunch_crossing_id);

   if(m_DataType=="Data"){
     if(m_use_GRL){
       if(!m_grlTool->passRunLB(*l_EventInfo)) {
         pass_cleaning_cuts=false;
         ATH_MSG_INFO("Event FAILED GRL\n");
         return StatusCode::SUCCESS;
       }
       else ATH_MSG_INFO("Event PASSED GRL\n");
     }
     else {ATH_MSG_INFO("Not using GRL\n");}
   }
   else if(m_DataType=="MC"){
     ATH_MSG_INFO("Skipping GRL for MC\n");
   }
//------------------------------------------------------  




//------------------------------------------------------  
//Event by Event Cleaning
    if(!isMC){
      if((l_EventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error ) || 
         (l_EventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error ) || 
         (l_EventInfo->errorState(xAOD::EventInfo::SCT)==xAOD::EventInfo::Error ) || 
         (l_EventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) ) ){
          pass_cleaning_cuts=false;
          ATH_MSG_INFO("Event FAILED EventLevelCleaning");
          return StatusCode::SUCCESS;
      }
      else ATH_MSG_INFO("Event PASSED EventLevelCleaning");
    }
//------------------------------------------------------  





//Vertex
//------------------------------------------------------  
//Must have reconstructed vertex with |Z|<m_MaxZvtx
   ATH_MSG_INFO("Num_Vertices="<<m_vertices->size());
   if(m_vertices->size()<=1) {pass_cleaning_cuts=false;}

   int count=0;
   for(auto vtx=m_vertices->begin();vtx!=m_vertices->end();vtx++){
      float z_vtx   =(*vtx)->z();
      int   vtx_ntrk=(*vtx)->nTrackParticles();

      ATH_MSG_INFO("     z["<<count<<"]="<<z_vtx<<"  vtx_ntrk="<<vtx_ntrk);
      if(count==0 && fabs(z_vtx)>m_MaxZvtx) {pass_cleaning_cuts=false;}
      count++;
   }
//------------------------------------------------------  
   

   return StatusCode::SUCCESS;
}

