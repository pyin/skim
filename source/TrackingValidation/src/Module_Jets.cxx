#include "TrackingValidation/Module.h"
#include "TrackingValidation/Module_Jets.h"
#include "xAODJet/JetContainer.h"
#include "xAODPFlow/PFO.h"
#include <xAODPFlow/PFOContainer.h>
#include <xAODPFlow/PFOAuxContainer.h>
#include <xAODCore/ShallowCopy.h>
#include <TTree.h>
#include <TLorentzVector.h>





void Jets::Init(TTree *l_OutTree, int level_of_detail) {
    m_level_of_detail = level_of_detail;

    char name[600];
    // sprintf(name, "%s_E"      , m_BranchName.c_str()); l_OutTree->Branch(name, &m_E      );
    sprintf(name, "%s_pt_orig", m_BranchName.c_str()); l_OutTree->Branch(name, &m_pt_orig);
    sprintf(name, "%s_pt"     , m_BranchName.c_str()); l_OutTree->Branch(name, &m_pt     );
    sprintf(name, "%s_eta"    , m_BranchName.c_str()); l_OutTree->Branch(name, &m_eta    );
    sprintf(name, "%s_phi"    , m_BranchName.c_str()); l_OutTree->Branch(name, &m_phi    );
    sprintf(name, "%s_t"      , m_BranchName.c_str()); l_OutTree->Branch(name, &m_t      );
    // sprintf(name, "%s_JCTight", m_BranchName.c_str()); l_OutTree->Branch(name, &m_JCTight);
    // sprintf(name, "%s_jvf"    , m_BranchName.c_str()); l_OutTree->Branch(name, &m_jvf    );
    // sprintf(name, "%s_jvt"    , m_BranchName.c_str()); l_OutTree->Branch(name, &m_jvt    );
    sprintf(name, "%s_newjvt" , m_BranchName.c_str()); l_OutTree->Branch(name, &m_newjvt );

    sprintf(name, "%s_EMpt"   , m_BranchName.c_str()); l_OutTree->Branch(name, &m_EMpt   );
    // sprintf(name, "%s_EMeta"  , m_BranchName.c_str()); l_OutTree->Branch(name, &m_EMeta  );
    // sprintf(name, "%s_EMphi"  , m_BranchName.c_str()); l_OutTree->Branch(name, &m_EMphi  );
}

void Jets::InitTrackLinks(TTree *l_OutTree, std::string l_TrackParticleContainerKey) {
    char name[600];
    sprintf(name, "%s_jetTrk_index" , m_BranchName.c_str()); l_OutTree->Branch(name, &m_jetTrk_index );
    sprintf(name, "%s_jetTRK_pT"    , m_BranchName.c_str()); l_OutTree->Branch(name, &m_jetTRK_pT    );
    sprintf(name, "%s_jetTRK_WgtPt" , m_BranchName.c_str()); l_OutTree->Branch(name, &m_jetTRK_WgtPt );
    sprintf(name, "%s_jetTRK_eta"   , m_BranchName.c_str()); l_OutTree->Branch(name, &m_jetTRK_eta   );
    sprintf(name, "%s_jetTRK_phi"   , m_BranchName.c_str()); l_OutTree->Branch(name, &m_jetTRK_phi   );
    sprintf(name, "%s_jetNPFO_pT"   , m_BranchName.c_str()); l_OutTree->Branch(name, &m_jetNPFO_pT   );
    sprintf(name, "%s_jetNPFO_eta"  , m_BranchName.c_str()); l_OutTree->Branch(name, &m_jetNPFO_eta  );
    sprintf(name, "%s_jetNPFO_phi"  , m_BranchName.c_str()); l_OutTree->Branch(name, &m_jetNPFO_phi  );
    // sprintf(name, "%s_layerEnergies", m_BranchName.c_str()); l_OutTree->Branch(name, &m_layerEnergies);
    m_TrackParticleContainerKey = l_TrackParticleContainerKey;
    std::cout << "l_TrackParticleContainerKey = " << l_TrackParticleContainerKey << std::endl;
}

StatusCode Jets::ProcessTrackLinks(const xAOD::Jet *jet) {
    static std::map<const xAOD::TrackParticle*, int> l_trk_index_temp;

    //first call in event is with jet=nullptr
    if (jet == nullptr) {
        m_jetTrk_index  .clear();
        m_jetTRK_pT     .clear();
        m_jetTRK_WgtPt  .clear();
        m_jetTRK_eta    .clear();
        m_jetTRK_phi    .clear();
        m_jetNPFO_pT    .clear();
        m_jetNPFO_eta   .clear();
        m_jetNPFO_phi   .clear();
        l_trk_index_temp.clear();

        //retreieve the track container
        const xAOD::TrackParticleContainer *l_TrackParticleContainer;
        if (evtStore()->retrieve(l_TrackParticleContainer, m_TrackParticleContainerKey).isFailure()) {
            //ATH_MSG_ERROR("Could not retrieve TrackParticleContainer with key "<<m_TrackParticleContainerKey.c_str());
            return StatusCode::FAILURE;
        }

        //index each track
        int trk_index = 0;
        for (auto track : *l_TrackParticleContainer) {
            l_trk_index_temp[track] = trk_index;
            trk_index++;
        }
    }
    //subsequent calls are with a jet sent in
    else {
        std::vector<int> indices_vector;

        const std::vector<ElementLink<DataVector<xAOD::IParticle>>> ptrackContainer =
            (jet->auxdata<std::vector<ElementLink<DataVector<xAOD::IParticle>>>>("constituentLinks"));

        for (auto& elink : ptrackContainer) {
            if (elink.isValid()) {
                const xAOD::TrackParticle *associated_track = dynamic_cast<const xAOD::TrackParticle*>(*elink);
                if (!associated_track) {
                    //ATH_MSG_ERROR();
                    std::cout << "ERROR in line " << __LINE__ << " in file " << __FILE__ << " :: nullptr track associated with jet of type " << m_ContainerKey << std::endl;
                    //return StatusCode::FAILURE;
                    continue;
                }
                indices_vector.push_back(l_trk_index_temp[associated_track]);
            }
        }
        m_jetTrk_index.push_back(indices_vector);
    }

    return StatusCode::SUCCESS;
}


StatusCode Jets::Process(ToolHandle<IJetCalibrationTool>& jetCalibToolEMTopo4, ToolHandle<IJetCalibrationTool>& jetCalibToolEMPFlow4,
                         ToolHandle<IJetSelector>& jetCleaningToolLoose, ToolHandle<IJetSelector>&,
                         ToolHandle<IJetUpdateJvt>& jvtTool, ToolHandle<CP::IWeightPFOTool>& weightPFOTool) {
    if (!IsEnabled()) return StatusCode::SUCCESS;
    std::unordered_map<const xAOD::TrackParticle*, int> trk_index_map;

    // m_E      .clear();
    m_pt     .clear();
    m_eta    .clear();
    m_phi    .clear();
    m_t      .clear();
    m_pt_orig.clear();
    // m_JCTight.clear();
    // m_jvf    .clear();
    // m_jvt    .clear();
    m_newjvt .clear();

    m_EMpt   .clear();
    // m_EMeta  .clear();
    // m_EMphi  .clear();

    m_jetTrk_index .clear();
    m_jetTRK_pT    .clear();
    m_jetTRK_WgtPt .clear();
    m_jetTRK_eta   .clear();
    m_jetTRK_phi   .clear();
    m_jetNPFO_pT   .clear();
    m_jetNPFO_eta  .clear();
    m_jetNPFO_phi  .clear();
    // m_layerEnergies.clear();
    trk_index_map  .clear();

    if (m_ContainerKey == "AntiKt4EMPFlowJets") {
        //retreieve the track container
        const xAOD::TrackParticleContainer *l_TrackParticleContainer;
        if (evtStore()->retrieve(l_TrackParticleContainer, m_TrackParticleContainerKey).isFailure()) {
            std::cout << "Could not retrieve TrackParticleContainer with key " << m_TrackParticleContainerKey << std::endl;
            return StatusCode::FAILURE;
        }

        //index each track
        int trk_index = 0;
        for (auto track : *l_TrackParticleContainer) {
            if (track->pt() < 400.) continue;
            if (std::abs(track->eta()) > 2.5) continue;
            trk_index_map[track] = trk_index;
            trk_index++;
        }
    }


    const xAOD::JetContainer* l_jet_container = nullptr;
    if (evtStore()->retrieve(l_jet_container, m_ContainerKey).isFailure()) {
        std::cout << "ERROR " << __LINE__ << " " << __FILE__ << "  Could not retrieve JetContainer with key " << m_ContainerKey << std::endl;
        return StatusCode::FAILURE;
    }

    // if (m_TrackParticleContainerKey != "") CHECK(ProcessTrackLinks(nullptr));

    xAOD::JetContainer::const_iterator jet_itr = l_jet_container->begin();
    xAOD::JetContainer::const_iterator jet_end = l_jet_container->end();
    for ( ; jet_itr != jet_end; ++jet_itr ) {// Next, we will loop over all of our jets and apply the calibration algorithm.
        xAOD::Jet *inputJet = new xAOD::Jet(); // Pointer to retrieve the un-calibrated jet
        xAOD::Jet *calibJet = NULL; // Pointer to store the calibrated jet
        inputJet->makePrivateStore(**jet_itr);

        if (m_ContainerKey == "AntiKt4EMTopoJets") {
            if (!jetCalibToolEMTopo4->calibratedCopy(*inputJet, calibJet)) {
                std::cout << "Error calibrating EMTopo jet" << std::endl;
                delete inputJet;
                continue;
            }
        }
        if (m_ContainerKey == "AntiKt4EMPFlowJets") {
            if (!jetCalibToolEMPFlow4->calibratedCopy(*inputJet, calibJet)) {
                std::cout << "Error calibrating EMPFlow jet" << std::endl;
                delete inputJet;
                continue;
            }
        }

        if (!jetCleaningToolLoose->keep(*calibJet)) {
            std::cout << "Bad jet from jet cleaning" << std::endl;
            delete calibJet;
            delete inputJet;
            continue;
        }

        // if (jetCleaningToolTight->keep(*calibJet)) {m_JCTight.push_back(1);}
        // else                                       {m_JCTight.push_back(0);}

        TLorentzVector fullCalibJet = calibJet->p4();
        // m_E  .push_back(fullCalibJet.E()  );
        m_pt .push_back(fullCalibJet.Pt() );
        m_eta.push_back(fullCalibJet.Eta());
        m_phi.push_back(fullCalibJet.Phi());
        m_pt_orig.push_back(inputJet->pt());

        const xAOD::JetFourMom_t jetLV = calibJet->jetP4("JetEMScaleMomentum"); // Retrieve the EM scale momentum
        m_EMpt.push_back(jetLV.pt());
        // m_EMeta.push_back(jetLV.eta());
        // m_EMphi.push_back(jetLV.phi());

        float timing;
        calibJet->getAttribute(xAOD::JetAttribute::Timing, timing);
        m_t.push_back(timing);

        // std::vector<float> jvf;
        // calibJet->getAttribute(xAOD::JetAttribute::JVF, jvf);
        // m_jvf.push_back(jvf[0]);

        // float jvt;
        // calibJet->getAttribute(xAOD::JetAttribute::Jvt, jvt);
        // m_jvt.push_back(jvt);

        float newjvt = jvtTool->updateJvt(*calibJet);
        m_newjvt.push_back(newjvt);

        if (m_ContainerKey == "AntiKt4EMPFlowJets") {
            std::vector<const xAOD::IParticle*> vec = (calibJet->getConstituents()).asIParticleVector();
            std::vector<int  > temp_index;
            std::vector<float> temp_cpt;
            std::vector<float> temp_cwgtpt;
            std::vector<float> temp_ceta;
            std::vector<float> temp_cphi;
            std::vector<float> temp_npt;
            std::vector<float> temp_neta;
            std::vector<float> temp_nphi;
            // std::vector<float> temp_layerEnergies(24, 0);
            for (unsigned int i = 0; i < vec.size(); i++) {
                const xAOD::PFO *obj = static_cast<const xAOD::PFO*>(vec[i]); // Retrieve each associated track particle in an iterative loop
                if (obj == NULL) {continue;}  // It will be a null pointer if the track particle cast fails. That's fine, and this cut allows for it.

                if (std::abs(obj->charge()) > 1E-3) {
                    std::vector<const xAOD::IParticle*> trackVec; // List to store associated tracks
                    if (obj->associatedParticles(xAOD::PFODetails::PFOParticleType::Track, trackVec)) { // Retrieve the associated tracks in the list
                        for (unsigned int i = 0; i < trackVec.size(); i++) { // Loop over the tracks
                            const xAOD::TrackParticle *track = (const xAOD::TrackParticle*)(trackVec[i]);
                            if (track == NULL) {continue;}
                            if (trk_index_map.find(track) != trk_index_map.end()) temp_index.push_back(trk_index_map[track]);
                        }
                    }
                    trackVec.clear();
                    temp_cpt .push_back(obj->pt () / 1000.);
                    temp_ceta.push_back(obj->eta());
                    temp_cphi.push_back(obj->phi());

                    // make use of weighting scheme for charged PFO
                    // https://gitlab.cern.ch/atlas/athena/-/tree/release/21.2.148.0/Reconstruction/Jet/JetRecTools
                    // https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Reconstruction/PFlow/PFlowUtils/Root/WeightPFOTool.cxx?v=21.2
                    float weight = 0.0;
                    weightPFOTool->fillWeight(*obj, weight);
                    TLorentzVector pfop4 = (*obj).p4();
                    pfop4 *= weight;
                    temp_cwgtpt.push_back(obj->pt() * weight / 1000.);
                }
                else {
                    temp_npt .push_back(obj->pt () / 1000.);
                    temp_neta.push_back(obj->eta());
                    temp_nphi.push_back(obj->phi());
                }

                // if (std::abs(obj->charge()) == 0) {
                //     float layerEnergies[24] = {0};
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_EMB1       , layerEnergies[ 0]); temp_layerEnergies[ 0] += layerEnergies[ 0];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_EMB2       , layerEnergies[ 1]); temp_layerEnergies[ 1] += layerEnergies[ 1];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_EMB3       , layerEnergies[ 2]); temp_layerEnergies[ 2] += layerEnergies[ 2];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_EME1       , layerEnergies[ 3]); temp_layerEnergies[ 3] += layerEnergies[ 3];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_EME2       , layerEnergies[ 4]); temp_layerEnergies[ 4] += layerEnergies[ 4];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_EME3       , layerEnergies[ 5]); temp_layerEnergies[ 5] += layerEnergies[ 5];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_FCAL0      , layerEnergies[ 6]); temp_layerEnergies[ 6] += layerEnergies[ 6];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_FCAL1      , layerEnergies[ 7]); temp_layerEnergies[ 7] += layerEnergies[ 7];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_FCAL2      , layerEnergies[ 8]); temp_layerEnergies[ 8] += layerEnergies[ 8];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_HEC0       , layerEnergies[ 9]); temp_layerEnergies[ 9] += layerEnergies[ 9];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_HEC1       , layerEnergies[10]); temp_layerEnergies[10] += layerEnergies[10];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_HEC2       , layerEnergies[11]); temp_layerEnergies[11] += layerEnergies[11];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_HEC3       , layerEnergies[12]); temp_layerEnergies[12] += layerEnergies[12];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_PreSamplerB, layerEnergies[13]); temp_layerEnergies[13] += layerEnergies[13];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_PreSamplerE, layerEnergies[14]); temp_layerEnergies[14] += layerEnergies[14];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_TileBar0   , layerEnergies[15]); temp_layerEnergies[15] += layerEnergies[15];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_TileBar1   , layerEnergies[16]); temp_layerEnergies[16] += layerEnergies[16];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_TileBar2   , layerEnergies[17]); temp_layerEnergies[17] += layerEnergies[17];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_TileExt0   , layerEnergies[18]); temp_layerEnergies[18] += layerEnergies[18];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_TileExt1   , layerEnergies[19]); temp_layerEnergies[19] += layerEnergies[19];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_TileExt2   , layerEnergies[20]); temp_layerEnergies[20] += layerEnergies[20];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_TileGap1   , layerEnergies[21]); temp_layerEnergies[21] += layerEnergies[21];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_TileGap2   , layerEnergies[22]); temp_layerEnergies[22] += layerEnergies[22];
                //     obj->attribute(xAOD::PFODetails::eflowRec_LAYERENERGY_TileGap3   , layerEnergies[23]); temp_layerEnergies[23] += layerEnergies[23];
                // }
            }
            m_jetTrk_index .push_back(temp_index );
            m_jetTRK_pT    .push_back(temp_cpt   );
            m_jetTRK_WgtPt .push_back(temp_cwgtpt);
            m_jetTRK_eta   .push_back(temp_ceta  );
            m_jetTRK_phi   .push_back(temp_cphi  );
            m_jetNPFO_pT   .push_back(temp_npt   );
            m_jetNPFO_eta  .push_back(temp_neta  );
            m_jetNPFO_phi  .push_back(temp_nphi  );
            // m_layerEnergies.push_back(temp_layerEnergies);
        }

        delete calibJet;
        delete inputJet;
    }


    return StatusCode::SUCCESS;
}

