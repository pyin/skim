#include "TrackingValidation/SkimAlg.h"
#include "TrackingValidation/SkimAlgFromDerivation.h"
#include "GaudiKernel/DeclareFactoryEntries.h"

DECLARE_ALGORITHM_FACTORY( SkimAlg )
DECLARE_ALGORITHM_FACTORY( SkimAlgFromDerivation )

DECLARE_FACTORY_ENTRIES( TrackingValidation ) {
    DECLARE_ALGORITHM( SkimAlg )
    DECLARE_ALGORITHM( SkimAlgFromDerivation )
}

