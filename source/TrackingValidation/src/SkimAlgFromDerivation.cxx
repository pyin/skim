#include "TrackingValidation/SkimAlgFromDerivation.h"

#include <GaudiKernel/ITHistSvc.h>
#include <GaudiKernel/ServiceHandle.h>

#include "xAODHIEvent/HIEventShapeContainer.h"
#include "xAODTrigger/TrigDecision.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertexContainer.h"

#include <TTree.h>
#include <cmath>


SkimAlgFromDerivation::SkimAlgFromDerivation(const std::string& name, ISvcLocator* pSvcLocator)
  : AthAlgorithm(name,pSvcLocator)
{
  declareProperty("GRLTool"               ,  m_grlTool                                    );
  declareProperty("UseGRL"                ,  m_use_GRL                     =true          );//true/false
  declareProperty("MaxZvtx"               ,  m_MaxZvtx                     =250           );

  declareProperty("StoreAllEvents"        ,  m_StoreAllEvents              =false         );//true,false
  declareProperty("UseTrigger"            ,  m_use_trigger                 =true          );//true,false
  declareProperty("TriggerChains"         ,  m_Trigger_Chains              ="HLT_noalg_mb_L1TE50|HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50");

  declareProperty("EventInfoKey"            ,  m_EventInfo_key               ="EventInfo"          );
  declareProperty("TrkContainerKey"         ,  m_trk_container_key           ="InDetTrackParticles");
  declareProperty("VtxContainerKey"         ,  m_vtx_container_key           ="PrimaryVertices"    );
  declareProperty("TruthVtxContainerKey"    ,  m_truth_vtx_container_key     ="TruthVertices"      );
  declareProperty("TruthContainerKey"       ,  m_truth_container_key         ="TruthParticles"     );
  declareProperty("HIEventShapeContainerKey",  m_HIEventShapeContainer_key   ="CaloSums"           );

  declareProperty("StoreEventInfo"        ,  m_store_EventInfo             =true          );//true/false
  declareProperty("StoreTracks"           ,  m_store_tracks                =true          );//true/false
  declareProperty("StoreVtx"              ,  m_store_Vtx                   =true          );//true/false
  declareProperty("StoreET"               ,  m_store_ET                    =true          );//true/false
  declareProperty("StoreTruthVtx"         ,  m_store_truth_Vtx             =false         );//true/false
  declareProperty("StoreTruth"            ,  m_store_truth                 =false         );//true/false
  declareProperty("TruthMinPT"            ,  m_min_pT_Truth                =500           );//Min Truth Partilce pT in MeV
}



StatusCode SkimAlgFromDerivation::initialize(){
   ATH_MSG_INFO("Starting initialize()");

//retreive the tools //THIS IS OPTIONAL!
//-------------------------------------------------------------------------
   CHECK( m_grlTool .retrieve() );
   CHECK( m_trigTool.retrieve() );
//-------------------------------------------------------------------------


//Create and Book OutPutTree
//Trigger branches are added later in first loop of execute()
//-------------------------------------------------------------------------
   ServiceHandle<ITHistSvc> histSvc("THistSvc",name()); 
   CHECK( histSvc.retrieve() );

   m_OutTree=new TTree("HeavyIonD3PD","HeavyIonD3PD");
   CHECK(histSvc->regTree("/MYSTREAM/HeavyIonD3PD",m_OutTree));

   if(m_store_EventInfo   ) InitEventInfo   (m_OutTree);
   if(m_store_tracks      ) InitTracks      (m_OutTree);
   if(m_store_Vtx         ) InitVertex      (m_OutTree);
   if(m_store_ET          ) InitET          (m_OutTree); 
   if(m_store_truth_Vtx   ) InitTruthVertex (m_OutTree);
   if(m_store_truth       ) InitTruth       (m_OutTree);
//-------------------------------------------------------------------------




   return StatusCode::SUCCESS;
}







StatusCode SkimAlgFromDerivation::execute(){
   ATH_MSG_DEBUG("Starting execute()");

   if(m_use_trigger){
     static bool b_AddTriggerBranches=false;
     if(b_AddTriggerBranches==false) AddTriggerBranches();
     b_AddTriggerBranches=true;
   }



   bool pass_cleaning_cuts=true;
   CHECK(CleaningCuts(pass_cleaning_cuts));
   if(!pass_cleaning_cuts) return StatusCode::SUCCESS;


   if(m_use_trigger){
     bool pass_trigger_cuts =false;
     CHECK(ProcessTriggers(pass_trigger_cuts));
     //keep event even if it didnot pass any triggers if m_StoreAllEvents==true
     if(m_StoreAllEvents==false && pass_trigger_cuts==false) return StatusCode::SUCCESS;
   }


   if(m_store_EventInfo)    CHECK(ProcessEventInfo   ());
   if(m_store_tracks   )    CHECK(ProcessTracks      ());
   if(m_store_Vtx      )    CHECK(ProcessVertex      ());
   if(m_store_ET       )    CHECK(ProcessET          ()); 
   if(m_store_truth_Vtx)    CHECK(ProcessTruthVertex ());
   if(m_store_truth    )    CHECK(ProcessTruth       ());

   m_OutTree->Fill();
   ATH_MSG_DEBUG("Filling RunNumber="<<RunNumber);
   return StatusCode::SUCCESS;
}





StatusCode SkimAlgFromDerivation::finalize(){
  return StatusCode::SUCCESS;
}





StatusCode SkimAlgFromDerivation::CleaningCuts(bool &pass_cleaning_cuts){
   ATH_MSG_DEBUG("Starting CleaningCuts()");
   pass_cleaning_cuts=true;
   static int event_num=-1;
   event_num++;

//Event Info
//------------------------------------------------------  
//Must Pass good lumi-block
   const xAOD::EventInfo* l_EventInfo = nullptr;
   CHECK(evtStore()->retrieve(l_EventInfo,m_EventInfo_key));

   bool isMC=false;
   if(l_EventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
     isMC = true; // can do something with this later
   }  

   if(m_use_GRL && !isMC){
     if(!m_grlTool->passRunLB(*l_EventInfo)) {
       pass_cleaning_cuts=false;
       ATH_MSG_DEBUG("Event FAILED GRL");
       return StatusCode::SUCCESS;
     }
     else ATH_MSG_DEBUG("Event PASSED GRL");
   }
   else {ATH_MSG_DEBUG("Not using GRL");}
//------------------------------------------------------  



//------------------------------------------------------  
//Event by Event Cleaning
    if(!isMC){
      if((l_EventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error ) || 
         (l_EventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error ) || 
         (l_EventInfo->errorState(xAOD::EventInfo::SCT)==xAOD::EventInfo::Error ) || 
         (l_EventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) ) ){
          pass_cleaning_cuts=false;
          ATH_MSG_DEBUG("Event FAILED EventLevelCleaning");
          return StatusCode::SUCCESS;
      }
      else ATH_MSG_DEBUG("Event PASSED EventLevelCleaning");
    }
//------------------------------------------------------  



//Vertex
//------------------------------------------------------  
//Must have reconstructed vertex with |Z|<m_MaxZvtx
   if(m_MaxZvtx>0){
     const xAOD::VertexContainer *l_VertexContainer = nullptr;
     if(evtStore()->retrieve(l_VertexContainer,m_vtx_container_key).isFailure()){
        ATH_MSG_ERROR("Could not retrieve VxContainer with key "<<m_vtx_container_key.c_str());
        return StatusCode::FAILURE;
     }
     ATH_MSG_DEBUG("Num_Vertices="<<l_VertexContainer->size());
     if(l_VertexContainer->size()<=1) {pass_cleaning_cuts=false;}

     int count=0;
     for(const auto* vtx : *l_VertexContainer){
        float z_vtx   =vtx->z();

        ATH_MSG_DEBUG("     z["<<count<<"]="<<z_vtx);
        if(count==0 && fabs(z_vtx)>m_MaxZvtx) {pass_cleaning_cuts=false;}
        count++;
     }
   }
//------------------------------------------------------  
   
   return StatusCode::SUCCESS;
}




//Triggers
void SkimAlgFromDerivation::AddTriggerBranches(){
   //HLT Triggers and chaingroup ID; 
   //The ID are not needed to be corect currently as the triggers are checked using the "key" part
   std::map<std::string,short> trigger_list ;//chain triggers for the triggers(trigger name is the "key")

   std::vector<std::string> ListOfTriggers;
   ListOfTriggers=m_trigTool->getChainGroup(m_Trigger_Chains)->getListOfTriggers();

   for(auto &trig : ListOfTriggers) {
     ATH_MSG_INFO("Found Trigger : "<<trig.c_str());
     trigger_list[trig]=0;
   }


   int itrig=0;
   for(auto trigger: trigger_list){
      std::string BranchName="b_";BranchName+=trigger.first;
      std::string BranchType=BranchName+"/O";

      m_OutTree->Branch(BranchName.c_str(), &m_trigger_Flag[itrig],BranchType.c_str());

      m_trigger_chain[trigger.first]=&m_trigger_Flag[itrig];
      itrig++;
   }
}

StatusCode SkimAlgFromDerivation::ProcessTriggers(bool &pass_trigger_cuts){
   for(const auto& trig_chain:m_trigger_chain){
     std::string trigger_name=trig_chain.first ; //The name of the trigger 
     bool       *trigger_flag=trig_chain.second; //The bool that gets writted to the outputTree
     *trigger_flag=false;

     if  (m_trigTool->getListOfTriggers(trigger_name).empty()) {ATH_MSG_WARNING("Trigger "<<trigger_name.c_str()<<" Is Not Configured");}
     else                                                      {ATH_MSG_DEBUG("Trigger "<<trigger_name.c_str()<<" Is Configured");}

     if(m_trigTool->isPassed(trig_chain.first)){
        ATH_MSG_DEBUG("   Passed Trigger "<<trigger_name.c_str());
        *trigger_flag=true;
        pass_trigger_cuts =true;
     }
     else{
        ATH_MSG_DEBUG("   Failed Trigger "<<trigger_name.c_str());
        *trigger_flag=false;
     }
   }
   return StatusCode::SUCCESS;
}






//EventInfo
void SkimAlgFromDerivation::InitEventInfo(TTree *l_OutTree){
   l_OutTree->Branch("RunNumber"  , &RunNumber        ,"RunNumber/i");
   l_OutTree->Branch("lbn"        , &lumi_block       ,"lbn/i");
   l_OutTree->Branch("bcid"       , &bunch_crossing_id,"bcid/i");
   l_OutTree->Branch("eventNumber", &eventNumber      ,"eventNumber/l");
}

StatusCode SkimAlgFromDerivation::ProcessEventInfo(){
   const xAOD::EventInfo* l_EventInfo = nullptr;
   CHECK(evtStore()->retrieve(l_EventInfo,m_EventInfo_key));

   //Set Variables written to output Tree
   RunNumber        =l_EventInfo->runNumber();
   lumi_block       =l_EventInfo->lumiBlock();
   bunch_crossing_id=l_EventInfo->bcid();
   eventNumber      =l_EventInfo->eventNumber();

   return StatusCode::SUCCESS;
}


//Vertex
void SkimAlgFromDerivation::InitVertex(TTree *l_OutTree){
     l_OutTree->Branch("vtx_z"     ,&m_vtx_z     );
     l_OutTree->Branch("vtx_x"     ,&m_vtx_x     );
     l_OutTree->Branch("vtx_y"     ,&m_vtx_y     );
     l_OutTree->Branch("vtx_ntrk"  ,&m_vtx_ntrk  );
}

StatusCode SkimAlgFromDerivation::ProcessVertex(){
   const xAOD::VertexContainer *l_VertexContainer = nullptr;
   CHECK(evtStore()->retrieve(l_VertexContainer,m_vtx_container_key));

   m_vtx_z     .clear();
   m_vtx_x     .clear();
   m_vtx_y     .clear();
   m_vtx_ntrk  .clear();
   for(const auto* vtx : *l_VertexContainer){
      m_vtx_z     .push_back(vtx->z());
      m_vtx_x     .push_back(vtx->x());
      m_vtx_y     .push_back(vtx->y());
      m_vtx_ntrk  .push_back(vtx->nTrackParticles());
    }
   return StatusCode::SUCCESS;
}




void SkimAlgFromDerivation::InitTruthVertex(TTree *l_OutTree){
     l_OutTree->Branch("truth_vtx_z"     ,&m_truth_vtx_z     );
     l_OutTree->Branch("truth_vtx_x"     ,&m_truth_vtx_x     );
     l_OutTree->Branch("truth_vtx_y"     ,&m_truth_vtx_y     );
     l_OutTree->Branch("truth_vtx_t"     ,&m_truth_vtx_t     );
}

StatusCode SkimAlgFromDerivation::ProcessTruthVertex(){
   const xAOD::TruthVertexContainer *l_TruthVertexContainer = nullptr;
   CHECK(evtStore()->retrieve(l_TruthVertexContainer,m_truth_vtx_container_key));
   m_truth_vtx_z     .clear();
   m_truth_vtx_x     .clear();
   m_truth_vtx_y     .clear();
   m_truth_vtx_t     .clear();
   for(const auto* vtx : *l_TruthVertexContainer){
      m_truth_vtx_z     .push_back(vtx->z());
      m_truth_vtx_x     .push_back(vtx->x());
      m_truth_vtx_y     .push_back(vtx->y());
      m_truth_vtx_t     .push_back(vtx->t());
   }
   return StatusCode::SUCCESS;
}


//Tracks
void SkimAlgFromDerivation::InitTracks(TTree *l_OutTree){
     l_OutTree->Branch("trk_numqual",&m_trk_numqual);
     l_OutTree->Branch("trk_pt"     ,&m_track_pt);
     l_OutTree->Branch("trk_eta"    ,&m_track_eta);
     l_OutTree->Branch("trk_phi"    ,&m_track_phi);
     l_OutTree->Branch("trk_charge" ,&m_track_charge );
     l_OutTree->Branch("trk_qual"   ,&m_track_quality);
}

StatusCode SkimAlgFromDerivation::ProcessTracks(){
   const xAOD::TrackParticleContainer *l_TrackParticleContainer = nullptr;
   CHECK(evtStore()->retrieve(l_TrackParticleContainer,m_trk_container_key));
   ATH_MSG_DEBUG("Num Tracks="<<l_TrackParticleContainer->size());

   m_trk_numqual  .clear();m_trk_numqual.assign(3,0);
   m_track_pt     .clear();
   m_track_eta    .clear();
   m_track_phi    .clear();
   m_track_charge .clear();
   m_track_quality.clear();
 
   if(m_store_tracks){
     for(const auto* track : *l_TrackParticleContainer){
       m_track_pt     .push_back(track->pt    ());
       m_track_eta    .push_back(track->eta   ());
       m_track_phi    .push_back(track->phi   ());
       m_track_charge .push_back(track->charge());
       m_track_quality.push_back(track->auxdata<unsigned short>("TrackQuality"));
     }
   }

   return StatusCode::SUCCESS;
}


void SkimAlgFromDerivation::InitTruth(TTree *l_OutTree){
     l_OutTree->Branch("truth_pt"     ,&m_truth_pt);
     l_OutTree->Branch("truth_eta"    ,&m_truth_eta);
     l_OutTree->Branch("truth_phi"    ,&m_truth_phi);
     l_OutTree->Branch("truth_charge" ,&m_truth_charge );
     l_OutTree->Branch("truth_id"     ,&m_truth_id     );
     l_OutTree->Branch("truth_barcode",&m_truth_barcode);
     l_OutTree->Branch("truth_qual"   ,&m_truth_quality);
}


StatusCode SkimAlgFromDerivation::ProcessTruth(){
   m_truth_pt     .clear();
   m_truth_eta    .clear();
   m_truth_phi    .clear();
   m_truth_charge .clear();
   m_truth_id     .clear();
   m_truth_barcode.clear();
   m_truth_quality.clear();

   const xAOD::TruthParticleContainer *l_TruthParticleContainer;
   CHECK(evtStore()->retrieve(l_TruthParticleContainer,m_truth_container_key));

   for(auto track_itr=l_TruthParticleContainer->begin();track_itr!=l_TruthParticleContainer->end();track_itr++){
     auto track=(*track_itr);

     if(track->status()!=1) continue;
     if(track->pt()<0.0001 || track->pt()<m_min_pT_Truth) continue;
     if(track->barcode()>=200000 || track->barcode()==0) continue;
     if(fabs(track->charge())<0.1 || fabs(track->eta())>2.5 ) continue;

     float pt    =track->pt    ();
     float eta   =0.0;
     if(fabs(pt)>0.00001) eta   =track->eta   ();
     float phi   =track->phi   ();
     float charge=track->charge();
     int   id    =track->pdgId ();
     int quality=1;
     if(track->isStrangeBaryon() ||track->barcode()<=0) quality=-1;

     m_truth_pt      .push_back(pt);
     m_truth_eta     .push_back(eta);
     m_truth_phi     .push_back(phi);
     m_truth_charge  .push_back(charge);
     m_truth_id      .push_back(id);
     m_truth_barcode .push_back(track->barcode());
     m_truth_quality .push_back(quality);
   }

   return StatusCode::SUCCESS;
}


void SkimAlgFromDerivation::InitET(TTree *l_OutTree){
     l_OutTree->Branch("FCal_Et",&m_FCalET);
}

StatusCode SkimAlgFromDerivation::ProcessET(){
   const xAOD::HIEventShapeContainer* l_HIevtShapeContainer;
   CHECK(evtStore()->retrieve(l_HIevtShapeContainer,m_HIEventShapeContainer_key));

   double FCal_Et[3]={0.0};
   m_FCalET.clear();
   m_FCalET.assign(3,0);

   for(const auto* sh:*l_HIevtShapeContainer){
     float eta=(sh->etaMin()+sh->etaMax())*0.5;
     int layer=sh->layer();
     if(layer==21 || layer==22 || layer==23){
                 FCal_Et[2]+=sh->et();
       if(eta>0) FCal_Et[0]+=sh->et();
       if(eta<0) FCal_Et[1]+=sh->et();
     }
   }
   m_FCalET[0]=FCal_Et[0];
   m_FCalET[1]=FCal_Et[1];
   m_FCalET[2]=FCal_Et[2];
   return StatusCode::SUCCESS;
}
