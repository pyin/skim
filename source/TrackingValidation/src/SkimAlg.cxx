#include "TrackingValidation/SkimAlg.h"
#include "TrackingValidation/MyUtils.h"
#include "TrackingValidation/Module_EventShape.h"
#include "TrackingValidation/Module_Jets.h"

#include <GaudiKernel/ITHistSvc.h>
#include <GaudiKernel/ServiceHandle.h>

#include "xAODHIEvent/HIEventShapeContainer.h"
#include "xAODTrigger/TrigDecision.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertexContainer.h"
#include "xAODTrigger/EnergySumRoI.h"

#include <TTree.h>
#include <cmath>


SkimAlg::SkimAlg(const std::string& name, ISvcLocator* pSvcLocator)
  : AthAlgorithm(name,pSvcLocator)
{
  declareProperty("GRLTool"               ,  m_grlTool                                    );
  declareProperty("UseGRL"                ,  m_use_GRL                     =true          );//true/false
  declareProperty("MaxZvtx"               ,  m_MaxZvtx                     =250           );

  declareProperty("StoreAllEvents"        ,  m_StoreAllEvents              =false         );//true,false
  declareProperty("UseTrigger"            ,  m_use_trigger                 =true          );//true,false
  declareProperty("TriggerChains"         ,  m_Trigger_Chains              ="HLT_.*"      );

  declareProperty("EventInfoKey"          ,  m_EventInfo_key               ="EventInfo"          );
  declareProperty("TrkContainerKey"       ,  m_trk_container_key           ="InDetTrackParticles");
  declareProperty("VtxContainerKey"       ,  m_vtx_container_key           ="PrimaryVertices"    );
  declareProperty("METContainerKey"       ,  m_met_container_key           ="MET_Calo"           );
  declareProperty("TruthVtxContainerKey"  ,  m_truth_vtx_container_key     ="TruthVertices"      );
  declareProperty("TruthContainerKey"       ,  m_truth_container_key       ="TruthParticles"     );
  declareProperty("HIEventShapeContainerKey",  m_HIEventShapeContainer_key ="HIEventShape"       );
  declareProperty("HIEventShapeTriggerContainerKey",  
                 m_HIEventShapeTriggerContainer_key       
             ="HLT_xAOD__HIEventShapeContainer_HIUE");


  declareProperty("StoreEventInfo"        ,  m_store_EventInfo             =true          );//true/false
  declareProperty("StoreTracks"           ,  m_store_tracks                =false         );//true/false
  declareProperty("StoreTrackDetails"     ,  m_store_track_details         =1             );//true/false
  declareProperty("StoreVtx"              ,  m_store_Vtx                   =false         );//true/false
  declareProperty("StoreET"               ,  m_store_ET                    =true          );//true/false
  declareProperty("StoreTruthVtx"         ,  m_store_truth_Vtx             =false         );//true/false
  declareProperty("StoreTruth"            ,  m_store_truth                 =true          );//true/false
  declareProperty("TruthMinPT"            ,  m_min_pT_Truth                =500           );//Min Truth Partilce pT in MeV

  declareProperty("TrackSelectionTool"    ,  m_trkSelTool                                 );
}



StatusCode SkimAlg::initialize(){
   ATH_MSG_INFO("Starting initialize()");

//retreive the tools //THIS IS OPTIONAL!
//-------------------------------------------------------------------------
   if(m_use_GRL    ) CHECK( m_grlTool   .retrieve() );
   if(m_use_trigger) CHECK( m_trigTool  .retrieve() );
                     CHECK( m_trkSelTool.retrieve() );
//-------------------------------------------------------------------------


//Create and Book OutPutTree
//Trigger branches are added later in first loop of execute()
//-------------------------------------------------------------------------
   ServiceHandle<ITHistSvc> histSvc("THistSvc",name()); 
   CHECK( histSvc.retrieve() );

   m_OutTree=new TTree("HeavyIonD3PD","HeavyIonD3PD");
   CHECK(histSvc->regTree("/MYSTREAM/HeavyIonD3PD",m_OutTree));

   if(m_store_EventInfo   ) InitEventInfo(m_OutTree);
   if(m_store_tracks      ) InitTracks   (m_OutTree);
   if(m_store_Vtx         ) InitVertex   (m_OutTree);
   if(m_store_ET          ) InitMET      (m_OutTree); 
   if(m_store_truth_Vtx   ) InitTruthVertex(m_OutTree);
   if(m_store_truth       ) InitTruth       (m_OutTree);


   Module* evtshape_module;
   if(m_HIEventShapeTriggerContainer_key != ""){
     evtshape_module=new EventShape(evtStore(),m_HIEventShapeTriggerContainer_key,"trig_");
     evtshape_module->Init(m_OutTree); 
     m_modules.push_back(evtshape_module);
   }

   if(m_HIEventShapeContainer_key != ""){
     evtshape_module=new EventShape(evtStore(),m_HIEventShapeContainer_key);
     evtshape_module->Init(m_OutTree); 
     m_modules.push_back(evtshape_module);
   }
//-------------------------------------------------------------------------




   return StatusCode::SUCCESS;
}







StatusCode SkimAlg::execute(){
   ATH_MSG_DEBUG("Starting execute()");

   if(m_use_trigger){
     static bool b_AddTriggerBranches=false;
     if(b_AddTriggerBranches==false) AddTriggerBranches();
     b_AddTriggerBranches=true;
   }



   bool pass_cleaning_cuts=true;
   CHECK(CleaningCuts(pass_cleaning_cuts));
   if(!pass_cleaning_cuts) return StatusCode::SUCCESS;


   if(m_use_trigger){
     bool pass_trigger_cuts =false;
     CHECK(ProcessTriggers(pass_trigger_cuts));
     //keep event even if it didnot pass any triggers if m_StoreAllEvents==true
     if(m_StoreAllEvents==false && pass_trigger_cuts==false) return StatusCode::SUCCESS;
   }


   if(m_store_EventInfo)    CHECK(ProcessEventInfo   ());
   if(m_store_tracks   )    CHECK(ProcessTracks      ());
   if(m_store_Vtx      )    CHECK(ProcessVertex      ());
   if(m_store_ET       )    CHECK(ProcessMET         ());
   if(m_store_truth_Vtx)    CHECK(ProcessTruthVertex ());
   if(m_store_truth    )    CHECK(ProcessTruth       ());

   for(auto module:m_modules) CHECK(module->Process()); 

   m_OutTree->Fill();
   ATH_MSG_DEBUG("Filling RunNumber="<<RunNumber);
   return StatusCode::SUCCESS;
}





StatusCode SkimAlg::finalize(){
  return StatusCode::SUCCESS;
}





StatusCode SkimAlg::CleaningCuts(bool &pass_cleaning_cuts){
   ATH_MSG_DEBUG("Starting CleaningCuts()");
   pass_cleaning_cuts=true;
   static int event_num=-1;
   event_num++;

//Event Info
//------------------------------------------------------  
//Must Pass good lumi-block
   const xAOD::EventInfo* l_EventInfo = nullptr;
   if(evtStore()->retrieve(l_EventInfo,m_EventInfo_key).isFailure()){
      ATH_MSG_ERROR(" Could not retrieve EventInfo with key "<<m_EventInfo_key.c_str());
      return StatusCode::FAILURE;
   }

   bool isMC=false;
   if(l_EventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
     isMC = true; // can do something with this later
   }  

   if(m_use_GRL && !isMC){
     if(!m_grlTool->passRunLB(*l_EventInfo)) {
       pass_cleaning_cuts=false;
       ATH_MSG_DEBUG("Event FAILED GRL");
       return StatusCode::SUCCESS;
     }
     else ATH_MSG_DEBUG("Event PASSED GRL");
   }
   else {ATH_MSG_DEBUG("Not using GRL");}
//------------------------------------------------------  



//------------------------------------------------------  
//Event by Event Cleaning
    if(!isMC){
      if((l_EventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error ) || 
         (l_EventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error ) || 
         (l_EventInfo->errorState(xAOD::EventInfo::SCT)==xAOD::EventInfo::Error ) || 
         (l_EventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) ) ){
          pass_cleaning_cuts=false;
          ATH_MSG_DEBUG("Event FAILED EventLevelCleaning");
          return StatusCode::SUCCESS;
      }
      else ATH_MSG_DEBUG("Event PASSED EventLevelCleaning");
    }
//------------------------------------------------------  



//Vertex
//------------------------------------------------------  
//Must have reconstructed vertex with |Z|<m_MaxZvtx
   if(m_MaxZvtx>0){
     const xAOD::VertexContainer *l_VertexContainer = nullptr;
     if(evtStore()->retrieve(l_VertexContainer,m_vtx_container_key).isFailure()){
        ATH_MSG_ERROR("Could not retrieve VxContainer with key "<<m_vtx_container_key.c_str());
        return StatusCode::FAILURE;
     }
     ATH_MSG_DEBUG("Num_Vertices="<<l_VertexContainer->size());
     if(l_VertexContainer->size()<=1) {pass_cleaning_cuts=false;}

     int count=0;
     for(const auto* vtx : *l_VertexContainer){
        float z_vtx   =vtx->z();

        ATH_MSG_DEBUG("     z["<<count<<"]="<<z_vtx);
        if(count==0 && fabs(z_vtx)>m_MaxZvtx) {pass_cleaning_cuts=false;}
        count++;
     }
   }
//------------------------------------------------------  
   
   return StatusCode::SUCCESS;
}




//Triggers
void SkimAlg::AddTriggerBranches(){
   //HLT Triggers and chaingroup ID; 
   //The ID are not needed to be corect currently as the triggers are checked using the "key" part
   std::map<std::string,short> trigger_list ;//chain triggers for the triggers(trigger name is the "key")

   std::vector<std::string> ListOfTriggers;
   ListOfTriggers=m_trigTool->getChainGroup(m_Trigger_Chains)->getListOfTriggers();

   std::cout<<"Dumping Trigger Names"<<std::endl;
   for(auto &trig : ListOfTriggers) {
     ATH_MSG_INFO("Found Trigger : "<<trig.c_str());
     trigger_list[trig]=0;
   }
   std::cout<<"Dumping Trigger Names Done"<<std::endl;


   int itrig=0;
   for(auto trigger: trigger_list){
      std::string BranchName="b_";BranchName+=trigger.first;
      std::string BranchType=BranchName+"/O";

      m_OutTree->Branch(BranchName.c_str(), &m_trigger_Flag[itrig],BranchType.c_str());

      m_trigger_chain[trigger.first]=&m_trigger_Flag[itrig];
      itrig++;
   }
}

StatusCode SkimAlg::ProcessTriggers(bool &pass_trigger_cuts){
   for(const auto& trig_chain:m_trigger_chain){
     std::string trigger_name=trig_chain.first ; //The name of the trigger 
     bool       *trigger_flag=trig_chain.second; //The bool that gets writted to the outputTree
     *trigger_flag=false;

     if  (m_trigTool->getListOfTriggers(trigger_name).empty()) {ATH_MSG_WARNING("Trigger "<<trigger_name.c_str()<<" Is Not Configured");}
     else                                                      {ATH_MSG_DEBUG("Trigger "<<trigger_name.c_str()<<" Is Configured");}

     if(m_trigTool->isPassed(trig_chain.first)){
        ATH_MSG_DEBUG("   Passed Trigger "<<trigger_name.c_str());
        *trigger_flag=true;
        pass_trigger_cuts =true;
     }
     else{
        ATH_MSG_DEBUG("   Failed Trigger "<<trigger_name.c_str());
        *trigger_flag=false;
     }
   }
   return StatusCode::SUCCESS;
}






//EventInfo
void SkimAlg::InitEventInfo(TTree *l_OutTree){
   l_OutTree->Branch("RunNumber"  , &RunNumber        ,"RunNumber/i");
   l_OutTree->Branch("lbn"        , &lumi_block       ,"lbn/i");
   l_OutTree->Branch("bcid"       , &bunch_crossing_id,"bcid/i");
   l_OutTree->Branch("eventNumber", &eventNumber      ,"eventNumber/l");
   l_OutTree->Branch("L1TE"       , &m_L1TE           ,"L1TE/F");
}

StatusCode SkimAlg::ProcessEventInfo(){
   const xAOD::EventInfo* l_EventInfo = nullptr;
   if(evtStore()->retrieve(l_EventInfo,m_EventInfo_key).isFailure()){
      ATH_MSG_ERROR(" Could not retrieve EventInfo with key "<<m_EventInfo_key.c_str());
      return StatusCode::FAILURE;
   }

   //Set Variables written to output Tree
   RunNumber        =l_EventInfo->runNumber();
   lumi_block       =l_EventInfo->lumiBlock();
   bunch_crossing_id=l_EventInfo->bcid();
   eventNumber      =l_EventInfo->eventNumber();

   m_L1TE=-1;
	 /*
   if(!l_EventInfo->eventType( xAOD::EventInfo::IS_SIMULATION )){
     const xAOD::EnergySumRoI *ptrOnL1te = 0;
     if(evtStore()->retrieve(ptrOnL1te, "LVL1EnergySumRoI").isFailure()){
        ATH_MSG_ERROR(" Could not retrieve EnergySumRoI with key "<<"LVL1EnergySumRoI");
        return StatusCode::FAILURE;
     }
     m_L1TE = ptrOnL1te->energyT()/1000;
   }
	 */


   return StatusCode::SUCCESS;
}


//Vertex
void SkimAlg::InitVertex(TTree *l_OutTree){
     l_OutTree->Branch("vtx_z"     ,&m_vtx_z     );
     l_OutTree->Branch("vtx_x"     ,&m_vtx_x     );
     l_OutTree->Branch("vtx_y"     ,&m_vtx_y     );
     l_OutTree->Branch("vtx_ntrk"  ,&m_vtx_ntrk  );
}

StatusCode SkimAlg::ProcessVertex(){
   const xAOD::VertexContainer *l_VertexContainer = nullptr;
   if(evtStore()->retrieve(l_VertexContainer,m_vtx_container_key).isFailure()){
      ATH_MSG_ERROR("Could not retrieve VxContainer with key "<<m_vtx_container_key.c_str());
      return StatusCode::FAILURE;
   }

   m_vtx_z     .clear();
   m_vtx_x     .clear();
   m_vtx_y     .clear();
   m_vtx_ntrk  .clear();
   for(const auto* vtx : *l_VertexContainer){
      m_vtx_z     .push_back(vtx->z());
      m_vtx_x     .push_back(vtx->x());
      m_vtx_y     .push_back(vtx->y());
      m_vtx_ntrk  .push_back(vtx->nTrackParticles());
    }
   return StatusCode::SUCCESS;
}




void SkimAlg::InitTruthVertex(TTree *l_OutTree){
     l_OutTree->Branch("truth_vtx_z"     ,&m_truth_vtx_z     );
     l_OutTree->Branch("truth_vtx_x"     ,&m_truth_vtx_x     );
     l_OutTree->Branch("truth_vtx_y"     ,&m_truth_vtx_y     );
     l_OutTree->Branch("truth_vtx_t"     ,&m_truth_vtx_t     );
}

StatusCode SkimAlg::ProcessTruthVertex(){
   const xAOD::TruthVertexContainer *l_TruthVertexContainer = nullptr;
   if(evtStore()->retrieve(l_TruthVertexContainer,m_truth_vtx_container_key).isFailure()){
      ATH_MSG_ERROR("Could not retrieve TruthVxContainer with key "<<m_truth_vtx_container_key.c_str());
      return StatusCode::FAILURE;
   }
   m_truth_vtx_z     .clear();
   m_truth_vtx_x     .clear();
   m_truth_vtx_y     .clear();
   m_truth_vtx_t     .clear();
   for(const auto* vtx : *l_TruthVertexContainer){
      m_truth_vtx_z     .push_back(vtx->z());
      m_truth_vtx_x     .push_back(vtx->x());
      m_truth_vtx_y     .push_back(vtx->y());
      m_truth_vtx_t     .push_back(vtx->t());
   }
   return StatusCode::SUCCESS;
}


//Tracks
void SkimAlg::InitTracks(TTree *l_OutTree){
     l_OutTree->Branch("trk_numqual",&m_trk_numqual);
     if(m_store_track_details>=1){
       l_OutTree->Branch("trk_pt"     ,&m_track_pt);
       l_OutTree->Branch("trk_eta"    ,&m_track_eta);
       l_OutTree->Branch("trk_phi"    ,&m_track_phi);
       l_OutTree->Branch("trk_charge" ,&m_track_charge );
       l_OutTree->Branch("trk_qual"   ,&m_track_quality);
       if(m_store_track_details>1){
         l_OutTree->Branch("trk_z0_wrtPV"       ,&m_track_z0_wrtPV       );
         l_OutTree->Branch("trk_d0"             ,&m_track_d0             );
         l_OutTree->Branch("trk_vz"             ,&m_track_vz             );
         l_OutTree->Branch("trk_Ipix_hits"      ,&m_track_Ipix_hits      );
         l_OutTree->Branch("trk_Ipix_expected"  ,&m_track_Ipix_expected  );
         l_OutTree->Branch("trk_NIpix_hits"     ,&m_track_NIpix_hits     );
         l_OutTree->Branch("trk_NIpix_expected" ,&m_track_NIpix_expected );
         l_OutTree->Branch("trk_sct_hits"       ,&m_track_sct_hits       );
         l_OutTree->Branch("trk_pix_hits"       ,&m_track_pix_hits       );
         l_OutTree->Branch("trk_sct_holes"      ,&m_track_sct_holes      );
         l_OutTree->Branch("trk_pix_holes"      ,&m_track_pix_holes      );
         l_OutTree->Branch("trk_sct_dead"       ,&m_track_sct_dead       );
         l_OutTree->Branch("trk_pix_dead"       ,&m_track_pix_dead       );
         l_OutTree->Branch("trk_sct_shared"     ,&m_track_sct_shared     );
         l_OutTree->Branch("trk_pix_shared"     ,&m_track_pix_shared     );
         l_OutTree->Branch("trk_chi2"           ,&m_track_chi2           );
         l_OutTree->Branch("trk_ndof"           ,&m_track_ndof           );
         l_OutTree->Branch("trk_patternRecoInfo",&m_track_patternRecoInfo);
       }
       if(m_store_truth){
         l_OutTree->Branch("trk_truth_index"    ,&m_trk_truth_index    ); 
         l_OutTree->Branch("trk_truth_prob"     ,&m_trk_truth_prob     ); 
         l_OutTree->Branch("trk_truth_barcode"  ,&m_trk_truth_barcode  ); 
         l_OutTree->Branch("trk_truth_IsPrimary",&m_trk_truth_IsPrimary); 
       }
     }
}

StatusCode SkimAlg::ProcessTracks(){
   const xAOD::TrackParticleContainer *l_TrackParticleContainer = nullptr;
   if(evtStore()->retrieve(l_TrackParticleContainer,m_trk_container_key).isFailure()){
     ATH_MSG_ERROR("Could not retrieve TrackParticleContainer with key "<<m_trk_container_key.c_str());
     return StatusCode::FAILURE;
   }
   ATH_MSG_DEBUG("Num Tracks="<<l_TrackParticleContainer->size());

   const xAOD::VertexContainer *l_VertexContainer = nullptr;
   if(evtStore()->retrieve(l_VertexContainer,m_vtx_container_key).isFailure()){
      ATH_MSG_ERROR("Could not retrieve VxContainer with key "<<m_vtx_container_key.c_str());
      return StatusCode::FAILURE;
   }
   const auto* priVtx = *(l_VertexContainer->cbegin());
   if (priVtx->vertexType() != xAOD::VxType::PriVtx) {
     ATH_MSG_ERROR( "First vertex is not of type \"Primary Vertex\"." );
     return StatusCode::FAILURE;
   }
   float z_vtx = priVtx->z();

   m_trk_numqual  .clear();m_trk_numqual.assign(3,0);
   if(m_store_track_details>=1){
     m_track_pt     .clear();
     m_track_eta    .clear();
     m_track_phi    .clear();
     m_track_charge .clear();
     m_track_quality.clear();
     if(m_store_track_details>1){
       m_track_d0             .clear();
       m_track_z0_wrtPV       .clear();
       m_track_vz             .clear();
       m_track_Ipix_hits      .clear();
       m_track_Ipix_expected  .clear();
       m_track_NIpix_hits     .clear();
       m_track_NIpix_expected .clear();
       m_track_sct_hits       .clear();
       m_track_pix_hits       .clear();
       m_track_sct_holes      .clear();
       m_track_pix_holes      .clear();
       m_track_sct_dead       .clear();
       m_track_pix_dead       .clear();
       m_track_sct_shared     .clear();
       m_track_pix_shared     .clear();
       m_track_chi2           .clear();
       m_track_ndof           .clear();
       m_track_patternRecoInfo.clear();
     }
     if(m_store_truth){
       m_trk_truth_index_temp1.clear();
       m_trk_truth_index      .clear();
       m_trk_truth_prob       .clear();
       m_trk_truth_barcode    .clear();
       m_trk_truth_IsPrimary  .clear();
     }
   }
 
   int trk_index=0;
   for(const auto* track : *l_TrackParticleContainer){
     float pt    =track->pt    ();
     int quality=MyUtils::TrackQuality(track, z_vtx);
     m_trk_numqual[1]++;if(quality>=0 && pt>400) {m_trk_numqual[0]++;}
     if(m_store_tracks && m_store_track_details>=1){
       float eta   =track->eta   ();
       float phi   =track->phi   ();
       float charge=track->charge();

       m_track_pt     .push_back(pt);
       m_track_eta    .push_back(eta);
       m_track_phi    .push_back(phi);
       m_track_charge .push_back(charge);
       m_track_quality.push_back(quality);
       if(m_store_track_details>1){
         m_track_d0             .push_back(track->d0());
         m_track_z0_wrtPV       .push_back(track->z0()+track->vz() - z_vtx);
         m_track_vz             .push_back(track->vz());
         m_track_Ipix_hits      .push_back(track->auxdata<uint8_t>("numberOfInnermostPixelLayerHits"));
         m_track_Ipix_expected  .push_back(track->auxdata<uint8_t>("expectInnermostPixelLayerHit"));
         m_track_NIpix_hits     .push_back(track->auxdata<uint8_t>("numberOfNextToInnermostPixelLayerHits"));
         m_track_NIpix_expected .push_back(track->auxdata<uint8_t>("expectNextToInnermostPixelLayerHit"));
         m_track_sct_hits       .push_back(track->auxdata<uint8_t>("numberOfSCTHits"));
         m_track_pix_hits       .push_back(track->auxdata<uint8_t>("numberOfPixelHits"));
         m_track_sct_holes      .push_back(track->auxdata<uint8_t>("numberOfSCTHoles"));
         m_track_pix_holes      .push_back(track->auxdata<uint8_t>("numberOfPixelHoles"));
         m_track_sct_dead       .push_back(track->auxdata<uint8_t>("numberOfSCTDeadSensors"));
         m_track_pix_dead       .push_back(track->auxdata<uint8_t>("numberOfPixelDeadSensors"));
				 m_track_sct_shared     .push_back(track->auxdata<uint8_t>("numberOfSCTSharedHits"));
				 m_track_pix_shared     .push_back(track->auxdata<uint8_t>("numberOfPixelSharedHits"));
         m_track_chi2           .push_back(track->auxdata<float  >("chiSquared"));
         m_track_ndof           .push_back(track->auxdata<float  >("numberDoF"));
         m_track_patternRecoInfo.push_back(track->auxdata<unsigned long>("patternRecoInfo"));
       }
       //http://acode-browser2.usatlas.bnl.gov/lxr-rel20/source/atlas/InnerDetector/InDetValidation/InDetPhysValMonitoring/src/InDetPhysValMonitoringTool.cxx?v=release_20_3_0
       if(m_store_truth){
         const                 ElementLink<xAOD::TruthParticleContainer> ptruthContainer=
               (track->auxdata<ElementLink<xAOD::TruthParticleContainer>>("truthParticleLink" ));
         if(ptruthContainer.isValid()){
           const xAOD::TruthParticle *associated_truth=*ptruthContainer;
           m_trk_truth_index_temp1   [associated_truth]=trk_index; 
           m_trk_truth_index    .push_back(-1);
           m_trk_truth_prob     .push_back(track->auxdata<float>("truthMatchProbability")); 
           m_trk_truth_barcode  .push_back(associated_truth->barcode());
           m_trk_truth_IsPrimary.push_back(IsPrimaryParticle(associated_truth));
         }
         else{
           m_trk_truth_index    .push_back(-1);
           m_trk_truth_prob     .push_back(0); 
           m_trk_truth_barcode  .push_back(-1);
           m_trk_truth_IsPrimary.push_back(false);
         }
       }
     }
     trk_index++;
   }

   return StatusCode::SUCCESS;
}

bool SkimAlg::IsPrimaryParticle(const xAOD::TruthParticle* particle){
  if(!particle) return false;
  if(particle->status()!=1                             ) return false;
  if(particle->barcode()>=2e5 || particle->barcode()==0) return false;
  if(particle->charge()==0                             ) return false;
  return true;
}




void SkimAlg::InitMET(TTree *l_OutTree){
  l_OutTree->Branch("MET_sumet", &m_MET_sumet);
}

StatusCode SkimAlg::ProcessMET(){
   const xAOD::MissingETContainer *l_MissingETContainer = nullptr;
   if(evtStore()->retrieve(l_MissingETContainer,m_met_container_key).isFailure()){
     ATH_MSG_ERROR("Could not retrieve MissingETContainer with key "<<m_met_container_key.c_str());
     return StatusCode::FAILURE;
   }
   m_MET_sumet.clear();
   m_MET_sumet.assign(7,-1.0e9);
   for(const auto& MET: *l_MissingETContainer){
      std::string MET_Name =MET->name();
      float       MET_sumet=MET->sumet();
      
      if     (MET_Name=="EMB" ) m_MET_sumet[0]=MET_sumet;
      else if(MET_Name=="EME" ) m_MET_sumet[1]=MET_sumet;
      else if(MET_Name=="FCAL") m_MET_sumet[2]=MET_sumet;
      else if(MET_Name=="HEC" ) m_MET_sumet[3]=MET_sumet; 
      else if(MET_Name=="PEMB") m_MET_sumet[4]=MET_sumet;
      else if(MET_Name=="PEME") m_MET_sumet[5]=MET_sumet;
      else if(MET_Name=="TILE") m_MET_sumet[6]=MET_sumet;
      else { ATH_MSG_ERROR("Unknown MET index::"<<MET_Name.c_str());exit(0);}
   } 
   return StatusCode::SUCCESS;
}


void SkimAlg::InitTruth(TTree *l_OutTree){
     l_OutTree->Branch("truth_pt"     ,&m_truth_pt);
     l_OutTree->Branch("truth_eta"    ,&m_truth_eta);
     l_OutTree->Branch("truth_phi"    ,&m_truth_phi);
     l_OutTree->Branch("truth_charge" ,&m_truth_charge );
     l_OutTree->Branch("truth_id"     ,&m_truth_id     );
     l_OutTree->Branch("truth_barcode",&m_truth_barcode);
     l_OutTree->Branch("truth_qual"   ,&m_truth_quality);
     if(m_store_tracks) m_OutTree->Branch("truth_trk_index",&m_truth_trk_index);
}


StatusCode SkimAlg::ProcessTruth(){
   m_truth_pt     .clear();
   m_truth_eta    .clear();
   m_truth_phi    .clear();
   m_truth_charge .clear();
   m_truth_id     .clear();
   m_truth_barcode.clear();
   m_truth_quality.clear();
   m_truth_trk_index.clear();

   const xAOD::TruthParticleContainer *l_TruthParticleContainer;
   if(evtStore()->retrieve(l_TruthParticleContainer,m_truth_container_key).isFailure()){
     ATH_MSG_ERROR("Could not retrieve TruthParticleContainer with key"<<m_truth_container_key);
     return StatusCode::FAILURE;
   }

   int truth_index=0;
   for(auto track_itr=l_TruthParticleContainer->begin();track_itr!=l_TruthParticleContainer->end();track_itr++){
     auto track=(*track_itr);

     if(track->status()!=1) continue;
     if(track->pt()<0.0001 || track->pt()<m_min_pT_Truth) continue;
     if(track->barcode()>=200000 || track->barcode()==0) continue;
     if(fabs(track->charge())<0.1 || fabs(track->eta())>2.5 ) continue;

     float pt    =track->pt    ();
     float eta   =0.0;
     if(fabs(pt)>0.00001) eta   =track->eta   ();
     float phi   =track->phi   ();
     float charge=track->charge();
     int   id    =track->pdgId ();
     int quality=1;
     if(track->isStrangeBaryon() ||track->barcode()<=0) quality=-1;

     m_truth_pt      .push_back(pt);
     m_truth_eta     .push_back(eta);
     m_truth_phi     .push_back(phi);
     m_truth_charge  .push_back(charge);
     m_truth_id      .push_back(id);
     m_truth_barcode .push_back(track->barcode());
     m_truth_quality .push_back(quality);

     if(m_store_tracks){
       int trk_index=-1;
       if(m_trk_truth_index_temp1.find(track)!=m_trk_truth_index_temp1.end()){
         trk_index=m_trk_truth_index_temp1[track];
         if(m_trk_truth_barcode[trk_index]!=track->barcode()){
           ATH_MSG_ERROR("Barcodes Dont Match");
           return StatusCode::FAILURE;
         }
         m_trk_truth_index[trk_index]=truth_index;
       }
       m_truth_trk_index.push_back(trk_index);
     }
     truth_index++;
   }

   return StatusCode::SUCCESS;
}

