#All Settings (Default)
m_EvtMax=-1
m_DataSource         ='data'

m_UseGRL             =True                  #           1)Ignore GRL if false
GRL_XML              ="merged_grl.xml"      #           1)XML File for GRL
m_MaxZvtx            =300                   #           1)max |zvtx| for primary zvtx
m_StoreAllEvents     =False
m_UseTrigger         =True                  #           1)Make Trigger branches 2)use decision
m_TriggerTool        =True
m_TriggerChains      ="HLT_noalg_mb_L1TE50|HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50" 
#m_TriggerChains      ="HLT_noalg_mb_L1TE50|HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50|HLT_hi_v.*" 
#m_TriggerChains      ="HLT_noalg_mb_L1TE50|HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50|HLT_mb_sp|HLT_noalg_mb_L1ZDC_AND|HLT_mb_mbts_L1MBTS.*|HLT_noalg_mb_L1MBTS.*|HLT_hi_v.*|L1_ZDC_A_C|L1_ZDC_A|L1_ZDC_C|L1_ZDC_A_C_VTE50|L1_TE50|" 


m_EventInfoKey            ="EventInfo"
m_TrkContainerKey         ="InDetTrackParticles"
m_VtxContainerKey         ="PrimaryVertices"
m_HIEventShapeContainerKey="CaloSums"
m_TruthVtxContainerKey    ="TruthVertices"
m_TruthContainerKey       ="TruthParticles"


m_StoreEventInfo     =True                  #           1)Store Event Info
m_StoreTracks        =True                  #           1)Store Tracks
m_StoreVtx           =True                  #           1)Store Vertex info
m_StoreET            =True                  #           1)Store ET info
m_StoreTruthVtx      =False
m_StoreTruth         =False
m_TruthMinPT         =500


#m_EvtMax=10
m_InputFile="data15_hi.AOD.root"


##Changes for for Pb+Pb MC
m_EvtMax=-1
m_DataSource           ='geant4'
m_InputFile          ="derivation.pool.root"
m_UseGRL             =False
m_UseTrigger         =False
m_TriggerTool        =False
m_StoreTruthVtx      =True
m_StoreTruth         =True
















import AthenaPoolCnvSvc.ReadAthenaPool
import glob
svcMgr.EventSelector.InputCollections = glob.glob(m_InputFile)


theApp.EvtMax = m_EvtMax


from AthenaCommon.GlobalFlags import globalflags
globalflags.DataSource.set_Value_and_Lock(m_DataSource) #data geant4


svcMgr += CfgMgr.AthenaEventLoopMgr(EventPrintoutInterval=1)


if m_TriggerTool==True :
     #TriggerConfigGetter looks at recflags, so it might be a good idea to set those, but I have found not to be always necessary
     from RecExConfig.RecFlags  import rec
     rec.readRDO = False #readRDO is the True one by default. Turn it off before turning others on
     rec.readAOD = True #example for reading AOD

     #need this bit for AOD because AOD doesn't contain all trigger info - so has to connect to DB for some of it
     #ESD/RDO appear not to need this. Only do this bit if running on data, not MC
     # from AthenaCommon.GlobalFlags import globalflags
     # globalflags.DataSource = 'data'

     #every situation needs the next bit
     from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
     athenaCommonFlags.FilesInput = svcMgr.EventSelector.InputCollections
     from TriggerJobOpts.TriggerFlags import TriggerFlags
     TriggerFlags.configurationSourceList=['ds']
     from TriggerJobOpts.TriggerConfigGetter import TriggerConfigGetter
     cfg = TriggerConfigGetter()


#the main algorithm sequence
algSeq = CfgMgr.AthSequencer("AthAlgSeq")

#The GRLTool
MyGRLTool=CfgMgr.GoodRunsListSelectionTool("MyGRLTool")
MyGRLTool.GoodRunsListVec=[GRL_XML]
MyGRLTool.PassThrough    =True
ToolSvc += MyGRLTool





#algorithm to fill the Validation hists
SkimAlgFromDerivationAlg=CfgMgr.SkimAlgFromDerivation()
#SkimAlgFromDerivationAlg.OutputLevel             =VERBOSE

SkimAlgFromDerivationAlg.EventInfoKey            =m_EventInfoKey
SkimAlgFromDerivationAlg.TrkContainerKey         =m_TrkContainerKey
SkimAlgFromDerivationAlg.VtxContainerKey         =m_VtxContainerKey
SkimAlgFromDerivationAlg.TruthVtxContainerKey    =m_TruthVtxContainerKey
SkimAlgFromDerivationAlg.TruthContainerKey       =m_TruthContainerKey
SkimAlgFromDerivationAlg.HIEventShapeContainerKey=m_HIEventShapeContainerKey

SkimAlgFromDerivationAlg.StoreEventInfo          =m_StoreEventInfo      #           1)Store Event Info
SkimAlgFromDerivationAlg.StoreTracks             =m_StoreTracks         #           1)Store Tracks
SkimAlgFromDerivationAlg.StoreVtx                =m_StoreVtx            #           1)Store Vertex info
SkimAlgFromDerivationAlg.StoreET                 =m_StoreET             #           1)Store ET info (from MET container)
SkimAlgFromDerivationAlg.StoreTruthVtx           =m_StoreTruthVtx
SkimAlgFromDerivationAlg.StoreTruth              =m_StoreTruth
SkimAlgFromDerivationAlg.TruthMinPT              =m_TruthMinPT

SkimAlgFromDerivationAlg.UseTrigger              =m_UseTrigger          #           1)Make Trigger branches 2)use decesion  #Typically turn off for MC
SkimAlgFromDerivationAlg.TriggerChains           =m_TriggerChains       #           1)Trigger Chains to be used
SkimAlgFromDerivationAlg.StoreAllEvents          =m_StoreAllEvents      #           1)keep event irrespective of Trigger (require GRL & Vertex however)
SkimAlgFromDerivationAlg.MaxZvtx                 =m_MaxZvtx             #           1)max zvtx for primary zvtx
SkimAlgFromDerivationAlg.GRLTool                 =MyGRLTool             #           1)GRL Tool 
SkimAlgFromDerivationAlg.UseGRL                  =m_UseGRL              #           1)Ignore GRL if false (automatically false for MC)
algSeq += SkimAlgFromDerivationAlg

svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["MYSTREAM DATAFILE='myfile.root' OPT='RECREATE'"]


