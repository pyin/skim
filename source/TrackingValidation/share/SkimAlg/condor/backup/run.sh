#!/bin/zsh

ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup asetup

dir=`pwd`
cd /atlas/u/soumya/work/testarea/21.2.6/build
acmSetup


cd $dir
cat joboptions_MC12.py | sed 's/directory="0"/directory="'$1'"/' > file$1.py
athena file$1.py
rm file$1.py
touch done$1.txt
