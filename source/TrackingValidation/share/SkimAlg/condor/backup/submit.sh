MAXJOBS=116
#MAXJOBS=2

  for job in `seq 0 ${MAXJOBS[$TYPE]}`
  do
    file=c_job$job.sub

    dir=`pwd`                        
    echo "Initialdir      = $dir" >$file
    echo "Universe        = vanilla" >>$file
    echo "GetEnv          = true" >>$file
    echo "" >>$file
    echo "" >>$file
                  
    echo "Executable      = \$(Initialdir)/run.sh" >>$file
    echo "JOB=$job" >>$file
    echo "" >>$file
    echo "" >>$file
    echo "Output          = \$(Initialdir)/log/$file.out" >>$file
    echo "Error           = \$(Initialdir)/log/$file.err" >>$file
    echo "Log             = \$(Initialdir)/log/$file.log" >>$file
    echo "" >>$file
    echo "" >>$file
                  
    echo "Arguments       = \"\$(JOB) \"" >>$file
    echo "queue" >>$file
    #condor_submit $file
    #rm $file
  done
