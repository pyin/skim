list=`ls --color=never athfile* eventLoopHeartBeat.txt geometryinfo*.txt  PoolFileCatalog.xml* hostnamelookup.tmp 2>/dev/null`
for file in $list
do
  if [ -e $file ]
  then
    rm $file
  fi
done
