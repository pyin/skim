#All Settings (Default)
m_EvtMax=-1
m_DataSource         ='data'

m_UseGRL             =True                  #           1)Ignore GRL if false
GRL_XML              ="merged_grl.xml"      #           1)XML File for GRL
m_MaxZvtx            =300                   #           1)max |zvtx| for primary zvtx
m_StoreAllEvents     =True                  #           1)keep event irrespective of Trigger (require GRL & Vertex however)
m_UseTrigger         =True                  #           1)Make Trigger branches 2)use decision
m_TriggerTool        =True
m_TriggerChains      ="HLT_mb_sptrk|HLT_mb_sp900_.*" #  1)Trigges Used 2)Branches will be made for them in the OutputTree


m_EventInfoKey            ="EventInfo"
m_TrkContainerKey         ="InDetTrackParticles"
m_VtxContainerKey         ="PrimaryVertices"
m_METContainerKey         ="MET_Calo"
m_TruthVtxContainerKey    ="TruthVertices"
m_TruthContainerKey       ="TruthParticles"
m_HIEventShapeContainerKey="HIEventShape"
m_HIEventShapeTriggerContainerKey="HLT_xAOD__HIEventShapeContainer_HIUE"

m_StoreEventInfo     =True                  #           1)Store Event Info
m_StoreTracks        =True                  #           1)Store Tracks
m_StoreTrackDetails  =1                     #           1)Additional Tracking info
m_StoreVtx           =True                  #           1)Store Vertex info
m_StoreET            =False                 #           1)Store ET info (from MET Container)
m_StoreTruthVtx      =False
m_StoreTruth         =False
m_TruthMinPT         =500


##changes for XeXe Data
##m_EvtMax=10
#m_METContainerKey         =""
#m_HIEventShapeTriggerContainerKey=""
#m_InputFile="/atlas/data10/soumya/data17_hi/*"
#m_StoreAllEvents=False
#m_StoreTrackDetails=1
#m_TriggerChains="HLT_noalg_mb_L1TE5|HLT_mb_sptrk_L1MBTS_1_VTE5|HLT_noalg_mb_L1TE4|HLT_mb_sptrk_L1MBTS_1_VTE4" 


##changes for XeXe MC
#m_EvtMax=10
dataSource           ='geant4'
m_InputFile="/atlas/data10/soumya/mc16_valid/*"
m_METContainerKey                =""
m_HIEventShapeTriggerContainerKey=""
m_StoreTrackDetails  =1
m_TriggerTool        =False
m_StoreTruthVtx      =True
m_StoreTruth         =True



##Changes for for Pb+Pb MC
#m_EvtMax=-1
#dataSource           ='geant4'
#m_InputFile          ="/atlas/work/shared/TestSamples2/mc15_5TeV.420000.Hijing_PbPb_5p02TeV_MinBias_Flow_JJFV6.recon.AOD.e3754_s2633_r7232_tid06826513_00/*"
#m_UseGRL             =False
#m_UseTrigger         =False
#m_TriggerTool        =False
#m_StoreTruthVtx      =True
#m_StoreTruth         =True
#m_HIEventShapeTriggerContainerKey=""
















import AthenaPoolCnvSvc.ReadAthenaPool
import glob
svcMgr.EventSelector.InputCollections = glob.glob(m_InputFile)


theApp.EvtMax = m_EvtMax


from AthenaCommon.GlobalFlags import globalflags
globalflags.DataSource.set_Value_and_Lock(m_DataSource) #data geant4


svcMgr += CfgMgr.AthenaEventLoopMgr(EventPrintoutInterval=1)


#if m_TriggerTool==True :
#     #TriggerConfigGetter looks at recflags, so it might be a good idea to set those, but I have found not to be always necessary
#     from RecExConfig.RecFlags  import rec
#     rec.readRDO = False #readRDO is the True one by default. Turn it off before turning others on
#     rec.readAOD = True #example for reading AOD
#
#     #need this bit for AOD because AOD doesn't contain all trigger info - so has to connect to DB for some of it
#     #ESD/RDO appear not to need this. Only do this bit if running on data, not MC
#     # from AthenaCommon.GlobalFlags import globalflags
#     # globalflags.DataSource = 'data'
#
#     #every situation needs the next bit
#     from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
#     athenaCommonFlags.FilesInput = svcMgr.EventSelector.InputCollections
#     from TriggerJobOpts.TriggerFlags import TriggerFlags
#     TriggerFlags.configurationSourceList=['ds']
#     from TriggerJobOpts.TriggerConfigGetter import TriggerConfigGetter
#     cfg = TriggerConfigGetter()


#the main algorithm sequence
algSeq = CfgMgr.AthSequencer("AthAlgSeq")

#The GRLTool
MyGRLTool=CfgMgr.GoodRunsListSelectionTool("MyGRLTool")
MyGRLTool.GoodRunsListVec=[GRL_XML]
MyGRLTool.PassThrough    =True
ToolSvc += MyGRLTool

TrkSelTool=CfgMgr.InDet__InDetTrackSelectionTool("TrackSelectionTool",
                                                 CutLevel = "MinBias",
                                                 minPt = 400.)
ToolSvc += TrkSelTool





#algorithm to fill the Validation hists
SkimAlgAlg=CfgMgr.SkimAlg()
#SkimAlgAlg.OutputLevel             =VERBOSE

SkimAlgAlg.EventInfoKey            =m_EventInfoKey
SkimAlgAlg.TrkContainerKey         =m_TrkContainerKey
SkimAlgAlg.VtxContainerKey         =m_VtxContainerKey
SkimAlgAlg.METContainerKey         =m_METContainerKey
SkimAlgAlg.TruthVtxContainerKey    =m_TruthVtxContainerKey
SkimAlgAlg.TruthContainerKey       =m_TruthContainerKey
SkimAlgAlg.HIEventShapeContainerKey=m_HIEventShapeContainerKey
SkimAlgAlg.HIEventShapeTriggerContainerKey=m_HIEventShapeTriggerContainerKey

SkimAlgAlg.StoreEventInfo          =m_StoreEventInfo      #           1)Store Event Info
SkimAlgAlg.StoreTracks             =m_StoreTracks         #           1)Store Tracks
SkimAlgAlg.StoreTrackDetails       =m_StoreTrackDetails   #           1)Additional Tracking info
SkimAlgAlg.StoreVtx                =m_StoreVtx            #           1)Store Vertex info
SkimAlgAlg.StoreET                 =m_StoreET             #           1)Store ET info (from MET container)
SkimAlgAlg.StoreTruthVtx           =m_StoreTruthVtx
SkimAlgAlg.StoreTruth              =m_StoreTruth
SkimAlgAlg.TruthMinPT              =m_TruthMinPT

SkimAlgAlg.UseTrigger              =m_UseTrigger          #           1)Make Trigger branches 2)use decesion  #Typically turn off for MC
SkimAlgAlg.TriggerChains           =m_TriggerChains       #           1)Trigger Chains to be used
SkimAlgAlg.StoreAllEvents          =m_StoreAllEvents      #           1)keep event irrespective of Trigger (require GRL & Vertex however)
SkimAlgAlg.MaxZvtx                 =m_MaxZvtx             #           1)max zvtx for primary zvtx
SkimAlgAlg.GRLTool                 =MyGRLTool             #           1)GRL Tool 
SkimAlgAlg.UseGRL                  =m_UseGRL              #           1)Ignore GRL if false (automatically false for MC)
SkimAlgAlg.TrackSelectionTool      =TrkSelTool            #           1)MinBias track selection tool
algSeq += SkimAlgAlg

svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["MYSTREAM DATAFILE='myfile.root' OPT='RECREATE'"]


