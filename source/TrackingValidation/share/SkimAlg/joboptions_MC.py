import AthenaPoolCnvSvc.ReadAthenaPool
import glob
#svcMgr.EventSelector.InputCollections = glob.glob("/atlas/u/soumya/work/MachineLearning/xAOD/mc16_valid.420000.Hijing_PbPb_5p02TeV_MinBias_Flow_JJFV6.recon.AOD.e4962_s2862_s2921_r11039_tid15956337_00/*")
#svcMgr.EventSelector.InputCollections = glob.glob("/atlas/u/soumya/work/group.phys-hi.5TeV.420012.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2R04_o21093_01_EXT2/*.root")
#svcMgr.EventSelector.InputCollections = glob.glob("/atlas/u/soumya/work/ATL-HI261/group.phys-hi.2018PbPb5TeV.420012.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2R04_o21095n_01_EXT2/*.root")
svcMgr.EventSelector.InputCollections = glob.glob("/atlas/u/soumya/work/ATL-HI261/group.phys-hi.2018PbPb5TeV.420012.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2R04_o21095n_05_EXT2/*.root")
theApp.EvtMax = -1


from AthenaCommon.GlobalFlags import globalflags
globalflags.DataSource.set_Value_and_Lock("geant4") #data geant4


svcMgr += CfgMgr.AthenaEventLoopMgr(EventPrintoutInterval=1)


m_TriggerTool=False
if m_TriggerTool==True :
     #TriggerConfigGetter looks at recflags, so it might be a good idea to set those, but I have found not to be always necessary
     from RecExConfig.RecFlags  import rec
     rec.readRDO = False #readRDO is the True one by default. Turn it off before turning others on
     rec.readAOD = True #example for reading AOD

     #need this bit for AOD because AOD doesn't contain all trigger info - so has to connect to DB for some of it
     #ESD/RDO appear not to need this. Only do this bit if running on data, not MC
     # from AthenaCommon.GlobalFlags import globalflags
     # globalflags.DataSource = 'data'

     #every situation needs the next bit
     from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
     athenaCommonFlags.FilesInput = svcMgr.EventSelector.InputCollections
     from TriggerJobOpts.TriggerFlags import TriggerFlags
     TriggerFlags.configurationSourceList=['ds']
     from TriggerJobOpts.TriggerConfigGetter import TriggerConfigGetter
     cfg = TriggerConfigGetter()


#the main algorithm sequence
algSeq = CfgMgr.AthSequencer("AthAlgSeq")

#The GRLTool
MyGRLTool=CfgMgr.GoodRunsListSelectionTool("MyGRLTool")
MyGRLTool.GoodRunsListVec=["merged_grl.xml"]
MyGRLTool.PassThrough    =True
ToolSvc += MyGRLTool

#Track Selector Tool
TrkSelTool=CfgMgr.InDet__InDetTrackSelectionTool("TrackSelectionTool",
                                                 CutLevel = "MinBias",
                                                 minPt = 400.)
ToolSvc += TrkSelTool



#algorithm to fill the Validation hists
SkimAlgAlg=CfgMgr.SkimAlg()
#SkimAlgAlg.OutputLevel             =VERBOSE
m_TriggerChains      ="HLT_mb_sptrk|HLT_mb_sp900_.*" #  1)Trigges Used 2)Branches will be made for them in the OutputTree

SkimAlgAlg.GRLTool                 =MyGRLTool       #           1)GRL Tool 
SkimAlgAlg.UseGRL                  =False           #           1)Ignore GRL if false (automatically false for MC)
SkimAlgAlg.MaxZvtx                 =250             #           1)max zvtx for primary zvtx

SkimAlgAlg.StoreAllEvents          =True           #      1)keep event irrespective of Trigger (require GRL & Vertex however)
SkimAlgAlg.UseTrigger              =m_TriggerTool  #      1)Make Trigger branches 2)use decesion  #Typically turn off for MC
SkimAlgAlg.TriggerChains           =m_TriggerChains#      1)Trigger Chains to be used

SkimAlgAlg.HIEventShapeContainerKey       =""
SkimAlgAlg.HIEventShapeTriggerContainerKey=""

SkimAlgAlg.StoreEventInfo          =True      #           1)Store Event Info
SkimAlgAlg.StoreTracks             =True      #           1)Store Tracks
SkimAlgAlg.StoreTrackDetails       =2         #           1)Additional Tracking info
SkimAlgAlg.StoreVtx                =True      #           1)Store Vertex info
SkimAlgAlg.StoreET                 =False     #           1)Store ET info (from MET container)
SkimAlgAlg.StoreTruthVtx           =True
SkimAlgAlg.StoreTruth              =True
SkimAlgAlg.TruthMinPT              =400

SkimAlgAlg.TrackSelectionTool      =TrkSelTool            #           1)MinBias track selection tool
algSeq += SkimAlgAlg

svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["MYSTREAM DATAFILE='myfile.root' OPT='RECREATE'"]


