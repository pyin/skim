#ifndef __MYUTILS_H__
#define __MYUTILS_H__

#include "xAODTracking/TrackParticle.h"

namespace MyUtils{
   enum{
      PP_MIN_BIAS=2,
      PP_MIN_BIAS_MODIFIED=4,
      HI_LOOSE=8,
      HI_TIGHT=16,
      // HI_TIGHT_TIGHTER_D0_Z0=32,
      // HI_LOOSE_7SCT_HITS    =64,
      HI_TIGHT_LOOSE_D0_Z0  =32,
      HI_LOOSE_TIGHT_D0_Z0  =64,
      HI_LOOSE_TIGHTER_D0_Z0=128
   };

   int TrackQuality(const xAOD::TrackParticle* track,float z_vtx);
}
#endif
