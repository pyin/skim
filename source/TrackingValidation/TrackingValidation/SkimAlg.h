// -*- c++ -*-
#ifndef __SKIMALG_H__
#define __SKIMALG_H__

#include "AthenaBaseComps/AthAlgorithm.h"

#include "string"
#include "map"
#include "vector"

#include "GaudiKernel/ToolHandle.h"
// #include "GoodRunsLists/IGoodRunsListSelectionTool.h"
#include "AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"
#include "xAODTruth/TruthParticleContainer.h"

//#include "Rtypes.h"

class TTree;
class Module;

// class IGoodRunsListSelectionTool;
// namespace Trig {
//   class TrigDecisionTool;
// }
// namespace InDet {
//   class IInDetTrackSelectionTool;
// }


class SkimAlg : public AthAlgorithm{
public:
   /** Standard Athena-Algorithm Constructor */
   SkimAlg(const std::string& name, ISvcLocator* pSvcLocator);
   /** Default Destructor */
   ~SkimAlg() {};


   virtual StatusCode initialize();
   virtual StatusCode execute();
   virtual StatusCode finalize();


private:
   std::string m_HIEventShapeTriggerContainer_key;
   std::string m_HIEventShapeContainer_key;
   std::vector<Module*> m_modules;

   StatusCode CleaningCuts    (bool &pass_cleaning_cuts);


   bool        m_use_trigger    = true ;     //=true then use the Trigger (make branches, use decesion)
   std::string m_Trigger_Chains   ;     //Trigger Chains for TrigDecesion and branches
   //void       InitTriggers    (){};
   void       AddTriggerBranches    ();
   StatusCode ProcessTriggers (bool &pass_trigger_cuts);
   bool m_trigger_Flag[1000]                  ;//the actual bool that stores if a trigger passes, this is written to the output tree
   std::map<std::string,bool*> m_trigger_chain;//bool flags that indicate if the trigger passes
                                               //trigger name is the "key", pointer to m_trigger_Flag is value


   bool m_store_EventInfo   =false;//=true then store even info
   std::string m_EventInfo_key    ;
   void       InitEventInfo   (TTree *l_OutTree = nullptr);
   StatusCode ProcessEventInfo();
   UInt_t RunNumber=0,lumi_block=0,bunch_crossing_id=0;
   ULong64_t eventNumber=0;
   Float_t m_L1TE;




   bool        m_store_Vtx           = false; //=true then store vertex info
   std::string m_vtx_container_key;
   void       InitVertex   (TTree *l_OutTree = nullptr);
   StatusCode ProcessVertex();
   std::vector<float> m_vtx_z     ;
   std::vector<float> m_vtx_x     ;
   std::vector<float> m_vtx_y     ;
   std::vector<int>   m_vtx_ntrk  ;



   bool        m_store_tracks        = false; //=true then store tracks
   int         m_store_track_details = 1;
   std::string m_trk_container_key;
   void       InitTracks   (TTree *l_OutTree = nullptr);
   StatusCode ProcessTracks();
   std::vector<int>   m_trk_numqual;
   std::vector<float> m_track_pt     ;
   std::vector<float> m_track_eta    ;
   std::vector<float> m_track_phi    ;
   std::vector<float> m_track_charge ;
   std::vector<int>   m_track_quality;
   std::vector<float> m_track_d0     ;
   std::vector<float> m_track_z0_wrtPV      ;
   std::vector<float> m_track_vz            ;
   std::vector<int>   m_track_Ipix_hits     ;
   std::vector<int>   m_track_Ipix_expected ;
   std::vector<int>   m_track_NIpix_hits    ;
   std::vector<int>   m_track_NIpix_expected;
   std::vector<int>   m_track_sct_hits      ;
   std::vector<int>   m_track_pix_hits      ;
   std::vector<int>   m_track_sct_holes     ;
   std::vector<int>   m_track_pix_holes     ;
   std::vector<int>   m_track_sct_dead      ;
   std::vector<int>   m_track_pix_dead      ;
   std::vector<int>   m_track_sct_shared    ;
   std::vector<int>   m_track_pix_shared    ;
   std::vector<float> m_track_chi2          ;
   std::vector<float> m_track_ndof          ;
   std::vector<unsigned long> m_track_patternRecoInfo;

   std::map<const xAOD::TruthParticle*,int> m_trk_truth_index_temp1;
   std::vector<int  > m_trk_truth_index;
   std::vector<float> m_trk_truth_prob ;
   std::vector<int  > m_trk_truth_barcode;
   std::vector<Bool_t>m_trk_truth_IsPrimary;
   bool IsPrimaryParticle(const xAOD::TruthParticle* particle);


   bool        m_store_ET            = false; //=true then store ET info (from MET object)
   std::string m_met_container_key;
   void       InitMET   (TTree *l_OutTree = nullptr);
   StatusCode ProcessMET();
   std::vector<float> m_MET_sumet;



   bool        m_store_truth_Vtx     = false; //=true then store truth vertex info
   std::string m_truth_vtx_container_key;
   void       InitTruthVertex(TTree *l_OutTree = nullptr);
   StatusCode ProcessTruthVertex();
   std::vector<float> m_truth_vtx_z;
   std::vector<float> m_truth_vtx_x;
   std::vector<float> m_truth_vtx_y;
   std::vector<float> m_truth_vtx_t;



   bool        m_store_truth;        //=true then store truth track info
   double      m_min_pT_Truth;
   std::string m_truth_container_key;
   void InitTruth(TTree *l_OutTree = nullptr);
   StatusCode ProcessTruth();
   std::vector<float> m_truth_pt     ;
   std::vector<float> m_truth_eta    ;
   std::vector<float> m_truth_phi    ;
   std::vector<float> m_truth_charge ;
   std::vector<int>   m_truth_id     ;
   std::vector<int>   m_truth_barcode;
   std::vector<int>   m_truth_quality;
   std::vector<int>   m_truth_trk_index;




   //Alg Properties
   bool        m_use_GRL        = true ;     //=true then use GRL
   float       m_MaxZvtx        = 250.f;     //max zvtx in mm
   bool        m_StoreAllEvents = false;     //=true then store all events,=false then store events that pass atleast one of the configured triggers


   //Tree and variables to be written to tree and related stuff
   TTree *m_OutTree = nullptr;


   //Tools
   ToolHandle<IGoodRunsListSelectionTool>      m_grlTool;
   ToolHandle<Trig::TrigDecisionTool>          m_trigTool;
   ToolHandle<InDet::IInDetTrackSelectionTool> m_trkSelTool;
};
#endif
