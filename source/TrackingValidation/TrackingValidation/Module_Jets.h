#ifndef __MODULE_JETS_H__
#define __MODULE_JETS_H__

#include "TrackingValidation/Module.h"
#include "Rtypes.h"
#include "xAODJet/JetContainer.h"
#include "GaudiKernel/ToolHandle.h"
#include <JetCalibTools/IJetCalibrationTool.h>
#include <xAODJet/JetAuxContainer.h>
#include <xAODJet/JetContainer.h>
#include <xAODJet/Jet.h>
#include <xAODJet/JetConstituentVector.h>
#include "JetSelectorTools/JetCleaningTool.h"
#include "JetInterface/IJetUpdateJvt.h"
#include "JetRecTools/CorrectPFOTool.h"




class Jets: public Module {

public:
  // std::vector<float> m_E;
  std::vector<float> m_pt;
  std::vector<float> m_eta;
  std::vector<float> m_phi;
  std::vector<float> m_t;
  std::vector<float> m_pt_orig;
  std::vector<int  > m_JCTight;
  // std::vector<float> m_jvf;
  // std::vector<float> m_jvt;
  std::vector<float> m_newjvt;

  std::vector<float> m_EMpt;
  std::vector<float> m_EMeta;
  std::vector<float> m_EMphi;

  std::vector<std::vector<int  >> m_jetTrk_index;
  std::vector<std::vector<float>> m_jetTRK_pT;
  std::vector<std::vector<float>> m_jetTRK_WgtPt;
  std::vector<std::vector<float>> m_jetTRK_eta;
  std::vector<std::vector<float>> m_jetTRK_phi;
  std::vector<std::vector<float>> m_jetNPFO_pT;
  std::vector<std::vector<float>> m_jetNPFO_eta;
  std::vector<std::vector<float>> m_jetNPFO_phi;
  // std::vector<std::vector<float>> m_layerEnergies;

  std::string m_TrackParticleContainerKey = "";
  StatusCode ProcessTrackLinks(const xAOD::Jet* jet);

public:
  Jets(ServiceHandle<StoreGateSvc> evtStore,
       std::string ContainerName,
       std::string BranchName = "")
    : Module(evtStore, ContainerName, BranchName) {
    m_TrackParticleContainerKey = "";
  };


  virtual void Init(TTree *l_OutTree, int level_of_detail = 0) override;
  virtual StatusCode Process() override {return StatusCode::SUCCESS;}
  virtual StatusCode Process(ToolHandle<IJetCalibrationTool>& jetCalibToolEMTopo4, ToolHandle<IJetCalibrationTool>& jetCalibToolEMPFlow4,
                             ToolHandle<IJetSelector>& jetCleaningToolLoose, ToolHandle<IJetSelector>&,
                             ToolHandle<IJetUpdateJvt>& jvtTool, ToolHandle<CP::IWeightPFOTool>& weightPFOTool) override;

  void InitTrackLinks(TTree *l_OutTree, std::string l_TrackParticleContainerKey);
};
#endif
