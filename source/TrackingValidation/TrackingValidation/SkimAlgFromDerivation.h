// -*- c++ -*-
#ifndef __SKIMALGFROMDERIVATION_H__
#define __SKIMALGFROMDERIVATION_H__

#include "AthenaBaseComps/AthAlgorithm.h"

#include "string"
#include "map"
#include "vector"

#include "GaudiKernel/ToolHandle.h"
// #include "GoodRunsLists/IGoodRunsListSelectionTool.h"
#include "AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"


class TTree;


class SkimAlgFromDerivation : public AthAlgorithm{
public:
   /** Standard Athena-Algorithm Constructor */
   SkimAlgFromDerivation(const std::string& name, ISvcLocator* pSvcLocator);
   /** Default Destructor */
   ~SkimAlgFromDerivation() {};


   virtual StatusCode initialize();
   virtual StatusCode execute();
   virtual StatusCode finalize();


private:

   StatusCode CleaningCuts    (bool &pass_cleaning_cuts);


   bool        m_use_trigger    = true ;     //=true then use the Trigger (make branches, use decesion)
   std::string m_Trigger_Chains   ;     //Trigger Chains for TrigDecesion and branches
   //void       InitTriggers    (TTree *l_OutTree = nullptr){};
   void       AddTriggerBranches    ();
   StatusCode ProcessTriggers (bool &pass_trigger_cuts);
   bool m_trigger_Flag[1000]                  ;//the actual bool that stores if a trigger passes, this is written to the output tree
   std::map<std::string,bool*> m_trigger_chain;//bool flags that indicate if the trigger passes
                                               //trigger name is the "key", pointer to m_trigger_Flag is value


   bool m_store_EventInfo   =false;//=true then store even info
   std::string m_EventInfo_key    ;
   void       InitEventInfo   (TTree *l_OutTree = nullptr);
   StatusCode ProcessEventInfo();
   UInt_t RunNumber=0,lumi_block=0,bunch_crossing_id=0;
   ULong64_t eventNumber=0;




   bool        m_store_Vtx           = false; //=true then store vertex info
   std::string m_vtx_container_key;
   void       InitVertex   (TTree *l_OutTree = nullptr);
   StatusCode ProcessVertex();
   std::vector<float> m_vtx_z     ;
   std::vector<float> m_vtx_x     ;
   std::vector<float> m_vtx_y     ;
   std::vector<int>   m_vtx_ntrk  ;



   bool        m_store_tracks        = false; //=true then store tracks
   std::string m_trk_container_key;
   void       InitTracks   (TTree *l_OutTree = nullptr);
   StatusCode ProcessTracks();
   std::vector<int>   m_trk_numqual;
   std::vector<float> m_track_pt     ;
   std::vector<float> m_track_eta    ;
   std::vector<float> m_track_phi    ;
   std::vector<float> m_track_charge ;
   std::vector<int>   m_track_quality;


   bool        m_store_ET            = false;
   std::string m_HIEventShapeContainer_key;
   void       InitET   (TTree *l_OutTree = nullptr);
   StatusCode ProcessET();
   std::vector<float> m_FCalET;



   bool        m_store_truth_Vtx     = false; //=true then store truth vertex info
   std::string m_truth_vtx_container_key;
   void       InitTruthVertex(TTree *l_OutTree = nullptr);
   StatusCode ProcessTruthVertex();
   std::vector<float> m_truth_vtx_z;
   std::vector<float> m_truth_vtx_x;
   std::vector<float> m_truth_vtx_y;
   std::vector<float> m_truth_vtx_t;



   bool        m_store_truth;        //=true then store truth track info
   double      m_min_pT_Truth;
   std::string m_truth_container_key;
   void InitTruth(TTree *l_OutTree = nullptr);
   StatusCode ProcessTruth();
   std::vector<float> m_truth_pt     ;
   std::vector<float> m_truth_eta    ;
   std::vector<float> m_truth_phi    ;
   std::vector<float> m_truth_charge ;
   std::vector<int>   m_truth_id     ;
   std::vector<int>   m_truth_barcode;
   std::vector<int>   m_truth_quality;




   //Alg Properties
   bool        m_use_GRL        = true ;     //=true then use GRL
   float       m_MaxZvtx        = 250.f;     //max zvtx in mm
   bool        m_StoreAllEvents = false;     //=true then store all events,=false then store events that pass atleast one of the configured triggers


   //Tree and variables to be written to tree and related stuff
   TTree *m_OutTree = nullptr;


   //Tools
   ToolHandle<IGoodRunsListSelectionTool>      m_grlTool;
   ToolHandle<Trig::TrigDecisionTool>          m_trigTool;
};
#endif
