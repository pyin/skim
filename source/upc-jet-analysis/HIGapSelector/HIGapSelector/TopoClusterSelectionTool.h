#ifndef UPC_TOPOCLUSTERSELECTION_TOOL_H
#define UPC_TOPOCLUSTERSELECTION_TOOL_H

#include "xAODCaloEvent/CaloCluster.h"
#include "AsgTools/AsgTool.h"
#include "PATCore/TAccept.h"

class TH1F;
namespace UPC
{

  //Interface class for python athena
  class ITopoClusterSelectionTool : virtual public asg::IAsgTool 
  {
    ASG_TOOL_INTERFACE(ITopoClusterSelectionTool)
  public:
    virtual ~ITopoClusterSelectionTool() {};
    virtual const Root::TAccept& getTAccept() const = 0;
    virtual const Root::TAccept& acceptWithBookkeeping(const xAOD::CaloCluster& cluster) const = 0;
    virtual bool accept(const xAOD::CaloCluster& cluster) const = 0;
    
  };

  class TopoClusterSelectionTool : public asg::AsgTool, virtual public ITopoClusterSelectionTool
  {
    ASG_TOOL_CLASS(TopoClusterSelectionTool,ITopoClusterSelectionTool)
  public:

    TopoClusterSelectionTool(const std::string& name);
    virtual ~TopoClusterSelectionTool() {};

    virtual StatusCode initialize() override;


    const Root::TAccept& getTAccept() const;
    const Root::TAccept& acceptWithBookkeeping(const xAOD::CaloCluster& cluster) const;
    bool accept(const xAOD::CaloCluster& cluster) const;
    StatusCode checkClusterForMoments(const xAOD::CaloCluster& cl) const;
    
  private:

    float lookupCellSignificanceCut(float eta, int cl_cell_sig_samp) const;
    bool m_isInitialized;
    std::string m_calib_file_name;
    std::string m_calib_histo_name;
    float m_minPt;
    float m_minEnergy;
    float  m_minEta;
    float m_maxEta;
    bool m_doAbsEta;
    bool m_doSigCuts;
    TH1F* m_h1_TCSigCut;

    mutable Root::TAccept m_accept;


  };
}
#endif
