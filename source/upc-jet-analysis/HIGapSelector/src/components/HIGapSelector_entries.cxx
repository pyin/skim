#include "GaudiKernel/DeclareFactoryEntries.h"
#include "HIGapSelector/TopoClusterSelectionTool.h"

DECLARE_NAMESPACE_TOOL_FACTORY( UPC, TopoClusterSelectionTool )

#include "../GapCalculationAlg.h"
DECLARE_ALGORITHM_FACTORY( GapCalculationAlg )

DECLARE_FACTORY_ENTRIES( HIGapSelector )
{
  DECLARE_ALGORITHM( GapCalculationAlg );
  DECLARE_NAMESPACE_TOOL( UPC, TopoClusterSelectionTool );
}
