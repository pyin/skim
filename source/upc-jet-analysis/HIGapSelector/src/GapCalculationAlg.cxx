#include "GapCalculationAlg.h"
#include "HIGapSelector/TopoClusterSelectionTool.h"
#include "HIGapSelector/GapCalculator.h"
#include <xAODCaloEvent/CaloClusterContainer.h>
#include <xAODTracking/TrackParticleContainer.h>
#include <InDetTrackSelectionTool/InDetTrackSelectionTool.h>


GapCalculationAlg::GapCalculationAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAlgorithm( name, pSvcLocator ),
											    m_cluster_container_name("CaloCalTopoClusters"),
											    m_track_container_name("InDetTrackParticles"),
											    m_gap_min(0.5)
{
  declareProperty( "ClusterContainerName", m_cluster_container_name);
  declareProperty( "TrackContainerName", m_track_container_name);
  declareProperty( "ClusterSelectionTool", m_tc_selection_tool);
  declareProperty( "TrackSelectionTool", m_track_selection_tool);
  declareProperty( "GapMinimum",m_gap_min);

}

GapCalculationAlg::~GapCalculationAlg() {}


StatusCode GapCalculationAlg::initialize() 
{
  ATH_MSG_INFO ("Initializing " << name() << "...");
  if(m_tc_selection_tool.retrieve().isFailure())
  {
    ATH_MSG_ERROR("Could not retrieve TopoClusterSelectionTool.");
    return StatusCode::FAILURE;
  }
  if(m_track_selection_tool.retrieve().isFailure())
  {
    ATH_MSG_ERROR("Could not retrieve InDetTrackSelectionTool.");
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}

StatusCode GapCalculationAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  return StatusCode::SUCCESS;
}

StatusCode GapCalculationAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");

  const xAOD::CaloClusterContainer* tc=nullptr;
  if(evtStore()->retrieve(tc,m_cluster_container_name).isFailure())
  {
    ATH_MSG_ERROR( "Could not find CaloClusterContainer object w/ name" << m_cluster_container_name );
    return StatusCode::FAILURE;
  }

  std::set<float> eta_vals;
  for(auto cl : *tc)
  {
    if(m_tc_selection_tool->accept(*cl)) eta_vals.insert(cl->eta());
  }
  ///
  const xAOD::TrackParticleContainer* track_container=nullptr;
  if(evtStore()->retrieve(track_container,m_track_container_name).isFailure())
  {
    ATH_MSG_ERROR( "Could not retrieve TrackParticleContainer object w/ name " << m_track_container_name );
    return StatusCode::FAILURE;
  }
  //could use version of track selection with explicit reference to Primary Vertex
  for(auto track : *track_container)
  {
    //if(m_track_selection_tool->accept(*track,pv)) eta_vals.insert(track->eta());
    if(m_track_selection_tool->accept(*track)) eta_vals.insert(track->eta());
  }
  float jet_eta1=1.2;
  float jet_eta2=-0.2;
  
  float edge_gap_pos=UPC::getEdgeGapPos(eta_vals);
  float edge_gap_neg=UPC::getEdgeGapNeg(eta_vals);
  float sum_gap=UPC::getSumGap(eta_vals,m_gap_min);
  float sum_gap_pos=UPC::getSumGapPos(eta_vals,jet_eta1,m_gap_min);
  float sum_gap_neg=UPC::getSumGapNeg(eta_vals,jet_eta2,m_gap_min);
  ATH_MSG_INFO( "Gap quantities: "  
		<< std::setw(12) << edge_gap_pos
		<< std::setw(12) << edge_gap_neg
		<< std::setw(12) << sum_gap
		<< std::setw(12) << sum_gap_pos
		<< std::setw(12) << sum_gap_neg);
  return StatusCode::SUCCESS;
}


