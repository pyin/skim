#ifndef HIGAPSELECTOR_GAPCALCULATIONALG_H
#define HIGAPSELECTOR_GAPCALCULATIONALG_H 1

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h" 

namespace UPC
{
  class ITopoClusterSelectionTool;
}
namespace InDet
{
  class IInDetTrackSelectionTool;
}

class GapCalculationAlg: public ::AthAlgorithm { 
 public: 
  GapCalculationAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~GapCalculationAlg(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

 private: 
  std::string m_cluster_container_name;
  std::string m_track_container_name;
  ToolHandle<UPC::ITopoClusterSelectionTool> m_tc_selection_tool;
  ToolHandle<InDet::IInDetTrackSelectionTool> m_track_selection_tool;
  float m_gap_min;
}; 

#endif //> !HIGAPSELECTOR_GAPCALCULATIONALG_H
