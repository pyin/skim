#include "HIGapSelector/TopoClusterSelectionTool.h"
#include <PathResolver/PathResolver.h>
#include <TFile.h>
#include <TH1F.h>

namespace UPC
{
  TopoClusterSelectionTool::TopoClusterSelectionTool(const std::string& name) : asg::AsgTool(name),
										m_isInitialized(false),
										m_calib_file_name(""),
										m_calib_histo_name("h1_TC_sig_cuts"),
										m_minPt(200.),
										m_minEnergy(-1),
										m_minEta(-1),
										m_maxEta(4.9),
										m_doAbsEta(true),
										m_doSigCuts(true),
										m_h1_TCSigCut(nullptr)
  {

#ifdef ASGTOOL_ATHENA
    declareInterface<ITopoClusterSelectionTool>(this);
#endif

    declareProperty("MinPt", m_minPt, "Minimum transverse momentum");
    declareProperty("MinEnergy", m_minEnergy, "Minimum energy");
    declareProperty("MaxEta", m_maxEta, "Maximum eta");
    declareProperty("MinEta", m_minEta, "Minimum eta");
    declareProperty("ApplySignificanceCuts", m_doSigCuts, "Significance requirements");
    declareProperty("DoAbsEta", m_doAbsEta, "If true, eta cuts are on absolute value");
    declareProperty("CalibFile", m_calib_file_name, "Calibration file name");
    declareProperty("CalibHisto", m_calib_histo_name, "Calibration histogram name");


  }

  StatusCode TopoClusterSelectionTool::initialize()
  {
    ATH_MSG_INFO("Initializing TopoClusterSelectionTool");
    m_isInitialized=true;
    if(m_doSigCuts)
    {
      TFile* f=TFile::Open(PathResolverFindCalibFile(m_calib_file_name).c_str());
      if(!f)
      {
	ATH_MSG_ERROR("Could not find TC significance file: " << m_calib_file_name);
	return StatusCode::FAILURE;
      }
      m_h1_TCSigCut=static_cast<TH1F*>(f->Get(m_calib_histo_name.c_str()));
      if(!m_h1_TCSigCut)
      {
	ATH_MSG_ERROR("Error getting significance cut histogram: " << m_calib_histo_name << " in file: " << m_calib_file_name);
	return StatusCode::FAILURE;
      }
      m_h1_TCSigCut->SetName(std::string(name()+"_"+m_calib_histo_name).c_str());
      m_h1_TCSigCut->SetDirectory(0);
      f->Close();
    }
    //
    m_accept.addCut("MinPt","Minimum transverse momentum");
    m_accept.addCut("MinEnergy","Minimum energy");
    m_accept.addCut("MinEta","Minimum eta");
    m_accept.addCut("MaxEta","Maximum eta");
    m_accept.addCut("MinCellSignificance","significance of cells");
    //
    ATH_MSG_INFO("TopoClusterSelectionTool configured with the following cuts:");
    ATH_MSG_INFO("MinPt = " << m_minPt);
    ATH_MSG_INFO("MinEnergy = " << m_minEnergy);
    if(m_doAbsEta) 
    {
      ATH_MSG_INFO("Eta selections applied to |eta|");
      if(m_minEta < 0) ATH_MSG_INFO( "|eta| <= " << m_maxEta);
      else ATH_MSG_INFO(m_minEta << "< |eta| <= " << m_maxEta);
    }
    else
    {
      ATH_MSG_INFO("Eta selections applied to signed eta");
      ATH_MSG_INFO(m_minEta << "< eta <= " << m_maxEta);
    }
    if(m_doSigCuts)
    {
      ATH_MSG_INFO("Significance cuts taken from " << m_calib_file_name);
      if(msgLvl(MSG::VERBOSE) && m_h1_TCSigCut) m_h1_TCSigCut->Print("ALL");
    }
    else ATH_MSG_INFO("No significance cuts applied");

    return StatusCode::SUCCESS;
  }

  float TopoClusterSelectionTool::lookupCellSignificanceCut(float eta, int cl_cell_sig_samp) const
  {
    float cl_cell_sig_cut=5.;
    if(m_h1_TCSigCut)
    {
      int etabin=m_h1_TCSigCut->GetXaxis()->FindBin(eta);
      if(etabin> 0  && etabin<=m_h1_TCSigCut->GetNbinsX()) cl_cell_sig_cut=m_h1_TCSigCut->GetBinContent(etabin);
    }
    if(cl_cell_sig_samp>=CaloSampling::TileBar0 && cl_cell_sig_samp<=CaloSampling::TileExt2) cl_cell_sig_cut+=2.;
    return cl_cell_sig_cut;
  }

  const Root::TAccept& TopoClusterSelectionTool::getTAccept() const
  {
    return m_accept;
  }

  bool TopoClusterSelectionTool::accept(const xAOD::CaloCluster& cl) const
  {
    if(cl.pt() < m_minPt) return false;
    if(cl.e() < m_minEnergy) return false;
    float eta=cl.eta();
    float eta_a=eta;
    if(m_doAbsEta) eta_a=std::abs(eta);
    if(eta_a < m_minEta) return false;
    if(eta_a >= m_maxEta) return false;

    if(m_doSigCuts)
    {
      double cl_cell_sig=cl.getMomentValue(xAOD::CaloCluster::CELL_SIGNIFICANCE);
      int cl_cell_sig_samp=static_cast<int>(cl.getMomentValue(xAOD::CaloCluster::CELL_SIG_SAMPLING));
      float sig_cut=lookupCellSignificanceCut(eta,cl_cell_sig_samp);
      if(cl_cell_sig < sig_cut) return false;
    }

    return true;
  }

  const Root::TAccept& TopoClusterSelectionTool::acceptWithBookkeeping(const xAOD::CaloCluster& cl) const
  {
    m_accept.clear();
    m_accept.setCutResult("MinPt",cl.pt() > m_minPt);
    m_accept.setCutResult("MinEnergy",cl.e() > m_minEnergy);
    float eta=cl.eta();
    float eta_a=eta;
    if(m_doAbsEta) eta_a=std::abs(eta);
    m_accept.setCutResult("MinEta",eta_a > m_minEta);
    m_accept.setCutResult("MaxEta",eta_a <= m_maxEta);

    if(m_doSigCuts)
    {
      double cl_cell_sig=cl.getMomentValue(xAOD::CaloCluster::CELL_SIGNIFICANCE);
      int cl_cell_sig_samp=static_cast<int>(cl.getMomentValue(xAOD::CaloCluster::CELL_SIG_SAMPLING));
      float sig_cut=lookupCellSignificanceCut(eta,cl_cell_sig_samp);
      m_accept.setCutResult("MinCellSignificance",cl_cell_sig > sig_cut);
    }
    else m_accept.setCutResult("MinCellSignificance",true);

    return m_accept;
  }


}
